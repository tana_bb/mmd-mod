uniform vec3 light0Pos;
uniform vec3 light0Diffuse;
uniform vec3 light0Specular;
uniform vec3 light0Ambient;
uniform vec3 light1Pos;
uniform vec3 light1Diffuse;
uniform vec3 light1Specular;
uniform vec3 light1Ambient;
uniform vec3 light2Pos;
uniform vec3 light2Diffuse;
uniform vec3 light2Specular;
uniform vec3 light2Ambient;
uniform vec4 materialDiffuse;
uniform vec4 materialSpecular;
uniform vec3 materialAmbient;
uniform int cullBack;
uniform int texture0Usage;
uniform vec3 localOrigin;
uniform mat4 localMatrix;

void main(void)
{
	vec4 p4 = gl_ModelViewMatrix * gl_Vertex;
	vec3 p = p4.xyz;
	vec3 n = normalize(gl_NormalMatrix * gl_Normal);
	vec3 v = normalize(-p);
	vec3 l0p = (gl_ModelViewMatrix * vec4(light0Pos, 1.0)).xyz;
	vec3 l1p = (gl_ModelViewMatrix * vec4(light1Pos, 1.0)).xyz;
	vec3 l2p = (gl_ModelViewMatrix * vec4(light2Pos, 1.0)).xyz;
	vec3 l0 = normalize(l0p - p);
	vec3 l1 = normalize(l1p - p);
	vec3 l2 = normalize(l2p - p);
	float l0d = distance(l0p, p);
	float l1d = distance(l1p, p);
	float l2d = distance(l1p, p);
	float l0c = 1.0;
	float l1c = 1.0;
	float l2c = 1.0;

	vec4 mDif;
	vec4 mSpc;
	vec3 mAmb;

	if (texture0Usage == 1)
	{
		mDif = materialDiffuse;
		mSpc = materialSpecular;
		mAmb = materialAmbient;
	}
	else
	{
		mDif = materialDiffuse;
		mSpc = materialSpecular;
		mAmb = materialAmbient;
	}

	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	color.a = mDif.a;
	color.rgb += l0c * max(dot(l0, n), 0.0) * light0Diffuse * mDif.rgb;
	color.rgb += l1c * max(dot(l1, n), 0.0) * light1Diffuse * mDif.rgb;
	color.rgb += l2c * max(dot(l2, n), 0.0) * light2Diffuse * mDif.rgb;
	color.rgb += l0c * pow(min(max(dot(reflect(-l0, n), v), 0.0), 1.0), mSpc.a) * light0Specular * mSpc.rgb;
	color.rgb += l1c * pow(min(max(dot(reflect(-l1, n), v), 0.0), 1.0), mSpc.a) * light1Specular * mSpc.rgb;
	color.rgb += l2c * pow(min(max(dot(reflect(-l2, n), v), 0.0), 1.0), mSpc.a) * light2Specular * mSpc.rgb;
	color.rgb += l0c * light0Ambient * mAmb;
	color.rgb += l1c * light1Ambient * mAmb;
	color.rgb += l2c * light1Ambient * mAmb;

	vec2 sphereST;
	vec3 r = reflect(-v, n);
	float m = 2.0 * sqrt(r.x * r.x + r.y * r.y + (r.z + 1.0) * (r.z + 1.0));
	sphereST = r.xy / m + 0.5;

	vec2 toonST;
	toonST.s = dot(n, l0);
	toonST.t = 1.0 - dot(n, v);

	gl_Position = ftransform();
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	gl_TexCoord[1].st = sphereST;
	gl_TexCoord[2].st = toonST;
	gl_FrontColor = color;
	if (cullBack == 0) gl_BackColor = color;
}
