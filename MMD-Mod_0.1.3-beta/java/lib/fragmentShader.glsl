uniform vec3 multMorphTexture;
uniform vec3 addMorphTexture;
uniform vec3 multMorphSphere;
uniform vec3 addMorphSphere;
uniform vec3 multMorphToon;
uniform vec3 addMorphToon;
uniform sampler2D texture0;
uniform int texture0Usage;
uniform sampler2D texture1;
uniform int texture1Usage;
uniform sampler2D texture2;
uniform int texture2Usage;

void main(void)
{
	vec4 color;

	if (texture0Usage == 1)
	{
		vec4 texColor = texture2D(texture0, gl_TexCoord[0].st);
		texColor.rgb = texColor.rgb * multMorphTexture + addMorphTexture;
		color = gl_Color * texColor;
	}
	else
	{
		color = gl_Color;
	}

	if (texture1Usage == 1)
	{
		color.rgb *= texture2D(texture1, gl_TexCoord[1].st).rgb * multMorphSphere + addMorphSphere;
	}
	else if (texture1Usage == 2)
	{
		color.rgb += texture2D(texture1, gl_TexCoord[1].st).rgb * multMorphSphere + addMorphSphere;
	}

	if (texture2Usage == 1)
	{
		vec4 toonColor = texture2D(texture2, gl_TexCoord[2].st);
		toonColor.rgb = toonColor.rgb * multMorphToon + addMorphToon;
		color.rgb = color.rgb * ((1.0 - toonColor.a) + toonColor.rgb * toonColor.a);
	}

	gl_FragColor = color;
}
