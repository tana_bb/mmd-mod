package tana.modding.mmdmod;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;

/**
 * MMDブロックのGUI用コンテナです。
 * @author tana
 */
public class MMDContainer extends Container
{
	private TileEntityMMDBlock entityMMD;

	public MMDContainer(EntityPlayer player, TileEntityMMDBlock entityMMD)
	{
		super();

		this.entityMMD = entityMMD;

		for (int i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new Slot(player.inventory, i, i * 16, -160));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return entityMMD.isUseableByPlayer(player);
	}
}
