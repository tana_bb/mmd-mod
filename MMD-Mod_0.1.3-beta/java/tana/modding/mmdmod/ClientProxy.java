package tana.modding.mmdmod;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import tana.modding.mmdmod.core.MMDRenderer;
import cpw.mods.fml.client.registry.ClientRegistry;

/**
 * クライアントサイドの処理を実装します。
 * @author tana
 */
public class ClientProxy extends CommonProxy
{
	@Override
	public void init()
	{
		// レンダリングオブジェクトの生成。
		MMDMod.instance.renderer = new MMDRenderer();

		// TileEntityの登録
		ClientRegistry.registerTileEntity(TileEntityMMDBlock.class,
				"mmdmod:tile_entity_mmd_block", new RenderTileEntityMMDBlock());
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		if (ID == MMDMod.instance.mmdGuiID)
		{
			return new MMDGui(player, (TileEntityMMDBlock)world.getTileEntity(x, y, z));
		}

		return null;
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		if (ID == MMDMod.instance.mmdGuiID)
		{
			return new MMDContainer(player, (TileEntityMMDBlock)world.getTileEntity(x, y, z));
		}

		return null;
	}
}
