package tana.modding.mmdmod;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import tana.modding.mmdmod.core.MemoryChecker;
import tana.modding.mmdmod.core.RenderedModelInfo;

/**
 * MMDブロックのGUIです。
 * @author tana
 */
public class MMDGui extends GuiContainer
{
	private static final String strEnablePhysics = "Physics:Enable";
	private static final String strDisablePhysics = "Physics:Disable";
	private static final String strEnableLight = "Lighting:ON";
	private static final String strDisableLight = "Lighting:OFF";
	private static final ResourceLocation bgImage = new ResourceLocation("mmdmod:textures/gui/mmd_bg.png");

	private GuiFileList fileList;
	private GuiTextField textPMX;
	private GuiTextField textVMD1;
	private GuiTextField textVMD2;
	private GuiTextField textAudio;
	private GuiTextField textModelHeight;
	private GuiTextField textSpeed;
	private GuiTextField textVolume;
	private GuiButton buttonPMX;
	private GuiButton buttonVMD1;
	private GuiButton buttonVMD2;
	private GuiButton buttonEnablePhysics;
	private GuiButton buttonEnableLight;
	private GuiButton buttonAudio;
	private GuiButton buttonApply;
	private GuiButton buttonCancel;
	/**
	 * クリックされたボタンを記録します。
	 */
	private GuiButton clicked = null;

	private TileEntityMMDBlock entityMMD;

	public MMDGui(EntityPlayer player, TileEntityMMDBlock entityMMD)
	{
		super(new MMDContainer(player, entityMMD));

		// TileEntityMMDBlockを登録
		this.entityMMD = entityMMD;
	}

	@Override
	public void initGui()
	{
		super.initGui();

		// 位置を左上に設定
		this.guiLeft = 0;
		this.guiTop = 0;

		// キーボードイベントの初期化
		Keyboard.enableRepeatEvents(true);

		// GuiSlotの初期化
		this.fileList = new GuiFileList(this.mc,
				(int)(this.width * 0.5F), this.height, (int)(this.height * 0.1F), (int)(this.height * 0.85F), 20);
		this.fileList.registerScrollButtons(10, 11);

		// GuiTextFieldの初期化
		this.textPMX = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.55F), (int)(this.height * 0.07F), (int)(this.width * 0.3F), 16);
		this.textVMD1 = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.55F), (int)(this.height * 0.22F), (int)(this.width * 0.3F), 16);
		this.textVMD2 = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.55F), (int)(this.height * 0.37F), (int)(this.width * 0.3F), 16);
		this.textAudio = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.55F), (int)(this.height * 0.52F), (int)(this.width * 0.3F), 16);
		this.textModelHeight = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.55F), (int)(this.height * 0.67F), (int)(this.width * 0.10F), 16);
		this.textSpeed = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.70F), (int)(this.height * 0.67F), (int)(this.width * 0.10F), 16);
		this.textVolume = new GuiTextField(this.fontRendererObj,
				(int)(this.width * 0.85F), (int)(this.height * 0.67F), (int)(this.width * 0.10F), 16);
		this.textPMX.setMaxStringLength(1024);
		this.textVMD1.setMaxStringLength(1024);
		this.textVMD2.setMaxStringLength(1024);
		this.textAudio.setMaxStringLength(1024);
		this.textModelHeight.setMaxStringLength(1024);
		this.textSpeed.setMaxStringLength(1024);
		this.textVolume.setMaxStringLength(1024);

		// GuiButtonの初期化
		this.buttonPMX = new GuiButton(1,
				(int)(this.width * 0.88F), (int)(this.height * 0.07F) - 2, (int)(this.width * 0.1F), 20, "Select");
		this.buttonVMD1 = new GuiButton(2,
				(int)(this.width * 0.88F), (int)(this.height * 0.22F) - 2, (int)(this.width * 0.1F), 20, "Select");
		this.buttonVMD2 = new GuiButton(3,
				(int)(this.width * 0.88F), (int)(this.height * 0.37F) - 2, (int)(this.width * 0.1F), 20, "Select");
		this.buttonAudio = new GuiButton(4,
				(int)(this.width * 0.88F), (int)(this.height * 0.52F) - 2, (int)(this.width * 0.1F), 20, "Select");
		this.buttonEnablePhysics = new GuiButton(5,
				(int)(this.width * 0.52F), (int)(this.height * 0.77F) - 2,
				(int)(this.width * 0.25F), 20, strDisablePhysics);
		this.buttonEnableLight = new GuiButton(6,
				(int)(this.width * 0.8F), (int)(this.height * 0.77F) - 2,
				(int)(this.width * 0.17F), 20, strDisableLight);

		this.buttonApply = new GuiButton(0,
				(int)(this.width * 0.08F), (int)(this.height * 0.9F) - 2, (int)(this.width * 0.4F), 20, "Apply");
		this.buttonCancel = new GuiButton(9,
				(int)(this.width * 0.52F), (int)(this.height * 0.9F) - 2, (int)(this.width * 0.4F), 20, "Cancel");

		this.buttonList.clear();
		this.buttonList.add(this.buttonPMX);
		this.buttonList.add(this.buttonVMD1);
		this.buttonList.add(this.buttonVMD2);
		this.buttonList.add(this.buttonAudio);
		this.buttonList.add(this.buttonApply);
		this.buttonList.add(this.buttonCancel);
		this.buttonList.add(this.buttonEnablePhysics);
		this.buttonList.add(this.buttonEnableLight);

		// GUI部品の値の初期化
		initValues();
	}

	/**
	 * GUI部品の値の初期化です。
	 */
	private void initValues()
	{
		if (this.entityMMD.info != null)
		{
			this.textPMX.setText(this.entityMMD.info.pmxPath);
			this.textVMD1.setText(this.entityMMD.info.vmd1Path);
			this.textVMD2.setText(this.entityMMD.info.vmd2Path);
			this.textAudio.setText(this.entityMMD.info.audioPath);
			this.textModelHeight.setText(Float.toString(this.entityMMD.info.modelHeight));
			this.textSpeed.setText(Float.toString(this.entityMMD.info.speed));
			this.textVolume.setText(Float.toString(this.entityMMD.info.volume));
			this.buttonEnablePhysics.displayString =
					this.entityMMD.info.enablePhysics ? strEnablePhysics : strDisablePhysics;
			this.buttonEnableLight.displayString =
					this.entityMMD.info.enableLight ? strEnableLight : strDisableLight;
		}
		else
		{
			this.textModelHeight.setText("1.58");
			this.textSpeed.setText("1.0");
			this.textVolume.setText("0.6");
		}
	}

	@Override
	public void onGuiClosed()
	{
		// 終了処理
		Keyboard.enableRepeatEvents(false);
		super.onGuiClosed();
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button == this.buttonPMX)
		{
			// GuiSlotにPMXファイルを列挙
			this.clicked = button;
			this.fileList.list.clear();
			this.fileList.list.add(null);
			this.fileList.currentIndex = -1;
			this.findFiles(new File(MMDMod.dirPath), "pmx");
		}
		else if (button == this.buttonVMD1)
		{
			// GuiSlotにVMDファイルを列挙
			this.clicked = button;
			this.fileList.list.clear();
			this.fileList.list.add(null);
			this.fileList.currentIndex = -1;
			this.findFiles(new File(MMDMod.dirPath), "vmd");
		}
		else if (button == this.buttonVMD2)
		{
			// GuiSlotにVMDファイルを列挙
			this.clicked = button;
			this.fileList.list.clear();
			this.fileList.list.add(null);
			this.fileList.currentIndex = -1;
			this.findFiles(new File(MMDMod.dirPath), "vmd");
		}
		else if (button == this.buttonAudio)
		{
			// GuiSlotにWAVファイルを列挙
			this.clicked = button;
			this.fileList.list.clear();
			this.fileList.list.add(null);
			this.fileList.currentIndex = -1;
			this.findFiles(new File(MMDMod.dirPath), "wav");
		}
		else if (button == this.buttonEnablePhysics)
		{
			// 物理演算の有効無効を切り替え
			if (this.buttonEnablePhysics.displayString.equals(strEnablePhysics))
			{
				this.buttonEnablePhysics.displayString = strDisablePhysics;
			}
			else
			{
				this.buttonEnablePhysics.displayString = strEnablePhysics;
			}
		}
		else if (button == buttonEnableLight)
		{
			// ライティングの有効無効を切り替え
			if (this.buttonEnableLight.displayString.equals(strEnableLight))
			{
				this.buttonEnableLight.displayString = strDisableLight;
			}
			else
			{
				this.buttonEnableLight.displayString = strEnableLight;
			}
		}
		else if (button == buttonApply)
		{
			// モデルデータの読み込み
			if (this.entityMMD.info == null)
			{
				this.entityMMD.info = new RenderedModelInfo();
			}

			this.entityMMD.info.pmxPath = this.textPMX.getText();
			this.entityMMD.info.vmd1Path = this.textVMD1.getText();
			this.entityMMD.info.vmd2Path = this.textVMD2.getText();
			this.entityMMD.info.audioPath = this.textAudio.getText();

			try
			{
				this.entityMMD.info.modelHeight = Float.parseFloat(this.textModelHeight.getText());
			}
			catch (NumberFormatException e)
			{
				this.entityMMD.info.modelHeight = 1.58F;
			}
			if (this.entityMMD.info.modelHeight < 0.1F)
			{
				this.entityMMD.info.modelHeight = 0.1F;
			}
			else if (this.entityMMD.info.modelHeight > 200.0F)
			{
				this.entityMMD.info.modelHeight = 200.0F;
			}

			try
			{
				this.entityMMD.info.speed = Float.parseFloat(this.textSpeed.getText());
			}
			catch (NumberFormatException e)
			{
				this.entityMMD.info.speed = 1.0F;
			}
			if (this.entityMMD.info.speed < 0.0F)
			{
				this.entityMMD.info.speed = 0.0F;
			}

			try
			{
				this.entityMMD.info.volume = Float.parseFloat(this.textVolume.getText());
			}
			catch (NumberFormatException e)
			{
				this.entityMMD.info.volume = 0.6F;
			}
			if (this.entityMMD.info.volume < 0.0F)
			{
				this.entityMMD.info.volume = 0.0F;
			}
			else if (this.entityMMD.info.volume > 1.0F)
			{
				this.entityMMD.info.volume = 1.0F;
			}

			this.entityMMD.info.enablePhysics = this.buttonEnablePhysics.displayString.equals(strEnablePhysics);
			this.entityMMD.info.enableLight = this.buttonEnableLight.displayString.equals(strEnableLight);

			// メモリ容量をチェック
			if (MemoryChecker.checkMemorySizeWithGC(this.entityMMD.info))
			{
				// モデルデータの読み込み
				this.entityMMD.initModelData();

				// GUIの終了
				this.mc.displayGuiScreen((GuiScreen)null);
				this.mc.setIngameFocus();
			}
			else
			{
				// メモリ不足の場合
				this.buttonApply.displayString = "Out of Memory!";
			}
		}
		else if (button == buttonCancel)
		{
			// GUIの終了
			this.mc.displayGuiScreen((GuiScreen)null);
            this.mc.setIngameFocus();
		}

		this.fileList.actionPerformed(button);
		super.actionPerformed(button);
	}

	@Override
	protected void mouseClicked(int x, int y, int button)
	{
		this.textPMX.mouseClicked(x, y, button);
		this.textVMD1.mouseClicked(x, y, button);
		this.textVMD2.mouseClicked(x, y, button);
		this.textAudio.mouseClicked(x, y, button);
		this.textModelHeight.mouseClicked(x, y, button);
		this.textSpeed.mouseClicked(x, y, button);
		this.textVolume.mouseClicked(x, y, button);
		super.mouseClicked(x, y, button);
	}

	@Override
	protected void keyTyped(char par1, int par2)
	{
		this.textPMX.textboxKeyTyped(par1, par2);
		this.textVMD1.textboxKeyTyped(par1, par2);
		this.textVMD2.textboxKeyTyped(par1, par2);
		this.textAudio.textboxKeyTyped(par1, par2);
		this.textModelHeight.textboxKeyTyped(par1, par2);
		this.textSpeed.textboxKeyTyped(par1, par2);
		this.textVolume.textboxKeyTyped(par1, par2);
		super.keyTyped(par1, par2);
	}

	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		super.drawScreen(par1, par2, par3);

		this.fontRendererObj.drawString("Model File (.pmx)",
				(int)(this.width * 0.55F), (int)(this.height * 0.07F) - 10, 0x000000);
		this.fontRendererObj.drawString("Idling Motion File (.vmd)",
				(int)(this.width * 0.55F), (int)(this.height * 0.22F) - 10, 0x000000);
		this.fontRendererObj.drawString("Active Motion File (.vmd)",
				(int)(this.width * 0.55F), (int)(this.height * 0.37F) - 10, 0x000000);
		this.fontRendererObj.drawString("Audio File (.wav)",
				(int)(this.width * 0.55F), (int)(this.height * 0.52F) - 10, 0x000000);
		this.fontRendererObj.drawString("Height (m)",
				(int)(this.width * 0.55F), (int)(this.height * 0.67F) - 10, 0x000000);
		this.fontRendererObj.drawString("Speed",
				(int)(this.width * 0.70F), (int)(this.height * 0.67F) - 10, 0x000000);
		this.fontRendererObj.drawString("Volume",
				(int)(this.width * 0.85F), (int)(this.height * 0.67F) - 10, 0x000000);

		this.fileList.drawScreen(par1, par2, par3);
		this.textPMX.drawTextBox();
		this.textVMD1.drawTextBox();
		this.textVMD2.drawTextBox();
		this.textAudio.drawTextBox();
		this.textModelHeight.drawTextBox();
		this.textSpeed.drawTextBox();
		this.textVolume.drawTextBox();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3)
	{
		// 背景の描画
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(bgImage);
		GL11.glEnable(GL11.GL_BLEND);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(0.0, 0.0, this.zLevel, 0.0, 0.0);
		tessellator.addVertexWithUV(0.0, this.height, this.zLevel, 0.0, 1.0);
		tessellator.addVertexWithUV(this.width, this.height, this.zLevel, 1.0, 1.0);
		tessellator.addVertexWithUV(this.width, 0.0, this.zLevel, 1.0, 0.0);
		tessellator.draw();
		GL11.glDisable(GL11.GL_BLEND);
	}

	@Override
	public void drawWorldBackground(int par1)
	{
		if (this.mc.theWorld != null)
		{
			this.drawGradientRect(0, 0, this.width, this.height, 0x00000000, 0x00000000);
		}
		else
		{
			this.drawBackground(par1);
		}
	}

	/**
	 * 指定されたディレクトリ下の、指定された拡張子のファイルをGuiSlotに登録します。
	 * @param current 探索するディレクトリ。
	 * @param suffix 探索する拡張子。
	 */
	private void findFiles(File current, String suffix)
	{
		try
		{
			if (current.exists())
			{
				if (current.isDirectory())
				{
					File[] files = current.listFiles();
					if (files != null)
					{
						for (File file : files)
						{
							findFiles(file, suffix);
						}
					}
				}
				else
				{
					String path = current.toString();
					int index = path.lastIndexOf(".") + 1;
					if (index >= 0 && path.substring(index).equalsIgnoreCase(suffix))
					{
						this.fileList.list.add(current);
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * ファイルを列挙するGuiSlotです。
	 * @author tana
	 */
	public class GuiFileList extends GuiSlot_Custom
	{
		/**
		 * 表示するファイルのリストです。
		 */
		public List<File> list = new ArrayList<File>();
		/**
		 * 選択中の添字です。
		 */
		public int currentIndex = -1;

		public GuiFileList(Minecraft mc, int width, int height, int top, int bottom, int slotHeight)
		{
			super(mc, width, height, top, bottom, slotHeight);
		}

		@Override
		protected int getSize()
		{
			return list.size();
		}

		@Override
		protected void elementClicked(int var1, boolean var2, int var3, int var4)
		{
			this.currentIndex = var1;

			// クリック時、TextFieldに値をセット
			if (MMDGui.this.clicked == MMDGui.this.buttonPMX)
			{
				File file = this.list.get(this.currentIndex);
				String path = file != null ? file.getAbsolutePath() : "";
				MMDGui.this.textPMX.setText(path);
			}
			else if (MMDGui.this.clicked == MMDGui.this.buttonVMD1)
			{
				File file = this.list.get(this.currentIndex);
				String path = file != null ? file.getAbsolutePath() : "";
				MMDGui.this.textVMD1.setText(path);
			}
			else if (MMDGui.this.clicked == MMDGui.this.buttonVMD2)
			{
				File file = this.list.get(this.currentIndex);
				String path = file != null ? file.getAbsolutePath() : "";
				MMDGui.this.textVMD2.setText(path);
			}
			else if (MMDGui.this.clicked == MMDGui.this.buttonAudio)
			{
				File file = this.list.get(this.currentIndex);
				String path = file != null ? file.getAbsolutePath() : "";
				MMDGui.this.textAudio.setText(path);
			}
		}

		@Override
		protected boolean isSelected(int var1)
		{
			return this.currentIndex == var1;
		}

		@Override
		protected void drawBackground()
		{
		}

		@Override
		protected void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5, int var6, int var7)
		{
			File file = list.get(var1);
			String label = file != null ? file.getName() : "-";
			if (label.length() > 25)
			{
				label = label.substring(0, 25);
			}
			MMDGui.this.fontRendererObj.drawString(label,
					(int)(this.width * 0.5F) - MMDGui.this.fontRendererObj.getStringWidth(label) / 2, var3 + 4,
					0x00000000);
		}

		@Override
		protected void overlayBackground(int var1, int var2, int var3, int var4)
		{
		}

		@Override
		protected void drawContainerBackground(Tessellator tessellator)
		{
		}

		@Override
		public int getListWidth()
		{
			return (int)(this.width * 0.95F);
		}

		@Override
		protected int getScrollBarX()
		{
			return (int)(this.width - 10);
		}
	}
}
