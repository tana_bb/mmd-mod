package tana.modding.mmdmod;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import tana.modding.mmdmod.core.AudioPlayer;
import tana.modding.mmdmod.core.RenderedModelInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * MMDブロックのTileEntityです。
 * @author tana
 */
public class TileEntityMMDBlock extends TileEntity
{
	/**
	 * worldObj.isRemote==trueのオブジェクトを格納します。
	 */
	private static Map<Position, TileEntityMMDBlock> remotePool = new HashMap<Position, TileEntityMMDBlock>();
	/**
	 * worldObj.isRemote==falseのオブジェクトを格納します。
	 */
	private static Map<Position, TileEntityMMDBlock> unremotePool = new HashMap<Position, TileEntityMMDBlock>();

	/**
	 * ワールド座標が同じ、対となるオブジェクトを格納します。
	 */
	private volatile TileEntityMMDBlock pair = null;

	/**
	 * モデルに関する情報を格納します。
	 */
	public volatile RenderedModelInfo info = null;
	/**
	 * RedPowerのon/off状態です。
	 */
	private volatile boolean redPowerOn = false;
	/**
	 * info.vmdRatioのステップです。
	 */
	private volatile float tStep = 0.0F;
	/**
	 * オーディオ再生オブジェクトです。
	 */
	private volatile AudioPlayer audio = null;

	public TileEntityMMDBlock()
	{
		super();
	}

	@Override
	protected void finalize() throws Throwable
	{
		// オーディオを停止
		if (this.audio != null)
		{
			this.audio.stop();
		}

		super.finalize();
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);

		boolean enable = tag.getBoolean("enable");

		// NBTTagからinfoの情報を読み込み
		if (enable)
		{
			this.info = new RenderedModelInfo();
			this.info.enable = true;
			this.info.modelIndex = -1;
			this.info.pmxPath = tag.getString("pmxPath");
			this.info.vmd1Path = tag.getString("vmd1Path");
			this.info.vmd2Path = tag.getString("vmd2Path");
			this.info.audioPath = tag.getString("audioPath");
			this.info.side = tag.getInteger("side");
			this.info.modelHeight = tag.getFloat("modelHeight");
			this.info.speed = tag.getFloat("speed");
			this.info.volume = tag.getFloat("volume");
			this.info.enablePhysics = tag.getBoolean("enablePhysics");
			this.info.enableLight = tag.getBoolean("enableLight");

			this.registerUnremotePool();
		}
		else
		{
			this.info = null;
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);

		// NBTTagにinfoの情報を書き込み
		if (!this.worldObj.isRemote)
		{
			if (this.info != null)
			{
				tag.setBoolean("enable", this.info.enable);
				tag.setInteger("modelIndex", this.info.modelIndex);
				tag.setString("pmxPath", this.info.pmxPath);
				tag.setString("vmd1Path", this.info.vmd1Path);
				tag.setString("vmd2Path", this.info.vmd2Path);
				tag.setString("audioPath", this.info.audioPath);
				tag.setInteger("side", this.info.side);
				tag.setFloat("modelHeight", this.info.modelHeight);
				tag.setFloat("speed", this.info.speed);
				tag.setFloat("volume", this.info.volume);
				tag.setBoolean("enablePhysics", this.info.enablePhysics);
				tag.setBoolean("enableLight", this.info.enableLight);
			}
			else
			{
				tag.setBoolean("enable", false);
				tag.setInteger("modelIndex", -1);
				tag.setString("pmxPath", "");
				tag.setString("vmd1Path", "");
				tag.setString("vmd2Path", "");
				tag.setString("audioPath", "");
				tag.setInteger("side", -1);
				tag.setFloat("modelHeight", 0.0F);
				tag.setFloat("speed", 0.0F);
				tag.setFloat("volume", 0.0F);
				tag.setBoolean("enablePhysics", false);
				tag.setBoolean("enableLight", false);
			}
		}
	}

	@Override
	public void updateEntity()
	{
		super.updateEntity();

		// リモートのオブジェクト
		if (this.worldObj.isRemote)
		{
			// 対となるオブジェクトを探す
			if (this.pair == null)
			{
				synchronized (TileEntityMMDBlock.class)
				{
					Position p = new Position(this.xCoord, this.yCoord, this.zCoord);
					this.pair = unremotePool.get(p);

					if (this.pair != null)
					{
						unremotePool.remove(p);
					}
				}
			}

			// 対となるオブジェクトに情報を記録
			if (this.pair != null)
			{
				if (this.pair.pair == null)
				{
					this.pair.pair = this;
				}

				if (this.info == null)
				{
					this.info = this.pair.info;
					this.audio = this.pair.audio;

					this.markDirty();
					this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
				}
			}

			if (this.info != null)
			{
				synchronized (TileEntityMMDBlock.class)
				{
					// info.vmdRatioにステップを加算
					if (this.tStep != 0.0F)
					{
						this.info.vmdRatio += this.tStep;

						if (this.info.vmdRatio < 0.0F)
						{
							this.info.vmdRatio = 0.0F;
							this.tStep = 0.0F;
						}
						else if (this.info.vmdRatio > 1.0F)
						{
							this.info.vmdRatio = 1.0F;
							this.tStep = 0.0F;
						}

						this.markDirty();
						this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
					}

					// info.lightRatioを計算
					if (this.info.enableLight)
					{
						this.info.lightRatio = 1.0F;
					}
					else
					{
						this.info.lightRatio = this.getLightRatio();
					}
				}
			}
		}
		// リモートでないオブジェクト
		else
		{
			// 対となるオブジェクトを探す
			if (this.pair == null)
			{
				synchronized (TileEntityMMDBlock.class)
				{
					Position p = new Position(this.xCoord, this.yCoord, this.zCoord);
					this.pair = remotePool.get(p);

					if (this.pair != null)
					{
						remotePool.remove(p);
					}
				}
			}

			// 対となるオブジェクトに情報を記録
			if (this.pair != null)
			{
				if (this.pair.pair == null)
				{
					this.pair.pair = this;
				}

				if (this.info == null)
				{
					this.info = this.pair.info;
					this.audio = this.pair.audio;

					this.markDirty();
					this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
				}
			}
		}
	}

	@Override
	public void onChunkUnload()
	{
		this.destroy(new RenderedModelInfo(this.info));
		super.onChunkUnload();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public double getMaxRenderDistanceSquared()
	{
		// レンダリング距離を無限に設定
		return Double.POSITIVE_INFINITY;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getRenderBoundingBox()
	{
		// レンダリングBoundingBoxを無限に設定
		return TileEntity.INFINITE_EXTENT_AABB;
	}

	/**
	 * GUIの有効距離を算出。
	 * @param par1EntityPlayer
	 * @return 有効かどうか。
	 */
	public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
	{
		return this.worldObj.getBlock(this.xCoord, this.yCoord, this.zCoord) != MMDMod.instance.mmdBlock ? false
				: par1EntityPlayer.getDistanceSq(
						(double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
	}

	/**
	 * モデルデータを初期化します。
	 */
	public void initModelData()
	{
		// 既存のinfoを破棄
		if (this.info != null && this.info.modelIndex >= 0)
		{
			destroy(new RenderedModelInfo(this.info));
		}

		if (this.info != null)
		{
			// 新規モデルインデックスを取得
			synchronized (TileEntityMMDBlock.class)
			{
				this.info.enable = true;
				this.info.modelIndex = MMDMod.instance.renderer.createModelIndex();
				this.info.side = this.getBlockMetadata();
			}

			if (this.info.modelIndex >= 0)
			{
				// initModelPendingsに登録
				synchronized (RenderTileEntityMMDBlock.class)
				{
					RenderTileEntityMMDBlock.initModelPendings.add(this.info);
				}

				// オーディオデータを初期化
				synchronized (TileEntityMMDBlock.class)
				{
					this.audio = new AudioPlayer();
					this.audio.load(this.info.audioPath, this.info.volume);

					MMDMod.instance.audioList.add(this.audio);
				}
			}

			if (this.pair != null)
			{
				// 対となるオブジェクトに情報を設定
				this.pair.info = this.info;
				this.pair.audio = this.audio;
			}
			else
			{
				// remotePoolに登録
				registerRemotePool();
			}

			this.markDirty();
			this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
		}
	}

	/**
	 * モデルデータを破棄します。
	 * @param info 破棄するinfo（this.infoをコピーして渡すこと）。
	 */
	public void destroy(RenderedModelInfo info)
	{
		if (info != null)
		{
			// destroyPendingsに登録
			synchronized (RenderTileEntityMMDBlock.class)
			{
				RenderTileEntityMMDBlock.destroyPendings.add(info);
			}

			if (this.audio != null)
			{
				// オーディオを破棄
				synchronized (TileEntityMMDBlock.class)
				{
					this.audio.stop();

					MMDMod.instance.audioList.remove(this.audio);
					this.audio = null;
				}
			}
		}
	}

	/**
	 * モーション時間をリセットします。
	 * @param vmdIndex IdlingおよびActive状態を示す添字
	 */
	public void resetTime(int vmdIndex)
	{
		if (this.info != null)
		{
			// resetTimePendingsに登録
			synchronized (RenderTileEntityMMDBlock.class)
			{
				if (vmdIndex == 0)
				{
					this.info.resetTime1 = true;
					RenderTileEntityMMDBlock.resetTimePendings.add(this.info);
				}
				else if (vmdIndex == 1)
				{
					this.info.resetTime2 = true;
					RenderTileEntityMMDBlock.resetTimePendings.add(this.info);
				}
			}
		}
	}

	/**
	 * RedPowerのon/offを設定します。
	 * @param on true:on、false:off。
	 */
	public void setRedPower(boolean on)
	{
		if (this.info != null && this.pair != null)
		{
			synchronized (TileEntityMMDBlock.class)
			{
				// offからonになる処理
				if (!this.redPowerOn && on)
				{
					this.redPowerOn = true;
					this.pair.tStep = 0.05F;
					resetTime(1);

					if (this.audio != null)
					{
						this.audio.start();
					}
				}
				// onからoffになる処理
				else if (this.redPowerOn && !on)
				{
					this.redPowerOn = false;
					this.pair.tStep = -0.05F;
					resetTime(0);

					if (this.audio != null)
					{
						this.audio.stop();
					}
				}
			}
		}
	}

	/**
	 * remotePoolに登録します。
	 */
	private void registerRemotePool()
	{
		synchronized (TileEntityMMDBlock.class)
		{
			remotePool.put(new Position(this.xCoord, this.yCoord, this.zCoord), this);
		}
	}

	/**
	 * unremotePoolに登録します。
	 */
	private void registerUnremotePool()
	{
		synchronized (TileEntityMMDBlock.class)
		{
			unremotePool.put(new Position(this.xCoord, this.yCoord, this.zCoord), this);
		}
	}

	/**
	 * lightRatioを計算します。
	 * @return lightRatioの値。
	 */
	private float getLightRatio()
	{
		float t = ((this.worldObj.getWorldTime() + 18000) % 24000) / 24000.0F;

		float light = ((float)Math.cos(t * 2.0 * Math.PI) * 0.3F + 0.7F)
				* this.worldObj.getBlockLightValue(this.xCoord, this.yCoord + 1, this.zCoord) / 15.0F;

		return light >= 0.3F ? light : 0.3F;
	}
}
