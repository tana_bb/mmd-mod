package tana.modding.mmdmod;

/**
 * ワールド座標におけるブロックの位置です。
 * @author tana
 */
public class Position
{
	public int x = 0;
	public int y = 0;
	public int z = 0;

	public Position(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Position)
		{
			Position p = (Position)obj;

			return this.x == p.x && this.y == p.y && this.z == p.z;
		}
		else
		{
			return false;
		}
	}

	@Override
	public int hashCode()
	{
		return this.x * 961 + this.y * 31 + this.z;
	}
}
