package tana.modding.mmdmod;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import tana.modding.mmdmod.core.RenderedModelInfo;

/**
 * TileEntityMMDBlockのレンダリング処理を実装します。
 * @author tana
 */
public class RenderTileEntityMMDBlock extends TileEntitySpecialRenderer
{
	/**
	 * MMDRenderer.initModelData()のペンディングです。
	 */
	public static List<RenderedModelInfo> initModelPendings = new ArrayList<RenderedModelInfo>();
	/**
	 * MMDRenderer.releaseModelIndex()のペンディングです。
	 */
	public static List<RenderedModelInfo> destroyPendings = new ArrayList<RenderedModelInfo>();
	/**
	 * MMDRenderer.resetTime()のペンディングです。
	 */
	public static List<RenderedModelInfo> resetTimePendings = new ArrayList<RenderedModelInfo>();

	@Override
	public void renderTileEntityAt(TileEntity t, double x, double y, double z, float f)
	{
		// リリース処理
		synchronized (RenderTileEntityMMDBlock.class)
		{
			int dSize = destroyPendings.size();
			for (int i = 0; i < dSize; ++i)
			{
				RenderedModelInfo info = destroyPendings.get(0);
				MMDMod.instance.renderer.releaseModelIndex(info.modelIndex);
				info.modelIndex = -1;
				destroyPendings.remove(0);
			}
		}

		// モデル初期化処理
		synchronized (RenderTileEntityMMDBlock.class)
		{
			int iSize = initModelPendings.size();
			for (int i = 0; i < iSize; ++i)
			{
				RenderedModelInfo info = initModelPendings.get(0);
				MMDMod.instance.renderer.initModelData(
						info.modelIndex, info.pmxPath, info.vmd1Path, info.vmd2Path, info.modelHeight, info.speed);
				initModelPendings.remove(0);
			}
		}

		// タイマリセット処理
		synchronized (RenderTileEntityMMDBlock.class)
		{
			int rSize = resetTimePendings.size();
			for (int i = 0; i < rSize; ++i)
			{
				RenderedModelInfo info = resetTimePendings.get(0);
				if (info.resetTime1)
				{
					MMDMod.instance.renderer.resetTime(info.modelIndex, 0);
					info.resetTime1 = false;
				}
				if (info.resetTime2)
				{
					MMDMod.instance.renderer.resetTime(info.modelIndex, 1);
					info.resetTime2 = false;
				}
				resetTimePendings.remove(0);
			}
		}

		// TileEntityの描画
		TileEntityMMDBlock mmd = (TileEntityMMDBlock)t;
		if (mmd.info != null && mmd.info.modelIndex >= 0)
		{
			MMDMod.instance.renderer.update(
					mmd.info.modelIndex, (float)x, (float)y, (float)z,
					mmd.info.side, mmd.info.vmdRatio, mmd.info.enablePhysics, mmd.info.lightRatio);
		}
	}
}
