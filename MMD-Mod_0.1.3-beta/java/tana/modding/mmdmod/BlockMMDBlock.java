package tana.modding.mmdmod;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import tana.modding.mmdmod.core.RenderedModelInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * MMDブロックです。
 * @author tana
 */
public class BlockMMDBlock extends BlockContainer
{
	@SideOnly(Side.CLIENT)
    private IIcon icon_top;
	@SideOnly(Side.CLIENT)
    private IIcon icon_face;

	public BlockMMDBlock()
	{
		super(Material.wood);
		this.setCreativeTab(CreativeTabs.tabDecorations);
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z,
			EntityPlayer entityPlayer, int side, float par7, float par8, float par9)
	{
		// GUIを開く
		if (!world.isRemote)
		{
			entityPlayer.openGui(MMDMod.instance, MMDMod.instance.mmdGuiID, world, x, y, z);
		}
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase par5, ItemStack par6)
	{
		// ブロックの向きを計算
		int l = MathHelper.floor_double((double)(par5.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3;
		world.setBlockMetadataWithNotify(x, y, z, l, 2);
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int par6)
	{
		// TileEntityの破棄処理を実行
		if (!world.isRemote)
		{
			TileEntityMMDBlock mmd = (TileEntityMMDBlock)world.getTileEntity(x, y, z);
			mmd.destroy(new RenderedModelInfo(mmd.info));
		}

		super.breakBlock(world, x, y, z, block, par6);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		// RedPowerの処理
		if(!world.isRemote)
		{
			((TileEntityMMDBlock)world.getTileEntity(x, y, z)).setRedPower(
					world.isBlockIndirectlyGettingPowered(x, y, z));
		}
	}

	@Override
	public Item getItemDropped(int par1, Random par2, int par3)
    {
        return Item.getItemFromBlock(MMDMod.instance.mmdBlock);
    }

	@Override
	public TileEntity createNewTileEntity(World world, int par2)
	{
		return new TileEntityMMDBlock();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		// ブロックの向きによってアイコンを変更
		return side == 1 ? this.icon_top : (side == 0 ? this.icon_top :
			(meta == 2 && side == 2 ? this.icon_face :
				(meta == 3 && side == 5 ? this.icon_face :
					(meta == 0 && side == 3 ? this.icon_face :
						(meta == 1 && side == 4 ? this.icon_face : this.blockIcon)))));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1)
	{
		// アイコンの登録
		this.icon_face = par1.registerIcon(this.getTextureName() + "_face");
		this.icon_top = par1.registerIcon(this.getTextureName() + "_top");
		this.blockIcon = par1.registerIcon(this.getTextureName() + "_side");
	}
}
