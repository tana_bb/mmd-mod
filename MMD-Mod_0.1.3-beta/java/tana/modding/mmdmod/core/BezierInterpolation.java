package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector2f;

/**
 * VMDで使用される、ベジエ曲線による補間の機能を提供します。
 * @author tana
 */
public class BezierInterpolation
{
	private static final int bufSize = 32;

	private Vector2f p1;
	private Vector2f p2;
	private float[] map;

	/**
	 * コンストラクタです。
	 * @param p1 ベジエ曲線の点1。
	 * @param p2 ベジエ曲線の点2。
	 */
	public BezierInterpolation(Vector2f p1, Vector2f p2)
	{
		this.p1 = new Vector2f(p1);
		this.p2 = new Vector2f(p2);

		map = new float[bufSize];

		initMap();
	}

	/**
	 * 内部で使用されるベジエ曲線のマップを構築します。
	 */
	private void initMap()
	{
		int index = -1;
		float step = 1.0F / (bufSize * 4);
		for (float t = 0.0F; t <= 1.0F; t += step)
		{
			float ti = 1.0F - t;
			float x = (p1.x * 3.0F * t * ti * ti + p2.x * 3.0F * t * t * ti + 127.0F * t * t * t)
					/ 127.0F * (bufSize - 1);
			int xf = (int)Math.floor(x);
			if (index < xf)
			{
				index = xf;
				map[index] = (p1.y * 3.0F * t * ti * ti + p2.y * 3.0F * t * t * ti + 127.0F * t * t * t) / 127.0F;
			}
		}
		map[bufSize - 1] = 1.0F;
	}

	/**
	 * フレームpos地点の値を、startとendの間を補間して求めます。
	 * @param pos 値を求める対象のフレーム番号。
	 * @param start 補間開始地点のフレーム番号。
	 * @param end 補間終了地点のフレーム番号。
	 * @param startValue 補間開始地点の値。
	 * @param endValue 補間終了地点の値。
	 * @return フレームpos地点の値。
	 */
	public float getValue(int pos, int start, int end, float startValue, float endValue)
	{
		if (end == start)
		{
			return endValue;
		}

		float index =  (bufSize - 1) * (float)(pos - start) / (float)(end - start);
		int index0 = (int)Math.floor(index);
		int index1 = index0 + 1;
		float index_d = index - index0;

		float ratio;

		if (index0 < bufSize - 1)
		{
			ratio = (1.0F - index_d) * map[index0] + index_d * map[index1];
		}
		else
		{
			ratio = map[bufSize - 1];
		}

		return (1.0F - ratio) * startValue + ratio * endValue;
	}

	/**
	 * フレームpos地点の値を、startとendの間を補間して求めます。
	 * 値の計算はクォータニオンを球面補間して行ないます。
	 * @param pos 値を求める対象のフレーム番号。
	 * @param start 補間開始地点のフレーム番号。
	 * @param end 補間終了地点のフレーム番号。
	 * @param startValue 補間開始地点の値。
	 * @param endValue 補間終了地点の値。
	 * @param dest フレームpos地点の値が返される。
	 * @return destと同じインスタンス。
	 */
	public Quaternion getValue(int pos, int start, int end,
			Quaternion startValue, Quaternion endValue, Quaternion dest)
	{
		if (dest == null)
		{
			dest = new Quaternion();
		}

		if (end == start)
		{
			dest.w = endValue.w;
			dest.x = endValue.x;
			dest.y = endValue.y;
			dest.z = endValue.z;
			return dest;
		}

		float index =  (bufSize - 1) * (float)(pos - start) / (float)(end - start);
		int index0 = (int)Math.floor(index);
		int index1 = index0 + 1;
		float index_d = index - index0;

		float ratio;

		if (index0 < bufSize - 1)
		{
			ratio = (1.0F - index_d) * map[index0] + index_d * map[index1];
		}
		else
		{
			ratio = map[bufSize - 1];
		}

		Util.slerp(startValue, endValue, ratio, dest);

		return dest;
	}
}
