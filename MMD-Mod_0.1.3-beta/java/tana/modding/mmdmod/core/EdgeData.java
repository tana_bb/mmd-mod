package tana.modding.mmdmod.core;


/**
 * PMXのモデルから構築された辺のデータです。
 * @author tana
 */
public class EdgeData
{
	public int vertexIndex1 = -1;
	public int vertexIndex2 = -1;

	public int index = -1;
	public VertexData vertex1 = null;
	public VertexData vertex2 = null;
	public FaceData face1 = null;
	public FaceData face2 = null;
	public float edgeScale = 0.0F;
}
