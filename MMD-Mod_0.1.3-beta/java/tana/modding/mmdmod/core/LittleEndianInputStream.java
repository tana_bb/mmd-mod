package tana.modding.mmdmod.core;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * リトルエンディアンのバイナリデータを読み取るストリームです。
 * @author tana
 */
public class LittleEndianInputStream extends BufferedInputStream
{
	/**
	 * ワード長のデータを格納する、読み取り用バッファです。
	 */
	protected byte[] bytes;
	/**
	 * bytesをラップします。
	 */
	protected ByteBuffer byteBuffer;
	/**
	 * bytesの現在のポジションです。
	 */
	protected int bufferPos;
	/**
	 * bytesに格納された有効なデータのサイズです。
	 */
	protected int bufferSize;
	/**
	 * bytesの最大サイズです。
	 */
	protected final int bufferCapacity = Integer.SIZE / 8;

	/**
	 * コンストラクタです。
	 * @param in バイナリデータを読み取るストリーム。
	 */
	public LittleEndianInputStream(InputStream in)
	{
		super(in);

		// 初期化
		this.bytes = new byte[bufferCapacity];
		this.byteBuffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		this.bufferPos = bufferCapacity;
		this.bufferSize = 0;
	}

	@Override
	public int read() throws IOException
	{
		// 現在位置がバッファサイズを超えていたらロード
		if (bufferPos >= bufferSize && !this.load())
		{
			return -1;
		}

		return (int)byteBuffer.get(bufferPos++) & 0xFF;  // バイトデータ取得
	}

	@Override
	public int read(byte[] b) throws IOException
	{
		return this.read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		for (int i = off; i < off + len; ++i)
		{
			int val = this.read();
			if (val < 0) return i;  // 読み取れない場合
			b[i] = (byte)val;
		}

		return len;
	}

	@Override
	public long skip(long n) throws IOException
	{
		for (int i = 0; i < n; ++i)
		{
			int val = this.read();
			if (val < 0) return i;  // 読み取れない場合
		}

		return n;
	}

	/**
	 * バイナリデータを符号なしbyteとしてlen個読み取り、bのインデックスoff以降に格納します。
	 * @param b 読み取ったデータを格納する配列。
	 * @param off bの書き込み先インデックス。
	 * @param len 読み取るデータの個数。
	 * @return 実際に読み取れたバイト数。
	 * @throws IOException
	 */
	public int readUByte(int[] b, int off, int len) throws IOException
	{
		int bLen = Byte.SIZE / 8 * len;
		byte[] bytes = new byte[bLen];  // バッファ
		ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.nativeOrder());
		int byteCount = 0;

		// バッファにデータを読み込む
		for (int i = 0; i < bLen; ++i)
		{
			int val = this.read();
			if (val < 0) break;
			bytes[i] = (byte)val;
			++byteCount;
		}

		// バッファのデータをbに書き込む
		buf.position(0);
		for (int i = off; i < off + len; ++i)
		{
			b[i] = buf.get() & 0xFF;
		}

		return byteCount;
	}

	/**
	 * バイナリデータを符号ありbyteとしてlen個読み取り、bのインデックスoff以降に格納します。
	 * @param b 読み取ったデータを格納する配列。
	 * @param off bの書き込み先インデックス。
	 * @param len 読み取るデータの個数。
	 * @return 実際に読み取れたバイト数。
	 * @throws IOException
	 */
	public int readByte(int[] b, int off, int len) throws IOException
	{
		int bLen = Byte.SIZE / 8 * len;
		byte[] bytes = new byte[bLen];  // バッファ
		ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.nativeOrder());
		int byteCount = 0;

		// バッファにデータを読み込む
		for (int i = 0; i < bLen; ++i)
		{
			int val = this.read();
			if (val < 0) break;
			bytes[i] = (byte)val;
			++byteCount;
		}

		// バッファのデータをbに書き込む
		buf.position(0);
		for (int i = off; i < off + len; ++i)
		{
			b[i] = buf.get();
		}

		return byteCount;
	}

	/**
	 * バイナリデータを符号なしshortとしてlen個読み取り、bのインデックスoff以降に格納します。
	 * @param b 読み取ったデータを格納する配列。
	 * @param off bの書き込み先インデックス。
	 * @param len 読み取るデータの個数。
	 * @return 実際に読み取れたバイト数。
	 * @throws IOException
	 */
	public int readUShort(int[] b, int off, int len) throws IOException
	{
		int bLen = Short.SIZE / 8 * len;
		byte[] bytes = new byte[bLen];  // バッファ
		ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.nativeOrder());
		int byteCount = 0;

		// バッファにデータを読み込む
		for (int i = 0; i < bLen; ++i)
		{
			int val = this.read();
			if (val < 0) break;
			bytes[i] = (byte)val;
			++byteCount;
		}

		// バッファのデータをbに書き込む
		buf.position(0);
		for (int i = off; i < off + len; ++i)
		{
			b[i] = buf.getShort() & 0xFFFF;
		}

		return byteCount;
	}

	/**
	 * バイナリデータを符号ありshortとしてlen個読み取り、bのインデックスoff以降に格納します。
	 * @param b 読み取ったデータを格納する配列。
	 * @param off bの書き込み先インデックス。
	 * @param len 読み取るデータの個数。
	 * @return 実際に読み取れたバイト数。
	 * @throws IOException
	 */
	public int readShort(int[] b, int off, int len) throws IOException
	{
		int bLen = Short.SIZE / 8 * len;
		byte[] bytes = new byte[bLen];  // バッファ
		ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.nativeOrder());
		int byteCount = 0;

		// バッファにデータを読み込む
		for (int i = 0; i < bLen; ++i)
		{
			int val = this.read();
			if (val < 0) break;
			bytes[i] = (byte)val;
			++byteCount;
		}

		// バッファのデータをbに書き込む
		buf.position(0);
		for (int i = off; i < off + len; ++i)
		{
			b[i] = buf.getShort();
		}

		return byteCount;
	}

	/**
	 * バイナリデータをintとしてlen個読み取り、bのインデックスoff以降に格納します。
	 * @param b 読み取ったデータを格納する配列。
	 * @param off bの書き込み先インデックス。
	 * @param len 読み取るデータの個数。
	 * @return 実際に読み取れたバイト数。
	 * @throws IOException
	 */
	public int readInt(int[] b, int off, int len) throws IOException
	{
		int bLen = Integer.SIZE / 8 * len;
		byte[] bytes = new byte[bLen];  // バッファ
		ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.nativeOrder());
		int byteCount = 0;

		// バッファにデータを読み込む
		for (int i = 0; i < bLen; ++i)
		{
			int val = this.read();
			if (val < 0) break;
			bytes[i] = (byte)val;
			++byteCount;
		}

		// バッファのデータをbに書き込む
		buf.position(0);
		for (int i = off; i < off + len; ++i)
		{
			b[i] = buf.getInt();
		}

		return byteCount;
	}

	/**
	 * バイナリデータをfloatとしてlen個読み取り、bのインデックスoff以降に格納します。
	 * @param b 読み取ったデータを格納する配列。
	 * @param off bの書き込み先インデックス。
	 * @param len 読み取るデータの個数。
	 * @return 実際に読み取れたバイト数。
	 * @throws IOException
	 */
	public int readFloat(float[] b, int off, int len) throws IOException
	{
		int bLen = Float.SIZE / 8 * len;
		byte[] bytes = new byte[bLen];  // バッファ
		ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.nativeOrder());
		int byteCount = 0;

		// バッファにデータを読み込む
		for (int i = 0; i < bLen; ++i)
		{
			int val = this.read();
			if (val < 0) break;
			bytes[i] = (byte)val;
			++byteCount;
		}

		// バッファのデータをbに書き込む
		buf.position(0);
		for (int i = off; i < off + len; ++i)
		{
			b[i] = buf.getFloat();
		}

		return byteCount;
	}

	/**
	 * ストリームからbytesバッファにデータを読み込みます。
	 * @return データが読み取れたかどうか。
	 * @throws IOException
	 */
	protected boolean load() throws IOException
	{
		// ストリームからデータを読み込む
		this.bufferSize = super.read(bytes, 0, bufferCapacity);

		if (this.bufferSize <= 0) return false;  // 読み取り失敗

		// 読み取ったデータがバッファ最大サイズより小さい場合の処理
		for (int i = this.bufferSize; i < bufferCapacity; ++i)
		{
			bytes[i] = 0;
		}
		// ポジション初期化
		bufferPos = 0;

		return true;
	}

	@Override
	public boolean markSupported()
	{
		return false;
	}

	@Override
	public int available() throws IOException
	{
		throw new UnsupportedOperationException("Unsupported method.");
	}

	@Override
	public void mark(int readlimit)
	{
		throw new UnsupportedOperationException("Unsupported method.");
	}

	@Override
	public void reset() throws IOException
	{
		throw new UnsupportedOperationException("Unsupported method.");
	}
}
