package tana.modding.mmdmod.core;

/**
 * VMDデータから構築されたモーフのフレームデータです。
 * @author tana
 */
public class MorphFrameData
{
	public float value = 0.0F;
}
