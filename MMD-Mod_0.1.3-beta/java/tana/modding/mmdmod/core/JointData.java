package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Vector3f;

/**
 * PMXのJointデータです。
 * @author tana
 */
public class JointData
{
	public String name = null;
	public String nameEn = null;
	public int jointType = -1;
	public int rigidBodyIndex1 = -1;
	public int rigidBodyIndex2 = -1;
	public Vector3f p = new Vector3f();
	public Vector3f r = new Vector3f();
	public Vector3f minLinear = new Vector3f();
	public Vector3f maxLinear = new Vector3f();
	public Vector3f minAngular = new Vector3f();
	public Vector3f maxAngular = new Vector3f();
	public Vector3f springLinear = new Vector3f();
	public Vector3f springAngular = new Vector3f();

	public int index = -1;
}
