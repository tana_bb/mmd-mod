package tana.modding.mmdmod.core;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * PMXファイルのデータを格納します。
 * 詳しくはPMXの仕様を確認して下さい。
 * @author tana
 */
public class PMXData
{
	/**
	 * PMXヘッダです。
	 */
	public PMXHeader pmxHeader = null;
	/**
	 * モデル情報です。
	 */
	public ModelInfo modelInfo = null;

	/**
	 * 頂点データです。
	 * 配列のサイズは vertexCount となります。
	 */
	public VertexData[] vertexData = null;
	/**
	 * 頂点の個数です。
	 */
	public int vertexCount = -1;

	/**
	 * 面データです。
	 * 配列のサイズは faceCount となります。
	 */
	public FaceData[] faceData = null;
	/**
	 * 面の個数（頂点数）です。
	 */
	public int faceCount = -1;

	/**
	 * 辺データです。
	 * 配列のサイズは edgeCount となります。
	 */
	public EdgeData[] edgeData = null;
	/**
	 * 辺の個数です。
	 */
	public int edgeCount = -1;

	/**
	 * テクスチャデータです。
	 * 配列のサイズは textureCount + sharedTextureCount となります。
	 */
	public TextureData[] textureData = null;
	/**
	 * 個別テクスチャの個数です。
	 */
	public int textureCount = -1;
	/**
	 * 共有テクスチャの個数です。
	 */
	public int sharedTextureCount = -1;
	/**
	 * テクスチャが使用されるかどうかを表すフラグです。
	 */
	public boolean[] textureUsageFlag = null;

	/**
	 * 材質データです。
	 * 配列のサイズは materialCount となります。
	 */
	public MaterialData[] materialData = null;
	/**
	 * 材質の個数です。
	 */
	public int materialCount = -1;

	/**
	 * ボーンデータです。
	 * 配列のサイズは boneCount となります。
	 */
	public BoneData[] boneData = null;
	/**
	 * ボーンの個数です。
	 */
	public int boneCount = -1;
	/**
	 * 座標変換順に再編成されたボーンデータです。
	 * 最初の添字には、0 : 物理前変換、1 : 物理後変換を指定します。
	 * 次の添字には、階層を指定します。
	 * 最後の添字には、ボーンのインデックス順にボーンが格納されます。
	 */
	public BoneData[][][] tBoneData = null;
	/**
	 * ボーンの階層の個数です。
	 */
	public int boneHierarchyCount = -1;
	/**
	 * 名称ごとにマップされたボーンデータです。
	 */
	public Map<String, BoneData> boneMap = null;

	/**
	 * モーフデータです。
	 * 配列のサイズは morphCount となります。
	 */
	public MorphData[] morphData = null;
	/**
	 * モーフの個数です。
	 */
	public int morphCount = -1;
	/**
	 * モーフ種類ごとに再編成されたモーフデータです。
	 * 配列の最初の添字には、モーフ種類ごとに 9 個の要素が順に確保されます。
	 * 次の添字には、それぞれ xxMorphCount 個のデータが確保されます。
	 */
	public MorphData[][] tMorphData = null;
	/**
	 * グループモーフの個数です。
	 */
	public int groupMorphCount = -1;
	/**
	 * 頂点モーフの個数です。
	 */
	public int vertexMorphCount = -1;
	/**
	 * ボーンモーフの個数です。
	 */
	public int boneMorphCount = -1;
	/**
	 * UVモーフの個数です。
	 */
	public int uvMorphCount = -1;
	/**
	 * 追加UV1モーフの個数です。
	 */
	public int addUV1MorphCount = -1;
	/**
	 * 追加UV2モーフの個数です。
	 */
	public int addUV2MorphCount = -1;
	/**
	 * 追加UV3モーフの個数です。
	 */
	public int addUV3MorphCount = -1;
	/**
	 * 追加UV4モーフの個数です。
	 */
	public int addUV4MorphCount = -1;
	/**
	 * 材質モーフの個数です。
	 */
	public int materialMorphCount = -1;
	/**
	 * フリップモーフの個数です。
	 */
	public int flipMorphCount = -1;
	/**
	 * インパルスモーフの個数です。
	 */
	public int impulseMorphCount = -1;

	/**
	 * 表示枠データです。
	 * 配列のサイズは paneCount となります。
	 */
	public PaneData[] paneData = null;
	/**
	 * 表示枠の個数です。
	 */
	public int paneCount = -1;

	/**
	 * 剛体データです。
	 * 配列のサイズは rigidBodyCount となります。
	 */
	public RigidBodyData[] rigidBodyData = null;
	/**
	 * 剛体の個数です。
	 */
	public int rigidBodyCount = -1;

	/**
	 * Jointデータです。
	 * 配列のサイズは jointCount となります。
	 */
	public JointData[] jointData = null;
	/**
	 * Jointの個数です。
	 */
	public int jointCount = -1;

	/**
	 * リソースが格納されるディレクトリのパスです。
	 */
	public String resourceDirPath = null;

	/**
	 * 頂点の最小xyz座標です。
	 */
	public Vector3f min = new Vector3f(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
	/**
	 * 頂点の最大xyz座標です。
	 */
	public Vector3f max = new Vector3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);

	/**
	 * 内部バッファのサイズです。
	 */
	private final int bufferSize = 0x10000;
	/**
	 * 不正なファイル形式を警告するメッセージです。
	 */
	private final String invalidPMXDataMessage = "Invalid PMX data.";
	/**
	 * 計算用のベクトルバッファです。
	 */
	private Vector3f vBuf0 = new Vector3f(), vBuf1 = new Vector3f(), vBuf2 = new Vector3f(), vBuf3 = new Vector3f();
	/**
	 * 計算用のベクトルバッファです。
	 */
	private Vector4f v4Buf0 = new Vector4f(), v4Buf1 = new Vector4f();
	/**
	 * 計算用の行列バッファです。
	 */
	private Matrix4f mBuf0 = new Matrix4f(), mBuf1 = new Matrix4f();
	/**
	 * 計算用のクォータニオンバッファです。
	 */
	private Quaternion qBuf0 = new Quaternion(), qBuf1 = new Quaternion();
	/**
	 * 計算用のボーンフレームバッファです。
	 */
	private BoneFrameData boneBuf0 = new BoneFrameData(), boneBuf1 = new BoneFrameData();
	/**
	 * 計算用のモーフフレームバッファです。
	 */
	private MorphFrameData morphBuf0 = new MorphFrameData(), morphBuf1 = new MorphFrameData();

	/**
	 * コンストラクタです。
	 */
	public PMXData()
	{
	}

	/**
	 * PMXファイルを読み込みます。
	 * @param resourceDirPath リソースが格納されるディレクトリのパス。
	 * @param fileName PMXファイルのファイル名。
	 * @throws IOException
	 */
	public void readFile(String resourceDirPath, String fileName) throws IOException
	{
		this.readFile(new File(resourceDirPath, fileName));
	}

	/**
	 * PMXファイルを読み込みます。
	 * @param file PMXファイルへのパス。
	 * @throws IOException
	 */
	public void readFile(File file) throws IOException
	{
		this.resourceDirPath = file.getParent();

		LittleEndianInputStream in = null;
		try
		{
			in = new LittleEndianInputStream(new FileInputStream(file));

			// ファイル読み取り
			this.readPMXHeader(in);
			this.readModelInfo(in);
			this.readVertexData(in);
			this.readFaceData(in);
			this.readTextureData(in);
			this.readMaterialData(in);
			this.readBoneData(in);
			this.readMorphData(in);
			this.readPaneData(in);
			this.readRigidBodyData(in);
			this.readJointData(in);
		}
		finally
		{
			if (in != null) in.close();
		}
		// 辺データの構築
		this.structureEdges();
	}

	/**
	 * PMXヘッダを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readPMXHeader(LittleEndianInputStream in) throws IOException
	{
		byte[] bBuf = new byte[bufferSize];
		float[] fBuf = new float[1];

		this.pmxHeader = new PMXHeader();

		this.read(in, bBuf, 0, 4);
		this.pmxHeader.pmxName = new String(bBuf, 0, 4, Charset.forName("US-ASCII"));

		this.readFloat(in, fBuf, 0, 1);
		this.pmxHeader.version = fBuf[0];
		if (this.pmxHeader.version != 2.0F && this.pmxHeader.version != 2.1F)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}

		this.read(in, bBuf, 0, 1);
		int infoSize = bBuf[0];

		this.read(in, bBuf, 0, infoSize);
		switch (bBuf[0])
		{
		case 0:
			this.pmxHeader.encoding = Charset.forName("UTF-16LE");
			break;
		case 1:
			this.pmxHeader.encoding = Charset.forName("UTF-8");
			break;
		}

		switch (bBuf[1])
		{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			this.pmxHeader.additionalUVCount = bBuf[1];
			break;
		}

		switch (bBuf[2])
		{
		case 1:
		case 2:
		case 4:
			this.pmxHeader.vertexIndexSize = bBuf[2];
			break;
		default:
			throw new IOException(this.invalidPMXDataMessage);
		}

		switch (bBuf[3])
		{
		case 1:
		case 2:
		case 4:
			this.pmxHeader.textureIndexSize = bBuf[3];
			break;
		default:
			throw new IOException(this.invalidPMXDataMessage);
		}

		switch (bBuf[4])
		{
		case 1:
		case 2:
		case 4:
			this.pmxHeader.materialIndexSize = bBuf[4];
			break;
		default:
			throw new IOException(this.invalidPMXDataMessage);
		}

		switch (bBuf[5])
		{
		case 1:
		case 2:
		case 4:
			this.pmxHeader.boneIndexSize = bBuf[5];
			break;
		default:
			throw new IOException(this.invalidPMXDataMessage);
		}

		switch (bBuf[6])
		{
		case 1:
		case 2:
		case 4:
			this.pmxHeader.morphIndexSize = bBuf[6];
			break;
		default:
			throw new IOException(this.invalidPMXDataMessage);
		}

		switch (bBuf[7])
		{
		case 1:
		case 2:
		case 4:
			this.pmxHeader.rigidBodyIndexSize = bBuf[7];
			break;
		default:
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * モデル情報を読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readModelInfo(LittleEndianInputStream in) throws IOException
	{
		String[] strBuf = new String[4];

		this.modelInfo = new ModelInfo();

		this.readTextBuf(in, strBuf, 0, 4, this.pmxHeader.encoding);
		this.modelInfo.modelName = strBuf[0];
		this.modelInfo.modelNameEn = strBuf[1];
		this.modelInfo.comment = strBuf[2];
		this.modelInfo.commentEn = strBuf[3];
	}

	/**
	 * 頂点データを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readVertexData(LittleEndianInputStream in) throws IOException
	{
		byte[] bBuf = new byte[4];
		int[] iBuf = new int[4];
		float[] fBuf = new float[4];

		this.readInt(in, iBuf, 0, 1);
		this.vertexCount = iBuf[0];

		this.vertexData = new VertexData[this.vertexCount];

		for (int i = 0; i < this.vertexCount; ++i)
		{
			this.vertexData[i] = new VertexData();
			this.vertexData[i].index = i;
			this.vertexData[i].faces = new ArrayList<FaceData>(4);
			this.vertexData[i].edges = new ArrayList<EdgeData>(4);

			this.readFloat(in, fBuf, 0, 3);
			this.vertexData[i].p.x = fBuf[0];
			this.vertexData[i].p.y = fBuf[1];
			this.vertexData[i].p.z = fBuf[2];

			this.readFloat(in, fBuf, 0, 3);
			this.vertexData[i].n.x = fBuf[0];
			this.vertexData[i].n.y = fBuf[1];
			this.vertexData[i].n.z = fBuf[2];

			this.readFloat(in, fBuf, 0, 2);
			this.vertexData[i].uv.x = fBuf[0];
			this.vertexData[i].uv.y = fBuf[1];

			if (this.pmxHeader.additionalUVCount >= 1)
			{
				this.readFloat(in, fBuf, 0, 4);
				this.vertexData[i].addUV1 = new Vector4f();
				this.vertexData[i].morphAddUV1 = new Vector4f();
				this.vertexData[i].renderAddUV1 = new Vector4f();
				this.vertexData[i].addUV1.x = fBuf[0];
				this.vertexData[i].addUV1.y = fBuf[1];
				this.vertexData[i].addUV1.z = fBuf[2];
				this.vertexData[i].addUV1.w = fBuf[3];
			}
			if (this.pmxHeader.additionalUVCount >= 2)
			{
				this.readFloat(in, fBuf, 0, 4);
				this.vertexData[i].addUV2 = new Vector4f();
				this.vertexData[i].morphAddUV2 = new Vector4f();
				this.vertexData[i].renderAddUV2 = new Vector4f();
				this.vertexData[i].addUV2.x = fBuf[0];
				this.vertexData[i].addUV2.y = fBuf[1];
				this.vertexData[i].addUV2.z = fBuf[2];
				this.vertexData[i].addUV2.w = fBuf[3];
			}
			if (this.pmxHeader.additionalUVCount >= 3)
			{
				this.readFloat(in, fBuf, 0, 4);
				this.vertexData[i].addUV3 = new Vector4f();
				this.vertexData[i].morphAddUV3 = new Vector4f();
				this.vertexData[i].renderAddUV3 = new Vector4f();
				this.vertexData[i].addUV3.x = fBuf[0];
				this.vertexData[i].addUV3.y = fBuf[1];
				this.vertexData[i].addUV3.z = fBuf[2];
				this.vertexData[i].addUV3.w = fBuf[3];
			}
			if (this.pmxHeader.additionalUVCount >= 4)
			{
				this.readFloat(in, fBuf, 0, 4);
				this.vertexData[i].addUV4 = new Vector4f();
				this.vertexData[i].morphAddUV4 = new Vector4f();
				this.vertexData[i].renderAddUV4 = new Vector4f();
				this.vertexData[i].addUV4.x = fBuf[0];
				this.vertexData[i].addUV4.y = fBuf[1];
				this.vertexData[i].addUV4.z = fBuf[2];
				this.vertexData[i].addUV4.w = fBuf[3];
			}

			read(in, bBuf, 0, 1);
			this.vertexData[i].boneBdefMethod = bBuf[0];

			switch (bBuf[0])
			{
			case 0:
				readIndex(in, iBuf, 0, 1, this.pmxHeader.boneIndexSize);
				this.vertexData[i].boneIndex1 = iBuf[0];
				break;
			case 1:
				readIndex(in, iBuf, 0, 2, this.pmxHeader.boneIndexSize);
				this.vertexData[i].boneIndex1 = iBuf[0];
				this.vertexData[i].boneIndex2 = iBuf[1];
				readFloat(in, fBuf, 0, 1);
				this.vertexData[i].boneWeight1 = fBuf[0];
				break;
			case 2:
			case 4:
				readIndex(in, iBuf, 0, 4, this.pmxHeader.boneIndexSize);
				this.vertexData[i].boneIndex1 = iBuf[0];
				this.vertexData[i].boneIndex2 = iBuf[1];
				this.vertexData[i].boneIndex3 = iBuf[2];
				this.vertexData[i].boneIndex4 = iBuf[3];
				readFloat(in, fBuf, 0, 4);
				this.vertexData[i].boneWeight1 = fBuf[0];
				this.vertexData[i].boneWeight2 = fBuf[1];
				this.vertexData[i].boneWeight3 = fBuf[2];
				this.vertexData[i].boneWeight4 = fBuf[3];
				break;
			case 3:
				readIndex(in, iBuf, 0, 2, this.pmxHeader.boneIndexSize);
				this.vertexData[i].boneIndex1 = iBuf[0];
				this.vertexData[i].boneIndex2 = iBuf[1];
				readFloat(in, fBuf, 0, 1);
				this.vertexData[i].boneWeight1 = fBuf[0];
				readFloat(in, fBuf, 0, 3);
				this.vertexData[i].sdefC = new Vector3f();
				this.vertexData[i].sdefC.x = fBuf[0];
				this.vertexData[i].sdefC.y = fBuf[1];
				this.vertexData[i].sdefC.z = fBuf[2];
				readFloat(in, fBuf, 0, 3);
				this.vertexData[i].sdefR0 = new Vector3f();
				this.vertexData[i].sdefR0.x = fBuf[0];
				this.vertexData[i].sdefR0.y = fBuf[1];
				this.vertexData[i].sdefR0.z = fBuf[2];
				readFloat(in, fBuf, 0, 3);
				this.vertexData[i].sdefR1 = new Vector3f();
				this.vertexData[i].sdefR1.x = fBuf[0];
				this.vertexData[i].sdefR1.y = fBuf[1];
				this.vertexData[i].sdefR1.z = fBuf[2];
				break;
			}

			readFloat(in, fBuf, 0, 1);
			this.vertexData[i].edgeScale = fBuf[0];

			// 最大、最小の座標を記録
			if (min.x > this.vertexData[i].p.x) min.x = this.vertexData[i].p.x;
			if (max.x < this.vertexData[i].p.x) max.x = this.vertexData[i].p.x;
			if (min.y > this.vertexData[i].p.y) min.y = this.vertexData[i].p.y;
			if (max.y < this.vertexData[i].p.y) max.y = this.vertexData[i].p.y;
			if (min.z > this.vertexData[i].p.z) min.z = this.vertexData[i].p.z;
			if (max.z < this.vertexData[i].p.z) max.z = this.vertexData[i].p.z;
		}

		this.initVertices();
	}

	/**
	 * 面データを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readFaceData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[3];

		readInt(in, iBuf, 0, 1);
		this.faceCount = iBuf[0] / 3;

		this.faceData = new FaceData[this.faceCount];

		for (int i = 0; i < this.faceCount; ++i)
		{
			this.faceData[i] = new FaceData();
			this.faceData[i].index = i;

			readUIndex(in, iBuf, 0, 3, this.pmxHeader.vertexIndexSize);
			this.faceData[i].vertexIndex1 = iBuf[0];
			this.faceData[i].vertexIndex2 = iBuf[1];
			this.faceData[i].vertexIndex3 = iBuf[2];
			this.faceData[i].vertex1 = this.vertexData[iBuf[0]];
			this.faceData[i].vertex2 = this.vertexData[iBuf[1]];
			this.faceData[i].vertex3 = this.vertexData[iBuf[2]];

			// 頂点に面を関連づける
			this.vertexData[iBuf[0]].faces.add(this.faceData[i]);
			this.vertexData[iBuf[1]].faces.add(this.faceData[i]);
			this.vertexData[iBuf[2]].faces.add(this.faceData[i]);

		}
	}

	/**
	 * テクスチャデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readTextureData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[1];
		String[] strBuf = new String[1];

		readInt(in, iBuf, 0, 1);
		this.textureCount = iBuf[0];
		this.sharedTextureCount = 10;

		this.textureData = new TextureData[this.textureCount + this.sharedTextureCount];
		this.textureUsageFlag = new boolean[this.textureCount + this.sharedTextureCount];

		for (int i = 0; i < this.textureCount; ++i)
		{
			this.textureData[i] = new TextureData();
			this.textureData[i].index = i;

			readTextBuf(in, strBuf, 0, 1, this.pmxHeader.encoding);
			this.textureData[i].path = strBuf[0];

			this.textureUsageFlag[i] = false;
		}

		for (int i = 0; i < this.sharedTextureCount; ++i)
		{
			this.textureData[this.textureCount + i] = new TextureData();
			this.textureData[this.textureCount + i].index = -1;
			this.textureData[this.textureCount + i].path = "toon" + String.format("%02d", i + 1) + ".bmp";

			this.textureUsageFlag[this.textureCount + i] = false;
		}
	}

	/**
	 * 材質データを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readMaterialData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[2];
		String[] strBuf = new String[2];
		float[] fBuf = new float[4];
		byte[] bBuf = new byte[1];

		readInt(in, iBuf, 0, 1);
		this.materialCount = iBuf[0];

		this.materialData = new MaterialData[this.materialCount];

		for (int i = 0; i < this.materialCount; ++i)
		{
			this.materialData[i] = new MaterialData();
			this.materialData[i].index = i;

			readTextBuf(in, strBuf, 0, 2, this.pmxHeader.encoding);
			this.materialData[i].name = strBuf[0];
			this.materialData[i].nameEn = strBuf[1];

			readFloat(in, fBuf, 0, 4);
			this.materialData[i].diffuse.x = fBuf[0];
			this.materialData[i].diffuse.y = fBuf[1];
			this.materialData[i].diffuse.z = fBuf[2];
			this.materialData[i].diffuse.w = fBuf[3];

			readFloat(in, fBuf, 0, 4);
			this.materialData[i].specular.x = fBuf[0];
			this.materialData[i].specular.y = fBuf[1];
			this.materialData[i].specular.z = fBuf[2];
			this.materialData[i].specular.w = fBuf[3];

			readFloat(in, fBuf, 0, 3);
			this.materialData[i].ambient.x = fBuf[0];
			this.materialData[i].ambient.y = fBuf[1];
			this.materialData[i].ambient.z = fBuf[2];

			read(in, bBuf, 0, 1);
			this.materialData[i].bothFaces = (bBuf[0] & 0x01) != 0;
			this.materialData[i].groundShadow = (bBuf[0] & 0x02) != 0;
			this.materialData[i].drawSelfShadowMap = (bBuf[0] & 0x04) != 0;
			this.materialData[i].drawSelfShadow = (bBuf[0] & 0x08) != 0;
			this.materialData[i].drawEdge = (bBuf[0] & 0x10) != 0;

			readFloat(in, fBuf, 0, 4);
			this.materialData[i].edgeColor.x = fBuf[0];
			this.materialData[i].edgeColor.y = fBuf[1];
			this.materialData[i].edgeColor.z = fBuf[2];
			this.materialData[i].edgeColor.w = fBuf[3];

			readFloat(in, fBuf, 0, 1);
			this.materialData[i].edgeSize = fBuf[0];

			readIndex(in, iBuf, 0, 2, this.pmxHeader.textureIndexSize);
			this.materialData[i].textureIndex = iBuf[0];
			if (iBuf[0] >= 0) this.textureUsageFlag[iBuf[0]] = true;
			this.materialData[i].sphereIndex = iBuf[1];
			if (iBuf[1] >= 0) this.textureUsageFlag[iBuf[1]] = true;

			read(in, bBuf, 0, 1);
			this.materialData[i].sphereMode = bBuf[0];

			read(in, bBuf, 0, 1);
			switch (bBuf[0])
			{
			case 0:
				this.materialData[i].sharedToon = false;
				readIndex(in, iBuf, 0, 1, this.pmxHeader.textureIndexSize);
				this.materialData[i].toonTextureIndex = iBuf[0];
				if (iBuf[0] >= 0) this.textureUsageFlag[iBuf[0]] = true;
				break;
			case 1:
				this.materialData[i].sharedToon = true;
				read(in, bBuf, 0, 1);
				this.materialData[i].toonTextureIndex = this.textureCount + bBuf[0];
				if (bBuf[0] >= 0) this.textureUsageFlag[this.textureCount + bBuf[0]] = true;
				break;
			}

			readTextBuf(in, strBuf, 0, 1, this.pmxHeader.encoding);
			this.materialData[i].memo = strBuf[0];

			readInt(in, iBuf, 0, 1);
			this.materialData[i].faceCount = iBuf[0];
		}
	}

	/**
	 * ボーンデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readBoneData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[1];
		String[] strBuf = new String[2];
		float[] fBuf = new float[3];
		byte[] bBuf = new byte[1];

		readInt(in, iBuf, 0, 1);
		this.boneCount = iBuf[0];

		this.boneData = new BoneData[this.boneCount];
		this.boneHierarchyCount = 0;
		this.boneMap = new HashMap<String, BoneData>();

		List<List<BoneData> > childList = new ArrayList<List<BoneData> >();
		for (int i = 0; i < this.boneCount; ++i)
		{
			childList.add(new ArrayList<BoneData>());
		}

		for (int i = 0; i < this.boneCount; ++i)
		{
			this.boneData[i] = new BoneData();
			this.boneData[i].index = i;

			readTextBuf(in, strBuf, 0, 2, this.pmxHeader.encoding);
			this.boneData[i].name = strBuf[0];
			this.boneData[i].nameEn = strBuf[1];

			readFloat(in, fBuf, 0, 3);
			this.boneData[i].p.x = fBuf[0];
			this.boneData[i].p.y = fBuf[1];
			this.boneData[i].p.z = fBuf[2];

			readIndex(in, iBuf, 0, 1, this.pmxHeader.boneIndexSize);
			this.boneData[i].parentIndex = iBuf[0];

			readInt(in, iBuf, 0, 1);
			this.boneData[i].hierarchy = iBuf[0];

			readShort(in, iBuf, 0, 1);
			this.boneData[i].accessPointFlag = (iBuf[0] & 0x0001) != 0;
			this.boneData[i].canRotate = (iBuf[0] & 0x0002) != 0;
			this.boneData[i].canTranslate = (iBuf[0] & 0x0004) != 0;
			this.boneData[i].visible = (iBuf[0] & 0x0008) != 0;
			this.boneData[i].canControl = (iBuf[0] & 0x0010) != 0;
			this.boneData[i].isIK = (iBuf[0] & 0x0020) != 0;
			this.boneData[i].localAddition = (iBuf[0] & 0x0080) != 0;
			this.boneData[i].rotateAddition = (iBuf[0] & 0x0100) != 0;
			this.boneData[i].translateAddition = (iBuf[0] & 0x0200) != 0;
			this.boneData[i].fixAxis = (iBuf[0] & 0x0400) != 0;
			this.boneData[i].localAxis = (iBuf[0] & 0x0800) != 0;
			this.boneData[i].postTransform = (iBuf[0] & 0x1000) != 0;
			this.boneData[i].externalTransform = (iBuf[0] & 0x2000) != 0;

			if (!this.boneData[i].accessPointFlag)
			{
				readFloat(in, fBuf, 0, 3);
				this.boneData[i].accessPointP.x = fBuf[0];
				this.boneData[i].accessPointP.y = fBuf[1];
				this.boneData[i].accessPointP.z = fBuf[2];
			}
			else
			{
				readIndex(in, iBuf, 0, 1, this.pmxHeader.boneIndexSize);
				this.boneData[i].accessPointIndex = iBuf[0];
			}

			if (this.boneData[i].rotateAddition || this.boneData[i].translateAddition)
			{
				readIndex(in, iBuf, 0, 1, this.pmxHeader.boneIndexSize);
				this.boneData[i].additionParentIndex = iBuf[0];

				readFloat(in, fBuf, 0, 1);
				this.boneData[i].additionRatio = fBuf[0];
			}

			if (this.boneData[i].fixAxis)
			{
				readFloat(in, fBuf, 0, 3);
				this.boneData[i].pivot.x = fBuf[0];
				this.boneData[i].pivot.y = fBuf[1];
				this.boneData[i].pivot.z = fBuf[2];
			}

			if (this.boneData[i].localAxis)
			{
				readFloat(in, fBuf, 0, 3);
				this.boneData[i].localXAxis.x = fBuf[0];
				this.boneData[i].localXAxis.y = fBuf[1];
				this.boneData[i].localXAxis.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.boneData[i].localZAxis.x = fBuf[0];
				this.boneData[i].localZAxis.y = fBuf[1];
				this.boneData[i].localZAxis.z = fBuf[2];
			}

			if (this.boneData[i].externalTransform)
			{
				readInt(in, iBuf, 0, 1);
				this.boneData[i].externalKey = iBuf[0];
			}

			if (this.boneData[i].isIK)
			{
				readIndex(in, iBuf, 0, 1, this.pmxHeader.boneIndexSize);
				this.boneData[i].ikTargetIndex = iBuf[0];

				readInt(in, iBuf, 0, 1);
				this.boneData[i].ikLoopCount = iBuf[0];

				readFloat(in, fBuf, 0, 1);
				this.boneData[i].ikLoopConstRadian = fBuf[0];

				readInt(in, iBuf, 0, 1);
				this.boneData[i].ikLinkCount = iBuf[0];

				this.boneData[i].ikLink = new BoneData.IKLink[this.boneData[i].ikLinkCount];

				for (int j = 0; j < this.boneData[i].ikLinkCount; ++j)
				{
					this.boneData[i].ikLink[j] = this.boneData[i].new IKLink();

					readIndex(in, iBuf, 0, 1, this.pmxHeader.boneIndexSize);
					this.boneData[i].ikLink[j].linkIndex = iBuf[0];

					read(in, bBuf, 0, 1);
					this.boneData[i].ikLink[j].constRadian = bBuf[0] == 1;

					if (this.boneData[i].ikLink[j].constRadian)
					{
						readFloat(in, fBuf, 0, 3);
						this.boneData[i].ikLink[j].min.x = fBuf[0];
						this.boneData[i].ikLink[j].min.y = fBuf[1];
						this.boneData[i].ikLink[j].min.z = fBuf[2];

						readFloat(in, fBuf, 0, 3);
						this.boneData[i].ikLink[j].max.x = fBuf[0];
						this.boneData[i].ikLink[j].max.y = fBuf[1];
						this.boneData[i].ikLink[j].max.z = fBuf[2];
					}
				}
			}

			// ローカル座標
			this.boneData[i].localP.x = this.boneData[i].p.x;
			this.boneData[i].localP.y = this.boneData[i].p.y;
			this.boneData[i].localP.z = this.boneData[i].p.z;

			// 親ボーンの子情報を記録
			if (this.boneData[i].parentIndex >= 0)
			{
				childList.get(this.boneData[i].parentIndex).add(this.boneData[i]);
			}

			// 最大階層を記録
			if (this.boneHierarchyCount < this.boneData[i].hierarchy)
			{
				this.boneHierarchyCount = this.boneData[i].hierarchy;
			}

			// boneMapに登録
			boneMap.put(this.boneData[i].name, this.boneData[i]);
		}

		++this.boneHierarchyCount;  // 最大階層に1を加えて階層数にする。

		// 子ボーン情報を格納
		for (int i = 0; i < this.boneCount; ++i)
		{
			this.boneData[i].children = new BoneData[childList.get(i).size()];
			childList.get(i).toArray(this.boneData[i].children);
		}

		// 座標変換順に再編成
		List<List<List<BoneData> > > boneList = new ArrayList<List<List<BoneData> > >();
		for (int i = 0; i < 2; ++i)
		{
			boneList.add(new ArrayList<List<BoneData> >());
			for (int j = 0; j < this.boneHierarchyCount; ++j)
			{
				boneList.get(i).add(new ArrayList<BoneData>());
			}
		}

		for (int i = 0; i < this.boneCount; ++i)
		{
			int index = boneData[i].postTransform ? 1 : 0;
			boneList.get(index).get(this.boneData[i].hierarchy).add(boneData[i]);
		}

		this.tBoneData = new BoneData[2][][];
		for (int i = 0; i < 2; ++i)
		{
			this.tBoneData[i] = new BoneData[this.boneHierarchyCount][];
			for (int j = 0; j < this.boneHierarchyCount; ++j)
			{
				int size = boneList.get(i).get(j).size();
				this.tBoneData[i][j] = new BoneData[size];
				for (int k = 0; k < size; ++k)
				{
					this.tBoneData[i][j][k] = boneList.get(i).get(j).get(k);
				}
			}
		}

		this.initBones();
	}

	/**
	 * モーフデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readMorphData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[1];
		String[] strBuf = new String[2];
		float[] fBuf = new float[4];
		byte[] bBuf = new byte[1];

		readInt(in, iBuf, 0, 1);
		this.morphCount = iBuf[0];

		this.morphData = new MorphData[this.morphCount];

		for (int i = 0; i < this.morphCount; ++i)
		{
			this.morphData[i] = new MorphData();
			this.morphData[i].index = i;

			readTextBuf(in, strBuf, 0, 2, this.pmxHeader.encoding);
			this.morphData[i].name = strBuf[0];
			this.morphData[i].nameEn = strBuf[1];

			read(in, bBuf, 0, 1);
			this.morphData[i].panelPosition = bBuf[0];

			read(in, bBuf, 0, 1);
			this.morphData[i].morphType = bBuf[0];

			readInt(in, iBuf, 0, 1);
			this.morphData[i].offsetCount = iBuf[0];

			this.morphData[i].offsets = new MorphData.MorphOffset[this.morphData[i].offsetCount];

			switch (this.morphData[i].morphType)
			{
			case 0:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.GroupMorph offset = this.morphData[i].new GroupMorph();

					readIndex(in, iBuf, 0, 1, pmxHeader.morphIndexSize);
					offset.targetIndex = iBuf[0];

					readFloat(in, fBuf, 0, 1);
					offset.ratio = fBuf[0];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			case 1:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.VertexOffset offset = this.morphData[i].new VertexOffset();

					readIndex(in, iBuf, 0, 1, pmxHeader.vertexIndexSize);
					offset.targetIndex = iBuf[0];

					readFloat(in, fBuf, 0, 3);
					offset.p.x = fBuf[0];
					offset.p.y = fBuf[1];
					offset.p.z = fBuf[2];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			case 2:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.BoneOffset offset = this.morphData[i].new BoneOffset();

					readIndex(in, iBuf, 0, 1, pmxHeader.boneIndexSize);
					offset.targetIndex = iBuf[0];

					readFloat(in, fBuf, 0, 3);
					offset.translate.x = fBuf[0];
					offset.translate.y = fBuf[1];
					offset.translate.z = fBuf[2];

					readFloat(in, fBuf, 0, 4);
					offset.rotate.x = fBuf[0];
					offset.rotate.y = fBuf[1];
					offset.rotate.z = fBuf[2];
					offset.rotate.w = fBuf[3];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.UVOffset offset = this.morphData[i].new UVOffset();

					readIndex(in, iBuf, 0, 1, pmxHeader.vertexIndexSize);
					offset.targetIndex = iBuf[0];

					readFloat(in, fBuf, 0, 4);
					offset.uv.x = fBuf[0];
					offset.uv.y = fBuf[1];
					offset.uv.z = fBuf[2];
					offset.uv.w = fBuf[3];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			case 8:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.MaterialOffset offset = this.morphData[i].new MaterialOffset();

					readIndex(in, iBuf, 0, 1, pmxHeader.materialIndexSize);
					offset.targetIndex = iBuf[0];

					read(in, bBuf, 0, 1);
					offset.operator = bBuf[0];

					readFloat(in, fBuf, 0, 4);
					offset.diffuse.x = fBuf[0];
					offset.diffuse.y = fBuf[1];
					offset.diffuse.z = fBuf[2];
					offset.diffuse.w = fBuf[3];

					readFloat(in, fBuf, 0, 4);
					offset.specular.x = fBuf[0];
					offset.specular.y = fBuf[1];
					offset.specular.z = fBuf[2];
					offset.specular.w = fBuf[3];

					readFloat(in, fBuf, 0, 3);
					offset.ambient.x = fBuf[0];
					offset.ambient.y = fBuf[1];
					offset.ambient.z = fBuf[2];

					readFloat(in, fBuf, 0, 4);
					offset.edgeColor.x = fBuf[0];
					offset.edgeColor.y = fBuf[1];
					offset.edgeColor.z = fBuf[2];
					offset.edgeColor.w = fBuf[3];

					readFloat(in, fBuf, 0, 1);
					offset.edgeSize = fBuf[0];

					readFloat(in, fBuf, 0, 4);
					offset.texture.x = fBuf[0];
					offset.texture.y = fBuf[1];
					offset.texture.z = fBuf[2];
					offset.texture.w = fBuf[3];

					readFloat(in, fBuf, 0, 4);
					offset.sphere.x = fBuf[0];
					offset.sphere.y = fBuf[1];
					offset.sphere.z = fBuf[2];
					offset.sphere.w = fBuf[3];

					readFloat(in, fBuf, 0, 4);
					offset.toon.x = fBuf[0];
					offset.toon.y = fBuf[1];
					offset.toon.z = fBuf[2];
					offset.toon.w = fBuf[3];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			case 9:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.FlipMorph offset = this.morphData[i].new FlipMorph();

					readIndex(in, iBuf, 0, 1, pmxHeader.morphIndexSize);
					offset.targetIndex = iBuf[0];

					readFloat(in, fBuf, 0, 1);
					offset.ratio = fBuf[0];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			case 10:
				for (int j = 0; j < this.morphData[i].offsetCount; ++j)
				{
					MorphData.ImpulseMorph offset = this.morphData[i].new ImpulseMorph();

					readIndex(in, iBuf, 0, 1, pmxHeader.rigidBodyIndexSize);
					offset.targetIndex = iBuf[0];

					readFloat(in, fBuf, 0, 3);
					offset.velocity.x = fBuf[0];
					offset.velocity.y = fBuf[1];
					offset.velocity.z = fBuf[2];

					readFloat(in, fBuf, 0, 3);
					offset.torque.x = fBuf[0];
					offset.torque.y = fBuf[1];
					offset.torque.z = fBuf[2];

					this.morphData[i].offsets[j] = offset;
				}
				break;
			}
		}

		// モーフ種類ごとに再編成
		List<List<MorphData> > morphList = new ArrayList<List<MorphData> >();
		for (int i = 0; i < 11; ++i)
		{
			morphList.add(new ArrayList<MorphData>());
		}

		for (int i = 0; i < this.morphCount; ++i)
		{
			morphList.get(this.morphData[i].morphType).add(this.morphData[i]);
		}

		this.tMorphData = new MorphData[11][];

		for (int i = 0; i < 11; ++i)
		{
			this.tMorphData[i] = new MorphData[morphList.get(i).size()];
			morphList.get(i).toArray(this.tMorphData[i]);
		}

		this.groupMorphCount = this.tMorphData[0].length;
		this.vertexMorphCount = this.tMorphData[1].length;
		this.boneMorphCount = this.tMorphData[2].length;
		this.uvMorphCount = this.tMorphData[3].length;
		this.addUV1MorphCount = this.tMorphData[4].length;
		this.addUV2MorphCount = this.tMorphData[5].length;
		this.addUV3MorphCount = this.tMorphData[6].length;
		this.addUV4MorphCount = this.tMorphData[7].length;
		this.materialMorphCount = this.tMorphData[8].length;
		this.flipMorphCount = this.tMorphData[9].length;
		this.impulseMorphCount = this.tMorphData[10].length;

		this.initMorphs();
	}

	/**
	 * 表示枠データを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readPaneData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[1];
		String[] strBuf = new String[2];
		byte[] bBuf = new byte[1];

		readInt(in, iBuf, 0, 1);
		this.paneCount = iBuf[0];

		this.paneData = new PaneData[this.paneCount];

		for (int i = 0; i < this.paneCount; ++i)
		{
			this.paneData[i] = new PaneData();
			this.paneData[i].index = i;

			readTextBuf(in, strBuf, 0, 2, this.pmxHeader.encoding);
			this.paneData[i].name = strBuf[0];
			this.paneData[i].nameEn = strBuf[1];

			read(in, bBuf, 0, 1);
			this.paneData[i].isSpecialPane = bBuf[0] == 1;

			readInt(in, iBuf, 0, 1);
			this.paneData[i].elementCount = iBuf[0];

			this.paneData[i].elements = new PaneData.PaneElement[this.paneData[i].elementCount];

			for (int j = 0; j < this.paneData[i].elementCount; ++j)
			{
				this.paneData[i].elements[j] = this.paneData[i].new PaneElement();

				read(in, bBuf, 0, 1);
				this.paneData[i].elements[j].targetFlag = bBuf[0];

				switch (this.paneData[i].elements[j].targetFlag)
				{
				case 0:
					readIndex(in, iBuf, 0, 1, pmxHeader.boneIndexSize);
					this.paneData[i].elements[j].elementIndex = iBuf[0];
					break;
				case 1:
					readIndex(in, iBuf, 0, 1, pmxHeader.morphIndexSize);
					this.paneData[i].elements[j].elementIndex = iBuf[0];
					break;
				}
			}
		}
	}

	/**
	 * 剛体データを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readRigidBodyData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[1];
		String[] strBuf = new String[2];
		float[] fBuf = new float[5];
		byte[] bBuf = new byte[1];

		readInt(in, iBuf, 0, 1);
		this.rigidBodyCount = iBuf[0];

		this.rigidBodyData = new RigidBodyData[this.rigidBodyCount];

		for (int i = 0; i < this.rigidBodyCount; ++i)
		{
			this.rigidBodyData[i] = new RigidBodyData();
			this.rigidBodyData[i].index = i;

			readTextBuf(in, strBuf, 0, 2, this.pmxHeader.encoding);
			this.rigidBodyData[i].name = strBuf[0];
			this.rigidBodyData[i].nameEn = strBuf[1];

			readIndex(in, iBuf, 0, 1, pmxHeader.boneIndexSize);
			this.rigidBodyData[i].boneIndex = iBuf[0];

			read(in, bBuf, 0, 1);
			this.rigidBodyData[i].group = bBuf[0];

			readUShort(in, iBuf, 0, 1);
			this.rigidBodyData[i].uncollisionGroupFlag = iBuf[0];

			read(in, bBuf, 0, 1);
			this.rigidBodyData[i].shape = bBuf[0];

			readFloat(in, fBuf, 0, 3);
			this.rigidBodyData[i].size.x = fBuf[0];
			this.rigidBodyData[i].size.y = fBuf[1];
			this.rigidBodyData[i].size.z = fBuf[2];

			readFloat(in, fBuf, 0, 3);
			this.rigidBodyData[i].p.x = fBuf[0];
			this.rigidBodyData[i].p.y = fBuf[1];
			this.rigidBodyData[i].p.z = fBuf[2];

			readFloat(in, fBuf, 0, 3);
			this.rigidBodyData[i].r.x = fBuf[0];
			this.rigidBodyData[i].r.y = fBuf[1];
			this.rigidBodyData[i].r.z = fBuf[2];

			readFloat(in, fBuf, 0, 5);
			this.rigidBodyData[i].mass = fBuf[0];
			this.rigidBodyData[i].linearDamping = fBuf[1];
			this.rigidBodyData[i].angularDamping = fBuf[2];
			this.rigidBodyData[i].restitution = fBuf[3];
			this.rigidBodyData[i].friction = fBuf[4];

			read(in, bBuf, 0, 1);
			this.rigidBodyData[i].operationFlag = bBuf[0];

			// 物理ボーンを記録
			if (this.rigidBodyData[i].boneIndex >= 0 && this.rigidBodyData[i].operationFlag != 0)
			{
				this.boneData[this.rigidBodyData[i].boneIndex].isPhysicsBone = true;
			}
		}
	}

	/**
	 * Jointデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readJointData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[2];
		String[] strBuf = new String[2];
		float[] fBuf = new float[3];
		byte[] bBuf = new byte[1];

		readInt(in, iBuf, 0, 1);
		this.jointCount = iBuf[0];

		this.jointData = new JointData[this.jointCount];

		for (int i = 0; i < this.jointCount; ++i)
		{
			this.jointData[i] = new JointData();
			this.jointData[i].index = i;

			readTextBuf(in, strBuf, 0, 2, this.pmxHeader.encoding);
			this.jointData[i].name = strBuf[0];
			this.jointData[i].nameEn = strBuf[1];

			read(in, bBuf, 0, 1);
			this.jointData[i].jointType = bBuf[0];

			switch (this.jointData[i].jointType)
			{
			case 0:
				readIndex(in, iBuf, 0, 2, pmxHeader.rigidBodyIndexSize);
				this.jointData[i].rigidBodyIndex1 = iBuf[0];
				this.jointData[i].rigidBodyIndex2 = iBuf[1];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].p.x = fBuf[0];
				this.jointData[i].p.y = fBuf[1];
				this.jointData[i].p.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].r.x = fBuf[0];
				this.jointData[i].r.y = fBuf[1];
				this.jointData[i].r.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].minLinear.x = fBuf[0];
				this.jointData[i].minLinear.y = fBuf[1];
				this.jointData[i].minLinear.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].maxLinear.x = fBuf[0];
				this.jointData[i].maxLinear.y = fBuf[1];
				this.jointData[i].maxLinear.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].minAngular.x = fBuf[0];
				this.jointData[i].minAngular.y = fBuf[1];
				this.jointData[i].minAngular.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].maxAngular.x = fBuf[0];
				this.jointData[i].maxAngular.y = fBuf[1];
				this.jointData[i].maxAngular.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].springLinear.x = fBuf[0];
				this.jointData[i].springLinear.y = fBuf[1];
				this.jointData[i].springLinear.z = fBuf[2];

				readFloat(in, fBuf, 0, 3);
				this.jointData[i].springAngular.x = fBuf[0];
				this.jointData[i].springAngular.y = fBuf[1];
				this.jointData[i].springAngular.z = fBuf[2];
				break;
			}
		}
	}

	/**
	 * 指定されたフレームの座標変換を適用します。
	 * @param vmd1 VMDデータ1。
	 * @param frame1 vmd1のフレーム番号。
	 * @param vmd2 VMDデータ2。
	 * @param frame2 vmd2のフレーム番号。
	 * @param t vmd1とvmd2のブレンド係数。
	 * @param physics 物理演算オブジェクト。
	 * @param enablePhysics 物理演算を有効にするかどうか。
	 * @param deltaTime 前フレームからの経過時間。
	 */
	public void transform(
			VMDData vmd1,
			int frame1,
			VMDData vmd2,
			int frame2,
			float t,
			Physics physics,
			boolean enablePhysics,
			float deltaTime)
	{
		if (vmd1 == null || frame1 < 0)
		{
			frame1 = 0;
		}
		else if (frame1 > vmd1.maxFrame)
		{
			frame1 = vmd1.maxFrame;
		}

		if (vmd2 == null || frame2 < 0)
		{
			frame2 = 0;
		}
		else if (frame2 > vmd2.maxFrame)
		{
			frame2 = vmd2.maxFrame;
		}

		this.initBones();
		this.initMorphs();

		this.setUserTransformToBones(vmd1, frame1, vmd2, frame2, t);
		this.setUserTransformToMorphs(vmd1, frame1, vmd2, frame2, t);
		this.applyMorphs();
		this.transformBones(false);

		if (enablePhysics)
		{
			this.physicalTransform(physics, deltaTime);
		}

		this.transformBones(true);

		this.initVertices();
		this.transformVertices();
	}

	/**
	 * ボーンの座標変換を初期化します。
	 */
	protected void initBones()
	{
		for (int i = 0; i < this.boneCount; ++i)
		{
			this.boneData[i].translate.x = 0.0F;
			this.boneData[i].translate.y = 0.0F;
			this.boneData[i].translate.z = 0.0F;
			this.boneData[i].rotate.x = 0.0F;
			this.boneData[i].rotate.y = 0.0F;
			this.boneData[i].rotate.z = 0.0F;
			this.boneData[i].rotate.w = 1.0F;
			this.boneData[i].morphTranslate.x = 0.0F;
			this.boneData[i].morphTranslate.y = 0.0F;
			this.boneData[i].morphTranslate.z = 0.0F;
			this.boneData[i].morphRotate.x = 0.0F;
			this.boneData[i].morphRotate.y = 0.0F;
			this.boneData[i].morphRotate.z = 0.0F;
			this.boneData[i].morphRotate.w = 1.0F;
			this.boneData[i].addTranslate.x = 0.0F;
			this.boneData[i].addTranslate.y = 0.0F;
			this.boneData[i].addTranslate.z = 0.0F;
			this.boneData[i].addRotate.x = 0.0F;
			this.boneData[i].addRotate.y = 0.0F;
			this.boneData[i].addRotate.z = 0.0F;
			this.boneData[i].addRotate.w = 1.0F;
			this.boneData[i].ikRotate.x = 0.0F;
			this.boneData[i].ikRotate.y = 0.0F;
			this.boneData[i].ikRotate.z = 0.0F;
			this.boneData[i].ikRotate.w = 1.0F;
			this.boneData[i].localMatrix.setIdentity();
			this.boneData[i].localP.x = this.boneData[i].p.x;
			this.boneData[i].localP.y = this.boneData[i].p.y;
			this.boneData[i].localP.z = this.boneData[i].p.z;
		}
	}

	/**
	 * 各ボーンに対してユーザー指定の座標変換を設定します。
	 * @param vmd1 VMDデータ1。
	 * @param frame1 vmd1のフレーム番号。
	 * @param vmd2 VMDデータ2。
	 * @param frame2 vmd2のフレーム番号。
	 * @param t vmd1とvmd2のブレンド係数。
	 */
	protected void setUserTransformToBones(VMDData vmd1, int frame1, VMDData vmd2, int frame2, float t)
	{
		for (BoneData bone : this.boneData)
		{
			if (vmd1 != null)
			{
				vmd1.getBoneFrame(bone.name, frame1, boneBuf0);
			}
			else
			{
				boneBuf0.translate.x = 0.0F;
				boneBuf0.translate.y = 0.0F;
				boneBuf0.translate.z = 0.0F;
				boneBuf0.rotate.w = 1.0F;
				boneBuf0.rotate.x = 0.0F;
				boneBuf0.rotate.y = 0.0F;
				boneBuf0.rotate.z = 0.0F;
			}

			if (vmd2 != null)
			{
				vmd2.getBoneFrame(bone.name, frame2, boneBuf1);
			}
			else
			{
				boneBuf1.translate.x = 0.0F;
				boneBuf1.translate.y = 0.0F;
				boneBuf1.translate.z = 0.0F;
				boneBuf1.rotate.w = 1.0F;
				boneBuf1.rotate.x = 0.0F;
				boneBuf1.rotate.y = 0.0F;
				boneBuf1.rotate.z = 0.0F;
			}

			bone.translate.x = boneBuf0.translate.x * (1.0F - t) + boneBuf1.translate.x * t;
			bone.translate.y = boneBuf0.translate.y * (1.0F - t) + boneBuf1.translate.y * t;
			bone.translate.z = boneBuf0.translate.z * (1.0F - t) + boneBuf1.translate.z * t;

			Util.slerp(boneBuf0.rotate, boneBuf1.rotate, t, qBuf0);
			bone.rotate.w = qBuf0.w;
			bone.rotate.x = qBuf0.x;
			bone.rotate.y = qBuf0.y;
			bone.rotate.z = qBuf0.z;
		}
	}

	/**
	 * 各ボーンに対して座標変換を適用します。
	 * @param isPost 物理後変換であるかどうか。
	 */
	protected void transformBones(boolean isPost)
	{
		int prePost = isPost ? 1 : 0;

		// ボーン処理順序に従い変換
		for (int i = 0; i < this.boneHierarchyCount; ++i)
		{
			int len = this.tBoneData[prePost][i].length;
			for (int j = 0; j < len; ++j)
			{
				BoneData bone = this.tBoneData[prePost][i][j];

				transformOneBone(bone, vBuf0, vBuf1, v4Buf0, qBuf0, qBuf1, mBuf0);

				// IK
				if (bone.isIK)
				{
					transformIKLinks(bone, vBuf0, vBuf1, vBuf2, vBuf3, v4Buf0, qBuf0, mBuf0);
				}
			}
		}
	}

	/**
	 * 単一のボーンに座標変換を適用します。
	 * @param bone 変換対象のボーン。
	 * @param vBuf0 計算用。
	 * @param vBuf1 計算用。
	 * @param v4Buf0 計算用。
	 * @param qBuf0 計算用。
	 * @param qBuf1 計算用。
	 * @param mBuf0 計算用。
	 */
	private void transformOneBone(
			BoneData bone,
			Vector3f vBuf0, Vector3f vBuf1, Vector4f v4Buf0,
			Quaternion qBuf0, Quaternion qBuf1, Matrix4f mBuf0)
	{
		// 回転
		Quaternion rotate = qBuf1;
		rotate.w = 1.0F;
		rotate.x = 0.0F;
		rotate.y = 0.0F;
		rotate.z = 0.0F;

		if (bone.rotateAddition)
		{
			BoneData additionParent = this.boneData[bone.additionParentIndex];

			if (bone.localAddition)
			{
				Quaternion.mul(rotate, Util.toQuaternion(
						Util.getRotation(additionParent.localMatrix, mBuf0), qBuf0), rotate);
			}
			else
			{
				if (additionParent.rotateAddition)
				{
					Quaternion.mul(rotate, additionParent.addRotate, rotate);
				}
				else
				{
					Quaternion.mul(rotate, additionParent.rotate, rotate);
					Quaternion.mul(rotate, additionParent.morphRotate, rotate);
				}

				Quaternion.mul(rotate, additionParent.ikRotate, rotate);
			}

			if (bone.additionRatio != 1.0F)
			{
				qBuf0.w = 1.0F;
				qBuf0.x = 0.0F;
				qBuf0.x = 0.0F;
				qBuf0.x = 0.0F;
				Util.slerp(rotate, qBuf0, bone.additionRatio, rotate);
			}
			bone.addRotate.w = rotate.w;
			bone.addRotate.x = rotate.x;
			bone.addRotate.y = rotate.y;
			bone.addRotate.z = rotate.z;
		}

		Quaternion.mul(rotate, bone.rotate, rotate);
		Quaternion.mul(rotate, bone.morphRotate, rotate);
		Quaternion.mul(rotate, bone.ikRotate, rotate);

		// 移動
		Vector3f translate = vBuf1;
		translate.x = 0.0F;
		translate.y = 0.0F;
		translate.z = 0.0F;

		if (bone.translateAddition)
		{
			BoneData additionParent = this.boneData[bone.additionParentIndex];

			if (bone.localAddition)
			{
				Util.getTranslation(additionParent.localMatrix, vBuf0);
				translate.x += vBuf0.x;
				translate.y += vBuf0.y;
				translate.z += vBuf0.z;
			}
			else
			{
				if (additionParent.translateAddition)
				{
					translate.x += additionParent.addTranslate.x;
					translate.y += additionParent.addTranslate.y;
					translate.z += additionParent.addTranslate.z;
				}
				else
				{
					translate.x += additionParent.translate.x;
					translate.y += additionParent.translate.y;
					translate.z += additionParent.translate.z;

					translate.x += additionParent.morphTranslate.x;
					translate.y += additionParent.morphTranslate.y;
					translate.z += additionParent.morphTranslate.z;
				}
			}

			if (bone.additionRatio != 1.0F)
			{
				translate.x *= bone.additionRatio;
				translate.y *= bone.additionRatio;
				translate.z *= bone.additionRatio;
			}
			bone.addTranslate.x = translate.x;
			bone.addTranslate.y = translate.y;
			bone.addTranslate.z = translate.z;
		}

		translate.x += bone.translate.x;
		translate.y += bone.translate.y;
		translate.z += bone.translate.z;

		translate.x += bone.morphTranslate.x;
		translate.y += bone.morphTranslate.y;
		translate.z += bone.morphTranslate.z;

		// 変換適用
		bone.localMatrix.setIdentity();
		Matrix4f.mul(Util.toMatrix(rotate, mBuf0), bone.localMatrix, bone.localMatrix);
		Matrix4f.mul(Util.toMatrix(translate, mBuf0), bone.localMatrix, bone.localMatrix);
		if (bone.parentIndex >= 0)
		{
			vBuf0.x = bone.p.x - this.boneData[bone.parentIndex].p.x;
			vBuf0.y = bone.p.y - this.boneData[bone.parentIndex].p.y;
			vBuf0.z = bone.p.z - this.boneData[bone.parentIndex].p.z;

			Matrix4f.mul(Util.toMatrix(vBuf0, mBuf0), bone.localMatrix, bone.localMatrix);
			Matrix4f.mul(this.boneData[bone.parentIndex].localMatrix,
					bone.localMatrix, bone.localMatrix);
			Matrix4f.mul(Util.toMatrix(vBuf0.negate(vBuf0), mBuf0), bone.localMatrix, bone.localMatrix);
		}

		v4Buf0.x = 0.0F;
		v4Buf0.y = 0.0F;
		v4Buf0.z = 0.0F;
		v4Buf0.w = 1.0F;

		Matrix4f.transform(bone.localMatrix, v4Buf0, v4Buf0);

		bone.localP.x = v4Buf0.x + bone.p.x;
		bone.localP.y = v4Buf0.y + bone.p.y;
		bone.localP.z = v4Buf0.z + bone.p.z;
	}

	/**
	 * 指定されたIKボーンのリンクについて、座標変換を適用します。
	 * @param bone 計算対象のIKボーン
	 * @param vBuf0 計算用。
	 * @param vBuf1 計算用。
	 * @param vBuf2 計算用。
	 * @param vBuf3 計算用。
	 * @param v4Buf0 計算用。
	 * @param qBuf0 計算用。
	 * @param mBuf0 計算用。
	 */
	private void transformIKLinks(BoneData bone,
			Vector3f vBuf0, Vector3f vBuf1, Vector3f vBuf2, Vector3f vBuf3, Vector4f v4Buf0,
			Quaternion qBuf0, Matrix4f mBuf0)
	{
		if (!bone.isIK)
		{
			return;
		}

		if (bone.ikLinkCount <= 0)
		{
			return;
		}

		// 物理ボーンが含まれていたら終了
		for (int i = 0; i < bone.ikLinkCount; ++i)
		{
			if (this.boneData[bone.ikLink[i].linkIndex].isPhysicsBone)
			{
				return;
			}
		}

		// 最端のIKリンクボーン
		BoneData endLink = this.boneData[bone.ikLink[0].linkIndex];

		// 最子ボーンを記録
		BoneData linkChild;
		if (endLink.children.length > 0)
		{
			linkChild = endLink.children[0];
		}
		else
		{
			linkChild = endLink;
		}

		for (int i = 0; i < bone.ikLoopCount; ++i)
		{
			for (int j = 0; j < bone.ikLinkCount; ++j)
			{
				// 回転対象のIKリンクボーン
				BoneData link = this.boneData[bone.ikLink[j].linkIndex];

				Matrix4f targetRot = mBuf0;
				Vector3f initDir = vBuf0, targetDir = vBuf1, axis = vBuf2;
				float angle;

				// 子ボーンへの方向を算出
				initDir.x = linkChild.localP.x - link.localP.x;
				initDir.y = linkChild.localP.y - link.localP.y;
				initDir.z = linkChild.localP.z - link.localP.z;
				if (initDir.length() < 0.0001F)
				{
					continue;
				}
				initDir.normalise();

				// IKへの方向を算出
				targetDir.x = bone.localP.x - link.localP.x;
				targetDir.y = bone.localP.y - link.localP.y;
				targetDir.z = bone.localP.z - link.localP.z;
				if (targetDir.length() < 0.0001F)
				{
					continue;
				}
				targetDir.normalise();

				// 回転軸を算出
				Vector3f.cross(initDir, targetDir, axis);
				if (axis.length() < 0.0001F)
				{
					continue;
				}
				axis.normalise();

				// 回転軸の変換状態を戻す
				v4Buf0.x = axis.x;
				v4Buf0.y = axis.y;
				v4Buf0.z = axis.z;
				v4Buf0.w = 1.0F;
				Matrix4f.transform(Matrix4f.invert(Util.getRotation(
						link.localMatrix, mBuf0), mBuf0), v4Buf0, v4Buf0);
				axis.x = v4Buf0.x;
				axis.y = v4Buf0.y;
				axis.z = v4Buf0.z;

				// 回転角を算出
				float dot = Vector3f.dot(initDir, targetDir);
				if (dot > 1.0F)
				{
					dot = 1.0F;
				}
				else if (dot < -1.0F)
				{
					dot = -1.0F;
				}
				angle = (float)Math.acos(dot);

				// 回転角の制限
				if (angle > bone.ikLoopConstRadian)
				{
					angle = bone.ikLoopConstRadian;
				}

				// 変換を適用
				targetRot.setIdentity();
				targetRot.rotate(angle, axis);
				Util.toQuaternion(targetRot, qBuf0);

				Quaternion.mul(link.ikRotate, qBuf0, link.ikRotate);

				// IK回転の回転範囲の制限
				if (bone.ikLink[j].constRadian)
				{
					Util.toMatrix(link.ikRotate, mBuf0);
					Util.getZXYAngle(mBuf0, vBuf0);

					if (vBuf0.x < bone.ikLink[j].min.x) vBuf0.x = bone.ikLink[j].min.x;
					if (vBuf0.x > bone.ikLink[j].max.x) vBuf0.x = bone.ikLink[j].max.x;
					if (vBuf0.y < bone.ikLink[j].min.y) vBuf0.y = bone.ikLink[j].min.y;
					if (vBuf0.y > bone.ikLink[j].max.y) vBuf0.y = bone.ikLink[j].max.y;
					if (vBuf0.z < bone.ikLink[j].min.z) vBuf0.z = bone.ikLink[j].min.z;
					if (vBuf0.z > bone.ikLink[j].max.z) vBuf0.z = bone.ikLink[j].max.z;

					Util.getZXYMatrix(vBuf0, mBuf0);
					Util.toQuaternion(mBuf0, link.ikRotate);
				}

				// 子ボーンに変換を適用
				transformChildBones(link, vBuf0, vBuf1, v4Buf0, qBuf0, qBuf0, mBuf0);
			}
		}
	}

	/**
	 * 指定されたボーンとその子ボーンの座標変換を適用します。
	 * @param bone 対象のボーン。
	 * @param vBuf0 計算用。
	 * @param vBuf1 計算用。
	 * @param v4Buf0 計算用。
	 * @param qBuf0 計算用。
	 * @param qBuf1 計算用。
	 * @param mBuf0 計算用。
	 */
	private void transformChildBones(
			BoneData bone,
			Vector3f vBuf0, Vector3f vBuf1, Vector4f v4Buf0,
			Quaternion qBuf0, Quaternion qBuf1, Matrix4f mBuf0)
	{
		transformOneBone(bone, vBuf0, vBuf1, v4Buf0, qBuf0, qBuf1, mBuf0);

		for (int i = 0; i < bone.children.length; ++i)
		{
			transformChildBones(bone.children[i], vBuf0, vBuf1, v4Buf0, qBuf0, qBuf1, mBuf0);
		}
	}

	/**
	 * 各データのモーフの座標変換を初期化します。
	 */
	protected void initMorphs()
	{
		for (int i = 0; i < this.vertexCount; ++i)
		{
			this.vertexData[i].morphP.x = 0.0F;
			this.vertexData[i].morphP.y = 0.0F;
			this.vertexData[i].morphP.z = 0.0F;
			this.vertexData[i].morphUV.x = 0.0F;
			this.vertexData[i].morphUV.y = 0.0F;

			if (this.vertexData[i].morphAddUV1 != null)
			{
				this.vertexData[i].morphAddUV1.x = 0.0F;
				this.vertexData[i].morphAddUV1.y = 0.0F;
				this.vertexData[i].morphAddUV1.z = 0.0F;
				this.vertexData[i].morphAddUV1.w = 0.0F;
			}
			if (this.vertexData[i].morphAddUV2 != null)
			{
				this.vertexData[i].morphAddUV2.x = 0.0F;
				this.vertexData[i].morphAddUV2.y = 0.0F;
				this.vertexData[i].morphAddUV2.z = 0.0F;
				this.vertexData[i].morphAddUV2.w = 0.0F;
			}
			if (this.vertexData[i].morphAddUV3 != null)
			{
				this.vertexData[i].morphAddUV3.x = 0.0F;
				this.vertexData[i].morphAddUV3.y = 0.0F;
				this.vertexData[i].morphAddUV3.z = 0.0F;
				this.vertexData[i].morphAddUV3.w = 0.0F;
			}
			if (this.vertexData[i].morphAddUV4 != null)
			{
				this.vertexData[i].morphAddUV4.x = 0.0F;
				this.vertexData[i].morphAddUV4.y = 0.0F;
				this.vertexData[i].morphAddUV4.z = 0.0F;
				this.vertexData[i].morphAddUV4.w = 0.0F;
			}
		}

		for (int i = 0; i < this.materialCount; ++i)
		{
			this.materialData[i].multMorphDiffuse.x = 1.0F;
			this.materialData[i].multMorphDiffuse.y = 1.0F;
			this.materialData[i].multMorphDiffuse.z = 1.0F;
			this.materialData[i].multMorphSpecular.x = 1.0F;
			this.materialData[i].multMorphSpecular.y = 1.0F;
			this.materialData[i].multMorphSpecular.z = 1.0F;
			this.materialData[i].multMorphSpecular.w = 1.0F;
			this.materialData[i].multMorphAmbient.x = 1.0F;
			this.materialData[i].multMorphAmbient.y = 1.0F;
			this.materialData[i].multMorphAmbient.z = 1.0F;
			this.materialData[i].multMorphEdgeColor.x = 1.0F;
			this.materialData[i].multMorphEdgeColor.y = 1.0F;
			this.materialData[i].multMorphEdgeColor.z = 1.0F;
			this.materialData[i].multMorphEdgeSize = 1.0F;
			this.materialData[i].multMorphTexture.x = 1.0F;
			this.materialData[i].multMorphTexture.y = 1.0F;
			this.materialData[i].multMorphTexture.z = 1.0F;
			this.materialData[i].multMorphSphere.x = 1.0F;
			this.materialData[i].multMorphSphere.y = 1.0F;
			this.materialData[i].multMorphSphere.z = 1.0F;
			this.materialData[i].multMorphToon.x = 1.0F;
			this.materialData[i].multMorphToon.y = 1.0F;
			this.materialData[i].multMorphToon.z = 1.0F;

			this.materialData[i].addMorphDiffuse.x = 0.0F;
			this.materialData[i].addMorphDiffuse.y = 0.0F;
			this.materialData[i].addMorphDiffuse.z = 0.0F;
			this.materialData[i].addMorphSpecular.x = 0.0F;
			this.materialData[i].addMorphSpecular.y = 0.0F;
			this.materialData[i].addMorphSpecular.z = 0.0F;
			this.materialData[i].addMorphSpecular.w = 0.0F;
			this.materialData[i].addMorphAmbient.x = 0.0F;
			this.materialData[i].addMorphAmbient.y = 0.0F;
			this.materialData[i].addMorphAmbient.z = 0.0F;
			this.materialData[i].addMorphEdgeColor.x = 0.0F;
			this.materialData[i].addMorphEdgeColor.y = 0.0F;
			this.materialData[i].addMorphEdgeColor.z = 0.0F;
			this.materialData[i].addMorphEdgeSize = 0.0F;
			this.materialData[i].addMorphTexture.x = 0.0F;
			this.materialData[i].addMorphTexture.y = 0.0F;
			this.materialData[i].addMorphTexture.z = 0.0F;
			this.materialData[i].addMorphSphere.x = 0.0F;
			this.materialData[i].addMorphSphere.y = 0.0F;
			this.materialData[i].addMorphSphere.z = 0.0F;
			this.materialData[i].addMorphToon.x = 0.0F;
			this.materialData[i].addMorphToon.y = 0.0F;
			this.materialData[i].addMorphToon.z = 0.0F;
		}

		for (int i = 0; i < this.boneCount; ++i)
		{
			this.boneData[i].morphTranslate.x = 0.0F;
			this.boneData[i].morphTranslate.y = 0.0F;
			this.boneData[i].morphTranslate.z = 0.0F;
			this.boneData[i].morphRotate.x = 0.0F;
			this.boneData[i].morphRotate.y = 0.0F;
			this.boneData[i].morphRotate.z = 0.0F;
			this.boneData[i].morphRotate.w = 1.0F;
		}

		for (int i = 0; i < this.morphCount; ++i)
		{
			this.morphData[i].groupMorphRatio = 0.0F;
		}
	}

	/**
	 * 各モーフに対してユーザー指定の座標変換を設定します。
	 * @param vmd1 VMDデータ1。
	 * @param frame1 vmd1のフレーム番号。
	 * @param vmd2 VMDデータ2。
	 * @param frame2 vmd2のフレーム番号。
	 * @param t vmd1とvmd2のブレンド係数。
	 */
	protected void setUserTransformToMorphs(VMDData vmd1, int frame1, VMDData vmd2, int frame2, float t)
	{
		for (MorphData morph : this.morphData)
		{
			if (vmd1 != null)
			{
				vmd1.getMorphFrame(morph.name, frame1, morphBuf0);
			}
			else
			{
				morphBuf0.value = 0.0F;
			}

			if (vmd2 != null)
			{
				vmd2.getMorphFrame(morph.name, frame2, morphBuf1);
			}
			else
			{
				morphBuf1.value = 0.0F;
			}

			morph.ratio = morphBuf0.value * (1.0F - t) + morphBuf1.value * t;
		}
	}

	/**
	 * 各データに対してモーフの座標変換を適用します。
	 */
	protected void applyMorphs()
	{
		for (int i = 0; i < this.groupMorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[0][i].offsets)
			{
				MorphData.GroupMorph o = (MorphData.GroupMorph)offset;

				this.morphData[o.targetIndex].groupMorphRatio += o.ratio * this.tMorphData[0][i].ratio;
			}
		}

		for (int i = 0; i < this.vertexMorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[1][i].offsets)
			{
				MorphData.VertexOffset o = (MorphData.VertexOffset)offset;

				this.vertexData[o.targetIndex].morphP.x += o.p.x * this.tMorphData[1][i].ratio;
				this.vertexData[o.targetIndex].morphP.y += o.p.y * this.tMorphData[1][i].ratio;
				this.vertexData[o.targetIndex].morphP.z += o.p.z * this.tMorphData[1][i].ratio;
			}
		}

		for (int i = 0; i < this.boneMorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[2][i].offsets)
			{
				MorphData.BoneOffset o = (MorphData.BoneOffset)offset;

				this.boneData[o.targetIndex].morphTranslate.x += o.translate.x * this.tMorphData[2][i].ratio;
				this.boneData[o.targetIndex].morphTranslate.y += o.translate.y * this.tMorphData[2][i].ratio;
				this.boneData[o.targetIndex].morphTranslate.z += o.translate.z * this.tMorphData[2][i].ratio;

				qBuf0.w = o.rotate.w * this.tMorphData[2][i].ratio;
				qBuf0.x = o.rotate.x * this.tMorphData[2][i].ratio;
				qBuf0.y = o.rotate.y * this.tMorphData[2][i].ratio;
				qBuf0.z = o.rotate.z * this.tMorphData[2][i].ratio;
				Quaternion.mul(this.boneData[o.targetIndex].morphRotate, qBuf0, qBuf0);

				this.boneData[o.targetIndex].morphRotate.w = qBuf0.w;
				this.boneData[o.targetIndex].morphRotate.x = qBuf0.x;
				this.boneData[o.targetIndex].morphRotate.y = qBuf0.y;
				this.boneData[o.targetIndex].morphRotate.z = qBuf0.z;
			}
		}

		for (int i = 0; i < this.uvMorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[3][i].offsets)
			{
				MorphData.UVOffset o = (MorphData.UVOffset)offset;

				this.vertexData[o.targetIndex].morphUV.x += o.uv.x * this.tMorphData[3][i].ratio;
				this.vertexData[o.targetIndex].morphUV.y += o.uv.y * this.tMorphData[3][i].ratio;
			}
		}

		for (int i = 0; i < this.addUV1MorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[4][i].offsets)
			{
				MorphData.UVOffset o = (MorphData.UVOffset)offset;

				if (this.vertexData[o.targetIndex].morphAddUV1 != null)
				{
					this.vertexData[o.targetIndex].morphAddUV1.x += o.uv.x * this.tMorphData[4][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV1.y += o.uv.y * this.tMorphData[4][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV1.z += o.uv.z * this.tMorphData[4][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV1.w += o.uv.w * this.tMorphData[4][i].ratio;
				}
			}
		}

		for (int i = 0; i < this.addUV2MorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[5][i].offsets)
			{
				MorphData.UVOffset o = (MorphData.UVOffset)offset;

				if (this.vertexData[o.targetIndex].morphAddUV2 != null)
				{
					this.vertexData[o.targetIndex].morphAddUV2.x += o.uv.x * this.tMorphData[5][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV2.y += o.uv.y * this.tMorphData[5][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV2.z += o.uv.z * this.tMorphData[5][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV2.w += o.uv.w * this.tMorphData[5][i].ratio;
				}
			}
		}

		for (int i = 0; i < this.addUV3MorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[6][i].offsets)
			{
				MorphData.UVOffset o = (MorphData.UVOffset)offset;

				if (this.vertexData[o.targetIndex].morphAddUV3 != null)
				{
					this.vertexData[o.targetIndex].morphAddUV3.x += o.uv.x * this.tMorphData[6][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV3.y += o.uv.y * this.tMorphData[6][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV3.z += o.uv.z * this.tMorphData[6][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV3.w += o.uv.w * this.tMorphData[6][i].ratio;
				}
			}
		}

		for (int i = 0; i < this.addUV4MorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[7][i].offsets)
			{
				MorphData.UVOffset o = (MorphData.UVOffset)offset;

				if (this.vertexData[o.targetIndex].morphAddUV4 != null)
				{
					this.vertexData[o.targetIndex].morphAddUV4.x += o.uv.x * this.tMorphData[7][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV4.y += o.uv.y * this.tMorphData[7][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV4.z += o.uv.z * this.tMorphData[7][i].ratio;
					this.vertexData[o.targetIndex].morphAddUV4.w += o.uv.w * this.tMorphData[7][i].ratio;
				}
			}
		}

		for (int i = 0; i < this.materialMorphCount; ++i)
		{
			for (MorphData.MorphOffset offset : this.tMorphData[8][i].offsets)
			{
				MorphData.MaterialOffset o = (MorphData.MaterialOffset)offset;

				if (o.operator == 0)
				{
					this.materialData[o.targetIndex].multMorphDiffuse.x *=
							(1.0 - o.diffuse.w * this.tMorphData[8][i].ratio)
							+ o.diffuse.x * o.diffuse.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphDiffuse.y *=
							(1.0 - o.diffuse.w * this.tMorphData[8][i].ratio)
							+ o.diffuse.y * o.diffuse.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphDiffuse.z *=
							(1.0 - o.diffuse.w * this.tMorphData[8][i].ratio)
							+ o.diffuse.z * o.diffuse.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSpecular.x *=
							o.specular.x * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSpecular.y *=
							o.specular.y * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSpecular.z *=
							o.specular.z * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSpecular.w *=
							o.specular.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphAmbient.x *=
							o.ambient.x * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphAmbient.y *=
							o.ambient.y * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphAmbient.z *=
							o.ambient.z * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphEdgeColor.x *=
							(1.0 - o.edgeColor.w * this.tMorphData[8][i].ratio)
							+ o.edgeColor.x * o.edgeColor.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphEdgeColor.y *=
							(1.0 - o.edgeColor.w * this.tMorphData[8][i].ratio)
							+ o.edgeColor.y * o.edgeColor.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphEdgeColor.z *=
							(1.0 - o.edgeColor.w * this.tMorphData[8][i].ratio)
							+ o.edgeColor.z * o.edgeColor.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphEdgeSize *= o.edgeSize * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphTexture.x *=
							(1.0 - o.texture.w * this.tMorphData[8][i].ratio)
							+ o.texture.x * o.texture.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphTexture.y *=
							(1.0 - o.texture.w * this.tMorphData[8][i].ratio)
							+ o.texture.y * o.texture.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphTexture.z *=
							(1.0 - o.texture.w * this.tMorphData[8][i].ratio)
							+ o.texture.z * o.texture.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSphere.x *=
							(1.0 - o.sphere.w * this.tMorphData[8][i].ratio)
							+ o.sphere.x * o.sphere.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSphere.y *=
							(1.0 - o.sphere.w * this.tMorphData[8][i].ratio)
							+ o.sphere.y * o.sphere.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphSphere.z *=
							(1.0 - o.sphere.w * this.tMorphData[8][i].ratio)
							+ o.sphere.z * o.sphere.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphToon.x *=
							(1.0 - o.toon.w * this.tMorphData[8][i].ratio)
							+ o.toon.x * o.toon.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphToon.y *=
							(1.0 - o.toon.w * this.tMorphData[8][i].ratio)
							+ o.toon.y * o.toon.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].multMorphToon.z *=
							(1.0 - o.toon.w * this.tMorphData[8][i].ratio)
							+ o.toon.z * o.toon.w * this.tMorphData[8][i].ratio;
				}
				else if (o.operator == 1)
				{
					this.materialData[o.targetIndex].addMorphDiffuse.x +=
							o.diffuse.x * o.diffuse.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphDiffuse.y +=
							o.diffuse.y * o.diffuse.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphDiffuse.z +=
							o.diffuse.z * o.diffuse.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSpecular.x += o.specular.x * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSpecular.y += o.specular.y * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSpecular.z += o.specular.z * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSpecular.w += o.specular.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphAmbient.x += o.ambient.x * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphAmbient.y += o.ambient.y * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphAmbient.z += o.ambient.z * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphEdgeColor.x +=
							o.edgeColor.x * o.edgeColor.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphEdgeColor.y +=
							o.edgeColor.y * o.edgeColor.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphEdgeColor.z +=
							o.edgeColor.z * o.edgeColor.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphEdgeSize += o.edgeSize * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphTexture.x +=
							o.texture.x * o.texture.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphTexture.y +=
							o.texture.y * o.texture.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphTexture.z +=
							o.texture.z * o.texture.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSphere.x +=
							o.sphere.x * o.sphere.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSphere.y +=
							o.sphere.y * o.sphere.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphSphere.z +=
							o.sphere.z * o.sphere.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphToon.x +=
							o.toon.x * o.toon.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphToon.y +=
							o.toon.y * o.toon.w * this.tMorphData[8][i].ratio;
					this.materialData[o.targetIndex].addMorphToon.z +=
							o.toon.z * o.toon.w * this.tMorphData[8][i].ratio;
				}
			}
		}
	}

	/**
	 * 物理演算による座標変換を適用します。
	 * @param deltaTime 前フレームからの経過時間。
	 */
	protected void physicalTransform(Physics physics, float deltaTime)
	{
		physics.transformRigidBody(deltaTime);
	}

	/**
	 * 各頂点の変換状態を初期化します。
	 */
	protected void initVertices()
	{
		for (int i = 0; i < this.vertexCount; ++i)
		{
			vertexData[i].renderP.x = vertexData[i].p.x;
			vertexData[i].renderP.y = vertexData[i].p.y;
			vertexData[i].renderP.z = vertexData[i].p.z;
			vertexData[i].renderN.x = vertexData[i].n.x;
			vertexData[i].renderN.y = vertexData[i].n.y;
			vertexData[i].renderN.z = vertexData[i].n.z;
			vertexData[i].renderUV.x = vertexData[i].uv.x;
			vertexData[i].renderUV.y = vertexData[i].uv.y;

			if (vertexData[i].renderAddUV1 != null)
			{
				vertexData[i].renderAddUV1.x = vertexData[i].addUV1.x;
				vertexData[i].renderAddUV1.y = vertexData[i].addUV1.y;
				vertexData[i].renderAddUV1.z = vertexData[i].addUV1.z;
				vertexData[i].renderAddUV1.w = vertexData[i].addUV1.w;
			}
			if (vertexData[i].renderAddUV2 != null)
			{
				vertexData[i].renderAddUV2.x = vertexData[i].addUV2.x;
				vertexData[i].renderAddUV2.y = vertexData[i].addUV2.y;
				vertexData[i].renderAddUV2.z = vertexData[i].addUV2.z;
				vertexData[i].renderAddUV2.w = vertexData[i].addUV2.w;
			}
			if (vertexData[i].renderAddUV3 != null)
			{
				vertexData[i].renderAddUV3.x = vertexData[i].addUV3.x;
				vertexData[i].renderAddUV3.y = vertexData[i].addUV3.y;
				vertexData[i].renderAddUV3.z = vertexData[i].addUV3.z;
				vertexData[i].renderAddUV3.w = vertexData[i].addUV3.w;
			}
			if (vertexData[i].renderAddUV4 != null)
			{
				vertexData[i].renderAddUV4.x = vertexData[i].addUV4.x;
				vertexData[i].renderAddUV4.y = vertexData[i].addUV4.y;
				vertexData[i].renderAddUV4.z = vertexData[i].addUV4.z;
				vertexData[i].renderAddUV4.w = vertexData[i].addUV4.w;
			}
		}
	}

	/**
	 * 各頂点に座標変換を適用します。
	 */
	protected void transformVertices()
	{
		for (int i = 0; i < this.vertexCount; ++i)
		{
			this.vertexData[i].renderP.x += this.vertexData[i].morphP.x;
			this.vertexData[i].renderP.y += this.vertexData[i].morphP.y;
			this.vertexData[i].renderP.z += this.vertexData[i].morphP.z;
			this.vertexData[i].renderUV.x += this.vertexData[i].morphUV.x;
			this.vertexData[i].renderUV.y += this.vertexData[i].morphUV.y;
			if (vertexData[i].renderAddUV1 != null)
			{
				this.vertexData[i].renderAddUV1.x += this.vertexData[i].morphAddUV1.x;
				this.vertexData[i].renderAddUV1.y += this.vertexData[i].morphAddUV1.y;
				this.vertexData[i].renderAddUV1.z += this.vertexData[i].morphAddUV1.z;
				this.vertexData[i].renderAddUV1.w += this.vertexData[i].morphAddUV1.w;
			}
			if (vertexData[i].renderAddUV2 != null)
			{
				this.vertexData[i].renderAddUV2.x += this.vertexData[i].morphAddUV2.x;
				this.vertexData[i].renderAddUV2.y += this.vertexData[i].morphAddUV2.y;
				this.vertexData[i].renderAddUV2.z += this.vertexData[i].morphAddUV2.z;
				this.vertexData[i].renderAddUV2.w += this.vertexData[i].morphAddUV2.w;
			}
			if (vertexData[i].renderAddUV3 != null)
			{
				this.vertexData[i].renderAddUV3.x += this.vertexData[i].morphAddUV3.x;
				this.vertexData[i].renderAddUV3.y += this.vertexData[i].morphAddUV3.y;
				this.vertexData[i].renderAddUV3.z += this.vertexData[i].morphAddUV3.z;
				this.vertexData[i].renderAddUV3.w += this.vertexData[i].morphAddUV3.w;
			}
			if (vertexData[i].renderAddUV4 != null)
			{
				this.vertexData[i].renderAddUV4.x += this.vertexData[i].morphAddUV4.x;
				this.vertexData[i].renderAddUV4.y += this.vertexData[i].morphAddUV4.y;
				this.vertexData[i].renderAddUV4.z += this.vertexData[i].morphAddUV4.z;
				this.vertexData[i].renderAddUV4.w += this.vertexData[i].morphAddUV4.w;
			}

			Vector4f p = v4Buf0;
			p.x = this.vertexData[i].renderP.x;
			p.y = this.vertexData[i].renderP.y;
			p.z = this.vertexData[i].renderP.z;
			p.w = 1.0F;
			Vector4f n = v4Buf1;
			n.x = this.vertexData[i].renderN.x;
			n.y = this.vertexData[i].renderN.y;
			n.z = this.vertexData[i].renderN.z;
			n.w = 1.0F;

			if (this.vertexData[i].boneBdefMethod == 0)
			{
				Vector3f c = this.boneData[this.vertexData[i].boneIndex1].p;

				Matrix4f m = this.boneData[this.vertexData[i].boneIndex1].localMatrix;

				p.x -= c.x;
				p.y -= c.y;
				p.z -= c.z;

				Matrix4f.transform(m, p, p);
				Matrix4f.transform(Util.getRotation(m, mBuf0), n, n);

				p.x += c.x;
				p.y += c.y;
				p.z += c.z;
			}
			else if (this.vertexData[i].boneBdefMethod == 1)
			{
				Vector3f c = vBuf0;
				c.x = this.vertexData[i].boneWeight1
						* this.boneData[this.vertexData[i].boneIndex1].p.x
						+ (1.0F - this.vertexData[i].boneWeight1)
						* this.boneData[this.vertexData[i].boneIndex2].p.x;
				c.y = this.vertexData[i].boneWeight1
						* this.boneData[this.vertexData[i].boneIndex1].p.y
						+ (1.0F - this.vertexData[i].boneWeight1)
						* this.boneData[this.vertexData[i].boneIndex2].p.y;
				c.z = this.vertexData[i].boneWeight1
						* this.boneData[this.vertexData[i].boneIndex1].p.z
						+ (1.0F - this.vertexData[i].boneWeight1)
						* this.boneData[this.vertexData[i].boneIndex2].p.z;

				Matrix4f m = Util.lerp(
						this.boneData[this.vertexData[i].boneIndex1].localMatrix,
						this.boneData[this.vertexData[i].boneIndex2].localMatrix,
						1.0F - this.vertexData[i].boneWeight1, mBuf0);

				p.x -= c.x;
				p.y -= c.y;
				p.z -= c.z;

				Matrix4f.transform(m, p, p);
				Matrix4f.transform(Util.getRotation(m, mBuf0), n, n);

				p.x += c.x;
				p.y += c.y;
				p.z += c.z;
			}
			else if (this.vertexData[i].boneBdefMethod == 2 || this.vertexData[i].boneBdefMethod == 4)
			{
				Vector3f c = vBuf0;
				c.x = this.vertexData[i].boneWeight1 * this.boneData[this.vertexData[i].boneIndex1].p.x
						+ this.vertexData[i].boneWeight2 * this.boneData[this.vertexData[i].boneIndex2].p.x
						+ this.vertexData[i].boneWeight3 * this.boneData[this.vertexData[i].boneIndex3].p.x
						+ this.vertexData[i].boneWeight4 * this.boneData[this.vertexData[i].boneIndex4].p.x;
				c.y = this.vertexData[i].boneWeight1 * this.boneData[this.vertexData[i].boneIndex1].p.y
						+ this.vertexData[i].boneWeight2 * this.boneData[this.vertexData[i].boneIndex2].p.y
						+ this.vertexData[i].boneWeight3 * this.boneData[this.vertexData[i].boneIndex3].p.y
						+ this.vertexData[i].boneWeight4 * this.boneData[this.vertexData[i].boneIndex4].p.y;
				c.z = this.vertexData[i].boneWeight1 * this.boneData[this.vertexData[i].boneIndex1].p.z
						+ this.vertexData[i].boneWeight2 * this.boneData[this.vertexData[i].boneIndex2].p.z
						+ this.vertexData[i].boneWeight3 * this.boneData[this.vertexData[i].boneIndex3].p.z
						+ this.vertexData[i].boneWeight4 * this.boneData[this.vertexData[i].boneIndex4].p.z;

				Matrix4f m = Util.lerp4(
						this.boneData[this.vertexData[i].boneIndex1].localMatrix,
						this.boneData[this.vertexData[i].boneIndex2].localMatrix,
						this.boneData[this.vertexData[i].boneIndex3].localMatrix,
						this.boneData[this.vertexData[i].boneIndex4].localMatrix,
						this.vertexData[i].boneWeight1,
						this.vertexData[i].boneWeight2,
						this.vertexData[i].boneWeight3,
						this.vertexData[i].boneWeight4,
						mBuf0);

				p.x -= c.x;
				p.y -= c.y;
				p.z -= c.z;

				Matrix4f.transform(m, p, p);
				Matrix4f.transform(Util.getRotation(m, mBuf0), n, n);

				p.x += c.x;
				p.y += c.y;
				p.z += c.z;
			}
			else if (this.vertexData[i].boneBdefMethod == 3)
			{
				Vector3f c = vBuf0;
				c.x = this.vertexData[i].boneWeight1
						* this.boneData[this.vertexData[i].boneIndex1].p.x
						+ (1.0F - this.vertexData[i].boneWeight1)
						* this.boneData[this.vertexData[i].boneIndex2].p.x;
				c.y = this.vertexData[i].boneWeight1
						* this.boneData[this.vertexData[i].boneIndex1].p.y
						+ (1.0F - this.vertexData[i].boneWeight1)
						* this.boneData[this.vertexData[i].boneIndex2].p.y;
				c.z = this.vertexData[i].boneWeight1
						* this.boneData[this.vertexData[i].boneIndex1].p.z
						+ (1.0F - this.vertexData[i].boneWeight1)
						* this.boneData[this.vertexData[i].boneIndex2].p.z;

				Quaternion q1 = Util.toQuaternion(Util.getRotation(
						this.boneData[this.vertexData[i].boneIndex1].localMatrix, mBuf0), qBuf0);
				Quaternion q2 = Util.toQuaternion(Util.getRotation(
						this.boneData[this.vertexData[i].boneIndex2].localMatrix, mBuf0), qBuf1);
				Quaternion qr = Util.slerp(q1, q2, 1.0F - this.vertexData[i].boneWeight1, qBuf0);

				Vector3f t1 = Util.getTranslation(this.boneData[this.vertexData[i].boneIndex1].localMatrix, vBuf1);
				Vector3f t2 = Util.getTranslation(this.boneData[this.vertexData[i].boneIndex2].localMatrix, vBuf2);
				Vector3f tr = vBuf3;
				tr.x = this.vertexData[i].boneWeight1 * t1.x + (1.0F - this.vertexData[i].boneWeight1) * t2.x;
				tr.y = this.vertexData[i].boneWeight1 * t1.y + (1.0F - this.vertexData[i].boneWeight1) * t2.y;
				tr.z = this.vertexData[i].boneWeight1 * t1.z + (1.0F - this.vertexData[i].boneWeight1) * t2.z;

				Matrix4f.mul(Util.toMatrix(tr, mBuf0), Util.toMatrix(qr, mBuf1), mBuf0);

				p.x -= c.x;
				p.y -= c.y;
				p.z -= c.z;

				Matrix4f.transform(mBuf0, p, p);
				Matrix4f.transform(Util.getRotation(mBuf0, mBuf0), n, n);

				p.x += c.x;
				p.y += c.y;
				p.z += c.z;
			}

			this.vertexData[i].renderP.x = p.x;
			this.vertexData[i].renderP.y = p.y;
			this.vertexData[i].renderP.z = p.z;

			this.vertexData[i].renderN.x = n.x;
			this.vertexData[i].renderN.y = n.y;
			this.vertexData[i].renderN.z = n.z;
		}
	}

	/**
	 * 辺データを構築します。
	 */
	protected void structureEdges()
	{
		// 頂点インデックスに関連づいた辺データを格納するマップ
		Map<Integer, List<EdgeData>> edgeMap = new HashMap<Integer, List<EdgeData>>();
		this.edgeCount = 0;

		// 面データを走査する
		for (int i = 0; i < this.faceCount; ++i)
		{
			// 面の頂点座標
			int v1 = this.faceData[i].vertexIndex1;
			int v2 = this.faceData[i].vertexIndex2;
			int v3 = this.faceData[i].vertexIndex3;
			//面に関連づいた辺のリスト
			List<EdgeData> v1List = edgeMap.get(v1);
			List<EdgeData> v2List = edgeMap.get(v2);
			List<EdgeData> v3List = edgeMap.get(v3);
			EdgeData existEdge;

			// 頂点1
			existEdge = null;
			if (v2List != null)
			{
				for (EdgeData e : v2List)
				{
					if (e.vertex2.index == v1)
					{
						existEdge = e;
						break;
					}
				}
			}
			if (existEdge == null)  // 辺が存在しない場合
			{
				if (v1List == null)
				{
					v1List = new ArrayList<EdgeData>(4);
					edgeMap.put(v1, v1List);
				}

				EdgeData newEdge = new EdgeData();
				newEdge.vertexIndex1 = this.vertexData[v1].index;
				newEdge.vertexIndex2 = this.vertexData[v2].index;
				newEdge.vertex1 = this.vertexData[v1];
				newEdge.vertex2 = this.vertexData[v2];
				newEdge.face1 = this.faceData[i];
				newEdge.edgeScale = (this.vertexData[v1].edgeScale + this.vertexData[v2].edgeScale) / 2.0F;
				v1List.add(newEdge);
				++this.edgeCount;

				this.vertexData[v1].edges.add(newEdge);
				this.vertexData[v2].edges.add(newEdge);
				this.faceData[i].edge1 = newEdge;
			}
			else  // 辺が存在する場合
			{
				existEdge.face2 = this.faceData[i];

				this.faceData[i].edge1 = existEdge;
			}

			// 頂点2
			existEdge = null;
			if (v3List != null)
			{
				for (EdgeData e : v3List)
				{
					if (e.vertex2.index == v2)
					{
						existEdge = e;
						break;
					}
				}
			}
			if (existEdge == null)  // 辺が存在しない場合
			{
				if (v2List == null)
				{
					v2List = new ArrayList<EdgeData>(4);
					edgeMap.put(v2, v2List);
				}

				EdgeData newEdge = new EdgeData();
				newEdge.vertexIndex1 = this.vertexData[v2].index;
				newEdge.vertexIndex2 = this.vertexData[v3].index;
				newEdge.vertex1 = this.vertexData[v2];
				newEdge.vertex2 = this.vertexData[v3];
				newEdge.face1 = this.faceData[i];
				newEdge.edgeScale = (this.vertexData[v2].edgeScale + this.vertexData[v3].edgeScale) / 2.0F;
				v2List.add(newEdge);
				++this.edgeCount;

				this.vertexData[v2].edges.add(newEdge);
				this.vertexData[v3].edges.add(newEdge);
				this.faceData[i].edge2 = newEdge;
			}
			else  // 辺が存在する場合
			{
				existEdge.face2 = this.faceData[i];

				this.faceData[i].edge2 = existEdge;
			}

			// 頂点3
			existEdge = null;
			if (v1List != null)
			{
				for (EdgeData e : v1List)
				{
					if (e.vertex2.index == v3)
					{
						existEdge = e;
						break;
					}
				}
			}
			if (existEdge == null)  // 辺が存在しない場合
			{
				if (v3List == null)
				{
					v3List = new ArrayList<EdgeData>(4);
					edgeMap.put(v3, v3List);
				}

				EdgeData newEdge = new EdgeData();
				newEdge.vertexIndex1 = this.vertexData[v3].index;
				newEdge.vertexIndex2 = this.vertexData[v1].index;
				newEdge.vertex1 = this.vertexData[v3];
				newEdge.vertex2 = this.vertexData[v1];
				newEdge.face1 = this.faceData[i];
				newEdge.edgeScale = (this.vertexData[v3].edgeScale + this.vertexData[v1].edgeScale) / 2.0F;
				v3List.add(newEdge);
				++this.edgeCount;

				this.vertexData[v3].edges.add(newEdge);
				this.vertexData[v1].edges.add(newEdge);
				this.faceData[i].edge3 = newEdge;
			}
			else  // 辺が存在する場合
			{
				existEdge.face2 = this.faceData[i];

				this.faceData[i].edge3 = existEdge;
			}
		}

		this.edgeData = new EdgeData[edgeCount];

		// edgeMapを走査し、辺データを再構成
		int index = 0;
		for (Integer i : edgeMap.keySet())
		{
			List<EdgeData> list = edgeMap.get(i);
			for (EdgeData e : list)
			{
				this.edgeData[index] = e;
				e.index = index;

				++index;
			}
		}
	}

	/**
	 * byteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void read(LittleEndianInputStream in, byte[] b, int off, int len) throws IOException
	{
		if (in.read(b, off, len) != len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号なしbyteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readUByte(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readUByte(b, off, len) != Byte.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号ありbyteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readByte(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readByte(b, off, len) != Byte.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号なしshortデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readUShort(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readUShort(b, off, len) != Short.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * byteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readShort(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readShort(b, off, len) != Short.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * intデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readInt(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readInt(b, off, len) != Integer.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * floatデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readFloat(LittleEndianInputStream in, float[] b, int off, int len) throws IOException
	{
		if (in.readFloat(b, off, len) != Float.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号なしインデックスデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @param dataSize インデックスデータのバイト数（1、2、4）。
	 * @throws IOException
	 */
	protected void readUIndex(LittleEndianInputStream in, int[] b, int off, int len, int dataSize) throws IOException
	{
		switch (dataSize)
		{
		case 1:
			readUByte(in, b, off, len);
			break;
		case 2:
			readUShort(in, b, off, len);
			break;
		case 4:
			readInt(in, b, off, len);
			break;
		default:
			throw new IllegalArgumentException("Invalid data size.");
		}
	}

	/**
	 * 符号ありインデックスデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @param dataSize インデックスデータのバイト数（1、2、4）。
	 * @throws IOException
	 */
	protected void readIndex(LittleEndianInputStream in, int[] b, int off, int len, int dataSize) throws IOException
	{
		switch (dataSize)
		{
		case 1:
			readByte(in, b, off, len);
			break;
		case 2:
			readShort(in, b, off, len);
			break;
		case 4:
			readInt(in, b, off, len);
			break;
		default:
			throw new IllegalArgumentException("Invalid data size.");
		}
	}

	/**
	 * テキストデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @param encoding テキストのエンコード方式。
	 * @throws IOException
	 */
	protected void readTextBuf(LittleEndianInputStream in, String[] b, int off, int len, Charset encoding)
			throws IOException
	{
		byte[] buf = new byte[this.bufferSize];
		int[] sLen = new int[1];

		for (int i = off; i < off + len; ++i)
		{
			// 後続バイト数を読み取る
			if (in.readInt(sLen, 0, 1) != Integer.SIZE / 8)
			{
				throw new IOException(this.invalidPMXDataMessage);
			}

			// 文字列を読み取る
			if (in.read(buf, 0, sLen[0]) != sLen[0])
			{
				throw new IOException(this.invalidPMXDataMessage);
			}

			b[i] = new String(buf, 0, sLen[0], encoding);
		}
	}
}
