package tana.modding.mmdmod.core;


/**
 * PMXのモデル情報です。
 * @author tana
 */
public class ModelInfo
{
	public String modelName = null;
	public String modelNameEn = null;
	public String comment = null;
	public String commentEn = null;
}
