package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

/**
 * 各種機能を提供します。
 * @author tana
 */
public class Util
{
	/**
	 * 計算用のベクトルバッファです。
	 */
	private static Vector3f vBuf = new Vector3f();

	/**
	 * 行列の移動成分を取り出します。
	 * @param m 対象の行列。
	 * @param dest 変換後の移動ベクトルが返される。
	 * @return destと同じインスタンス。
	 */
	public static Vector3f getTranslation(Matrix4f m, Vector3f dest)
	{
		if (dest == null)
		{
			dest = new Vector3f();
		}

		if (m.m33 != 0.0F)
		{
			dest.x = m.m30 / m.m33;
			dest.y = m.m31 / m.m33;
			dest.z = m.m32 / m.m33;
		}
		else
		{
			dest.x = 0.0F;
			dest.y = 0.0F;
			dest.z = 0.0F;
		}
		return dest;
	}

	/**
	 * 行列の回転成分を取り出します。
	 * @param m 対象の行列。
	 * @param dest 変換後の回転行列が返される。
	 * @return destと同じインスタンス。
	 */
	public static Matrix4f getRotation(Matrix4f m, Matrix4f dest)
	{
		if (dest == null)
		{
			dest = new Matrix4f();
		}

		dest.m00 = m.m00;
		dest.m01 = m.m01;
		dest.m02 = m.m02;
		dest.m03 = 0.0F;
		dest.m10 = m.m10;
		dest.m11 = m.m11;
		dest.m12 = m.m12;
		dest.m13 = 0.0F;
		dest.m20 = m.m20;
		dest.m21 = m.m21;
		dest.m22 = m.m22;
		dest.m23 = 0.0F;
		dest.m30 = 0.0F;
		dest.m31 = 0.0F;
		dest.m32 = 0.0F;
		dest.m33 = m.m33;

		return dest;
	}

	/**
	 * 回転行列からZXY回転の各成分を取り出します。
	 * @param m 対象の回転行列。
	 * @param dest XYZ回転成分が返される。
	 * @return destと同じインスタンス。
	 */
	public static Vector3f getZXYAngle(Matrix4f m, Vector3f dest)
	{
		if (dest == null)
		{
			dest = new Vector3f();
		}

		if (m.m12 >= 1.0F)
		{
			dest.x = (float)Math.PI * 0.5F;
			dest.y = 0.0F;
			dest.z = (float)Math.atan2(m.m01, m.m00);
		}
		else if (m.m12 <= -1.0F)
		{
			dest.x = -(float)Math.PI * 0.5F;
			dest.y = 0.0F;
			dest.z = (float)Math.atan2(m.m01, m.m00);
		}
		else
		{
			dest.y = (float)Math.atan2(-m.m02, m.m22);
			dest.z = (float)Math.atan2(-m.m10, m.m11);

			float sx = m.m12;
			if (sx > 1.0F)
			{
				sx = 1.0F;
			}
			else if (sx < -1.0F)
			{
				sx = -1.0F;
			}

			float cx;
			float cz = (float)Math.cos(dest.z);
			if (Math.abs(cz) > 0.0001F)
			{
				cx = m.m11 / cz;
			}
			else
			{
				cx = -m.m10;
			}
			dest.x = (float)Math.atan2(sx, cx);
		}

		return dest;
	}

	/**
	 * ZXY回転角から回転行列を計算します。
	 * @param angle XYZ回転角。
	 * @param dest 計算結果の回転行列が返される。
	 * @return destと同じインスタンス。
	 */
	public static Matrix4f getZXYMatrix(Vector3f angle, Matrix4f dest)
	{
		if (dest == null)
		{
			dest = new Matrix4f();
		}

		dest.setIdentity();
		vBuf.set(0.0F, 1.0F, 0.0F);
		dest.rotate(angle.y, vBuf);
		vBuf.set(1.0F, 0.0F, 0.0F);
		dest.rotate(angle.x, vBuf);
		vBuf.set(0.0F, 0.0F, 1.0F);
		dest.rotate(angle.z, vBuf);

		return dest;
	}

	/**
	 * 移動ベクトルを移動行列に変換します。
	 * @param v 移動ベクトル。
	 * @param 変換後の移動行列が返される。
	 * @return destと同じインスタンス。
	 */
	public static Matrix4f toMatrix(Vector3f t, Matrix4f dest)
	{
		if (dest == null)
		{
			dest = new Matrix4f();
		}

		dest.m00 = 1.0F;
		dest.m01 = 0.0F;
		dest.m02 = 0.0F;
		dest.m03 = 0.0F;
		dest.m10 = 0.0F;
		dest.m11 = 1.0F;
		dest.m12 = 0.0F;
		dest.m13 = 0.0F;
		dest.m20 = 0.0F;
		dest.m21 = 0.0F;
		dest.m22 = 1.0F;
		dest.m23 = 0.0F;
		dest.m30 = t.x;
		dest.m31 = t.y;
		dest.m32 = t.z;
		dest.m33 = 1.0F;

		return dest;
	}

	/**
	 * クォータニオンを回転行列に変換します。
	 * @param q 変換対象のクォータニオン。
	 * @param dest 変換後の回転行列が返される。
	 * @return destと同じインスタンス。
	 */
	public static Matrix4f toMatrix(Quaternion q, Matrix4f dest)
	{
		if (dest == null)
		{
			dest = new Matrix4f();
		}

		dest.m00 = 1.0F - 2.0F * q.y * q.y - 2.0F * q.z * q.z;
		dest.m01 = 2.0F * q.x * q.y - 2.0F * q.w * q.z;
		dest.m02 = 2.0F * q.x * q.z + 2.0F * q.w * q.y;
		dest.m03 = 0.0F;
		dest.m10 = 2.0F * q.x * q.y + 2.0F * q.w * q.z;
		dest.m11 = 1.0F - 2.0F * q.x * q.x - 2.0F * q.z * q.z;
		dest.m12 = 2.0F * q.y * q.z - 2.0F * q.w * q.x;
		dest.m13 = 0.0F;
		dest.m20 = 2.0F * q.x * q.z - 2.0F * q.w * q.y;
		dest.m21 = 2.0F * q.y * q.z + 2.0F * q.w * q.x;
		dest.m22 = 1.0F - 2.0F * q.x * q.x - 2.0F * q.y * q.y;
		dest.m23 = 0.0F;
		dest.m30 = 0.0F;
		dest.m31 = 0.0F;
		dest.m32 = 0.0F;
		dest.m33 = 1.0F;

		return dest;
	}

	/**
	 * 回転行列をクォータニオンに変換します。
	 * @param m 変換対象の回転行列。
	 * @param dest 変換後のクォータニオンが返される。
	 * @return destと同じインスタンス。
	 */
	public static Quaternion toQuaternion(Matrix4f m, Quaternion dest)
	{
		if (dest == null)
		{
			dest = new Quaternion();
		}

		float[] elem = new float[4];  // 0:x, 1:y, 2:z, 3:w
		elem[0] = m.m00 - m.m11 - m.m22 + 1.0f;
		elem[1] = -m.m00 + m.m11 - m.m22 + 1.0f;
		elem[2] = -m.m00 - m.m11 + m.m22 + 1.0f;
		elem[3] = m.m00 + m.m11 + m.m22 + 1.0f;

		int biggestIndex = 0;
		for (int i = 1; i < 4; i++)
		{
			if (elem[i] > elem[biggestIndex])
			{
				biggestIndex = i;
			}
		}

		if (elem[biggestIndex] < 0.0F)
		{
			// 引数の行列に間違いあり
			dest.x = 0.0F;
			dest.y = 0.0F;
			dest.z = 0.0F;
			dest.w = 0.0F;
			return dest;
		}

		float v = (float)Math.sqrt(elem[biggestIndex]) * 0.5F;
		float mult = 0.25f / v;

		switch ( biggestIndex )
		{
		case 0:  // x
			dest.x = v;
			dest.y = (m.m10 + m.m01) * mult;
			dest.z = (m.m02 + m.m20) * mult;
			dest.w = (m.m21 - m.m12) * mult;
			break;
		case 1:  // y
			dest.x = (m.m10 + m.m01) * mult;
			dest.y = v;
			dest.z = (m.m21 + m.m12) * mult;
			dest.w = (m.m02 - m.m20) * mult;
			break;
		case 2:  // z
			dest.x = (m.m02 + m.m20) * mult;
			dest.y = (m.m21 + m.m12) * mult;
			dest.z = v;
			dest.w = (m.m10 - m.m01) * mult;
			break;
		case 3:  // w
			dest.x = (m.m21 - m.m12) * mult;
			dest.y = (m.m02 - m.m20) * mult;
			dest.z = (m.m10 - m.m01) * mult;
			dest.w = v;
			break;
		}

		return dest;
	}

	/**
	 * cを中心とした3点間を二次関数的に補間します。
	 * @param c 中心点の値。
	 * @param r0 補間開始地点の値。
	 * @param r1 補間終了地点の値。
	 * @param t 開始地点からの割合。
	 * @return 補間された値。
	 */
	public static float plerp(float c, float r0, float r1, float t)
	{
		return (2.0F * r0 - 4.0F * c + 2.0F * r1) * t * t + (-3.0F * r0 + 4.0F * c - r1) * t + r0;
	}

	/**
	 * 行列の線形補間を計算します。
	 * @param m0 補間開始地点の値。
	 * @param m1 補間終了地点の値。
	 * @param t 開始地点からの割合。
	 * @param dest 補間された値が返される。
	 * @return destと同じインスタンス。
	 */
	public static Matrix4f lerp(Matrix4f m0, Matrix4f m1, float t, Matrix4f dest)
	{
		if (dest == null)
		{
			dest = new Matrix4f();
		}

		dest.m00 = (1.0F - t) * m0.m00 + t * m1.m00;
		dest.m01 = (1.0F - t) * m0.m01 + t * m1.m01;
		dest.m02 = (1.0F - t) * m0.m02 + t * m1.m02;
		dest.m03 = (1.0F - t) * m0.m03 + t * m1.m03;
		dest.m10 = (1.0F - t) * m0.m10 + t * m1.m10;
		dest.m11 = (1.0F - t) * m0.m11 + t * m1.m11;
		dest.m12 = (1.0F - t) * m0.m12 + t * m1.m12;
		dest.m13 = (1.0F - t) * m0.m13 + t * m1.m13;
		dest.m20 = (1.0F - t) * m0.m20 + t * m1.m20;
		dest.m21 = (1.0F - t) * m0.m21 + t * m1.m21;
		dest.m22 = (1.0F - t) * m0.m22 + t * m1.m22;
		dest.m23 = (1.0F - t) * m0.m23 + t * m1.m23;
		dest.m30 = (1.0F - t) * m0.m30 + t * m1.m30;
		dest.m31 = (1.0F - t) * m0.m31 + t * m1.m31;
		dest.m32 = (1.0F - t) * m0.m32 + t * m1.m32;
		dest.m33 = (1.0F - t) * m0.m33 + t * m1.m33;

		return dest;
	}

	/**
	 * 行列の線形補間を計算します。
	 * @param m0 補間対象の値0。
	 * @param m1 補間対象の値1。
	 * @param m2 補間対象の値2。
	 * @param m3 補間対象の値3。
	 * @param t0 補間の割合0。
	 * @param t1 補間の割合1。
	 * @param t2 補間の割合2。
	 * @param t3 補間の割合3。
	 * @param dest 補間された値が返される。
	 * @return destと同じインスタンス。
	 */
	public static Matrix4f lerp4(
			Matrix4f m0, Matrix4f m1, Matrix4f m2, Matrix4f m3,
			float t0, float t1, float t2, float t3, Matrix4f dest)
	{
		if (dest == null)
		{
			dest = new Matrix4f();
		}

		dest.m00 = t0 * m0.m00 + t1 * m1.m00 + t2 * m2.m00 + t3 * m3.m00;
		dest.m01 = t0 * m0.m01 + t1 * m1.m01 + t2 * m2.m01 + t3 * m3.m01;
		dest.m02 = t0 * m0.m02 + t1 * m1.m02 + t2 * m2.m02 + t3 * m3.m02;
		dest.m03 = t0 * m0.m03 + t1 * m1.m03 + t2 * m2.m03 + t3 * m3.m03;
		dest.m10 = t0 * m0.m10 + t1 * m1.m10 + t2 * m2.m10 + t3 * m3.m10;
		dest.m11 = t0 * m0.m11 + t1 * m1.m11 + t2 * m2.m11 + t3 * m3.m11;
		dest.m12 = t0 * m0.m12 + t1 * m1.m12 + t2 * m2.m12 + t3 * m3.m12;
		dest.m13 = t0 * m0.m13 + t1 * m1.m13 + t2 * m2.m13 + t3 * m3.m13;
		dest.m20 = t0 * m0.m20 + t1 * m1.m20 + t2 * m2.m20 + t3 * m3.m20;
		dest.m21 = t0 * m0.m21 + t1 * m1.m21 + t2 * m2.m21 + t3 * m3.m21;
		dest.m22 = t0 * m0.m22 + t1 * m1.m22 + t2 * m2.m22 + t3 * m3.m22;
		dest.m23 = t0 * m0.m23 + t1 * m1.m23 + t2 * m2.m23 + t3 * m3.m23;
		dest.m30 = t0 * m0.m30 + t1 * m1.m30 + t2 * m2.m30 + t3 * m3.m30;
		dest.m31 = t0 * m0.m31 + t1 * m1.m31 + t2 * m2.m31 + t3 * m3.m31;
		dest.m32 = t0 * m0.m32 + t1 * m1.m32 + t2 * m2.m32 + t3 * m3.m32;
		dest.m33 = t0 * m0.m33 + t1 * m1.m33 + t2 * m2.m33 + t3 * m3.m33;

		return dest;
	}

	/**
	 * クォータニオンの球面補間を計算します。
	 * @param q0 補間開始地点の値。
	 * @param q1 補間終了地点の値。
	 * @param t 開始地点からの割合。
	 * @param dest 補間された値が返される。
	 * @return destと同じインスタンス。
	 */
	public static Quaternion slerp(Quaternion q0, Quaternion q1, float t, Quaternion dest)
	{
		if (dest == null)
		{
			dest = new Quaternion();
		}

		float q0w = q0.w;
		float q0x = q0.x;
		float q0y = q0.y;
		float q0z = q0.z;
		float q1w = q1.w;
		float q1x = q1.x;
		float q1y = q1.y;
		float q1z = q1.z;

		if (q0w * q1w + q0x * q1x + q0y * q1y + q0z * q1z < 0)
		{
			q1w = -q1w;
			q1x = -q1x;
			q1y = -q1y;
			q1z = -q1z;
		}

		float cosHalfTheta = q0w * q1w + q0x * q1x + q0y * q1y + q0z * q1z;

		if ((float)Math.abs(cosHalfTheta) >= 1.0F)
		{
			dest.w = q0w;
			dest.x = q0x;
			dest.y = q0y;
			dest.z = q0z;
			return dest;
		}

		float halfTheta = (float)Math.acos(cosHalfTheta);
		float sinHalfTheta = (float)Math.sqrt(1.0F - cosHalfTheta * cosHalfTheta);

		if ((float)Math.abs(sinHalfTheta) < 0.001F)
		{
			dest.w = q0w * 0.5F + q1w * 0.5F;
			dest.x = q0x * 0.5F + q1x * 0.5F;
			dest.y = q0y * 0.5F + q1y * 0.5F;
			dest.z = q0z * 0.5F + q1z * 0.5F;
			return dest;
		}

		float ratioA = (float)Math.sin((1.0F - t) * halfTheta) / sinHalfTheta;
		float ratioB = (float)Math.sin(t * halfTheta) / sinHalfTheta;

		dest.w = q0w * ratioA + q1w * ratioB;
		dest.x = q0x * ratioA + q1x * ratioB;
		dest.y = q0y * ratioA + q1y * ratioB;
		dest.z = q0z * ratioA + q1z * ratioB;

		return dest;
	}
}
