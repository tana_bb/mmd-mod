package tana.modding.mmdmod.core;


import java.nio.charset.Charset;

/**
 * PMXのヘッダです。
 * @author tana
 */
public class PMXHeader
{
	public String pmxName = null;
	public float version = 0.0F;
	public Charset encoding = null;
	public int additionalUVCount = -1;
	public int vertexIndexSize = -1;
	public int textureIndexSize = -1;
	public int materialIndexSize = -1;
	public int boneIndexSize = -1;
	public int morphIndexSize = -1;
	public int rigidBodyIndexSize = -1;
}
