package tana.modding.mmdmod.core;


/**
 * PMXの面データです。
 * @author tana
 */
public class FaceData
{
	public int vertexIndex1 = -1;
	public int vertexIndex2 = -1;
	public int vertexIndex3 = -1;

	public int index = -1;
	public VertexData vertex1 = null;
	public VertexData vertex2 = null;
	public VertexData vertex3 = null;
	public EdgeData edge1 = null;
	public EdgeData edge2 = null;
	public EdgeData edge3 = null;
}
