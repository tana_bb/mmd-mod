package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

/**
 * VMDデータから構築されたボーンのフレームデータです。
 * @author tana
 */
public class BoneFrameData
{
	public Vector3f translate = new Vector3f();
	public Quaternion rotate = new Quaternion();
}
