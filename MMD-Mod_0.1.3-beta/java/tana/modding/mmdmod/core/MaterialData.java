package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * PMXの材質データです。
 * @author tana
 */
public class MaterialData
{
	public String name = null;
	public String nameEn = null;
	public Vector4f diffuse = new Vector4f();
	public Vector4f specular = new Vector4f();
	public Vector3f ambient = new Vector3f();
	public boolean bothFaces = false;
	public boolean groundShadow = false;
	public boolean drawSelfShadowMap = false;
	public boolean drawSelfShadow = false;
	public boolean drawEdge = false;
	public Vector4f edgeColor = new Vector4f();
	public float edgeSize = 0.0F;
	public int textureIndex = -1;
	public int sphereIndex = -1;
	public int sphereMode = -1;
	public boolean sharedToon = false;
	public int toonTextureIndex = -1;
	public String memo = null;
	public int faceCount = -1;

	public int index = -1;
	public Vector3f multMorphDiffuse = new Vector3f();
	public Vector4f multMorphSpecular = new Vector4f();
	public Vector3f multMorphAmbient = new Vector3f();
	public Vector3f multMorphEdgeColor = new Vector3f();
	public float multMorphEdgeSize = 0.0F;
	public Vector3f multMorphTexture = new Vector3f();
	public Vector3f multMorphSphere = new Vector3f();
	public Vector3f multMorphToon = new Vector3f();
	public Vector3f addMorphDiffuse = new Vector3f();
	public Vector4f addMorphSpecular = new Vector4f();
	public Vector3f addMorphAmbient = new Vector3f();
	public Vector3f addMorphEdgeColor = new Vector3f();
	public float addMorphEdgeSize = 0.0F;
	public Vector3f addMorphTexture = new Vector3f();
	public Vector3f addMorphSphere = new Vector3f();
	public Vector3f addMorphToon = new Vector3f();
}
