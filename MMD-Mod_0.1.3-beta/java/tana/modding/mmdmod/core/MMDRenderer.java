package tana.modding.mmdmod.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.IntBuffer;
import java.nio.charset.Charset;

import org.lwjgl.BufferUtils;
import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 * レンダリング機能を提供します。
 * @author tana
 */
public class MMDRenderer
{
	/**
	 * モーションのFPSです。
	 */
	public static final int fps = 30;

	/**
	 * シェーダプログラムです。
	 */
	private int program;

	/**
	 * PMXデータ群です。
	 * createModelIndex()により生成された添字に対応します。
	 */
	private PMXData[] pmx;
	/**
	 * VMDデータ群です。
	 * 2つ目の添字は0がIdling状態、1がActive状態です。
	 */
	private VMDData[][] vmd;
	/**
	 * 物理演算オブジェクト群です。
	 */
	private Physics[] physics;
	/**
	 * テクスチャ群です。
	 */
	private Texture[][] textures;
	/**
	 * モデルのスケールです。
	 */
	private float[] scale;
	/**
	 * モーションのスピードです。
	 */
	private float[] speed;
	/**
	 * 光源の座標です。
	 */
	private float[] lightX, lightY, lightZ;
	/**
	 * createModelIndex()により生成された有効な添字かどうかを示す値です。
	 */
	private boolean[] validIndex;
	/**
	 * 最大モデル数です。
	 */
	private int modelLimit = 10000;

	/**
	 * モーションの初期時間です。
	 * ここからの経過時間でモーションを制御します。
	 */
	private double[][] initTime;
	/**
	 * 一つ前のレンダリング時の時間です。
	 */
	private double[] preTime;

	/**
	 * コンストラクタです。
	 */
	public MMDRenderer()
	{
		this.init();
	}

	/**
	 * 初期化処理です。
	 */
	private void init()
	{
		this.pmx = new PMXData[this.modelLimit];
		this.vmd = new VMDData[this.modelLimit][];
		for (int i = 0; i < this.modelLimit; ++i)
		{
			this.vmd[i] = new VMDData[2];
		}
		this.physics = new Physics[this.modelLimit];
		this.textures = new Texture[this.modelLimit][];
		this.scale = new float[this.modelLimit];
		this.speed = new float[this.modelLimit];
		this.lightX = new float[this.modelLimit];
		this.lightY = new float[this.modelLimit];
		this.lightZ = new float[this.modelLimit];
		this.validIndex = new boolean[this.modelLimit];

		this.program = this.initShader();

		this.initTime = new double[this.modelLimit][];
		for (int i = 0; i < this.modelLimit; ++i)
		{
			this.initTime[i] = new double[2];
		}
		this.preTime = new double[this.modelLimit];
	}

	/**
	 * ファイナライザです。
	 */
	@Override
	protected void finalize() throws Throwable
	{
		this.destroy();

		super.finalize();
	}

	/**
	 * デストラクタです。
	 */
	public void destroy()
	{
		if (this.program != 0) GL20.glDeleteProgram(this.program);

		for (int i = 0; i < this.modelLimit; ++i)
		{
			this.releaseModelIndex(i);
		}
	}

	/**
	 * シェーダプログラムの初期化処理です。
	 * @return シェーダプログラムID。
	 */
	private int initShader()
	{
		int vShader, fShader;
		int program;

		vShader = this.readShader(GL20.GL_VERTEX_SHADER, "lib/vertexShader.glsl");
		if (vShader == 0) return 0;

		fShader = this.readShader(GL20.GL_FRAGMENT_SHADER, "lib/fragmentShader.glsl");
		if (fShader == 0) return 0;

		program = GL20.glCreateProgram();
		GL20.glAttachShader(program, vShader);
		GL20.glAttachShader(program, fShader);
		GL20.glDeleteShader(vShader);
		GL20.glDeleteShader(fShader);

		GL20.glLinkProgram(program);

		IntBuffer iBuf = BufferUtils.createIntBuffer(1);
		GL20.glGetProgram(program, GL20.GL_INFO_LOG_LENGTH, iBuf);

		iBuf.position(0);
		String info = GL20.glGetProgramInfoLog(program, iBuf.get());
		if (info != null) System.out.print(info);

		if (GL20.glGetProgrami(program, GL20.GL_LINK_STATUS) == GL11.GL_FALSE)
		{
			return 0;
		}

		return program;
	}

	/**
	 * シェーダを読み込みます。
	 * @param shaderType OpenGLのシェーダタイプの値。
	 * @param fileName シェーダのファイル名。
	 * @return シェーダのID。
	 */
	private int readShader(int shaderType, String fileName)
	{
		BufferedReader reader = null;
		StringBuilder program = new StringBuilder();

		try
		{
			InputStream in = this.getClass().getClassLoader().getResourceAsStream(fileName);
			reader = new BufferedReader(new InputStreamReader(in, Charset.forName("Shift_JIS")));
			String line;
			while ((line = reader.readLine()) != null)
			{
				program.append(line);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (reader != null) reader.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		int shader = GL20.glCreateShader(shaderType);
		GL20.glShaderSource(shader, program.toString());
		GL20.glCompileShader(shader);

		int infoLen = GL20.glGetShaderi(shader, GL20.GL_INFO_LOG_LENGTH);
		String info = GL20.glGetShaderInfoLog(shader, infoLen);
		if (info != null) System.out.print(info);

		if (GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) == GL11.GL_TRUE)
		{
			return shader;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * 新規モデルインデックスを作成し返します。
	 * @return 新規モデルインデックス。
	 */
	public int createModelIndex()
	{
		int i;
		for (i = 0; i < this.modelLimit; ++i)
		{
			if (!this.validIndex[i]) break;
		}

		if (i >= this.modelLimit)
		{
			return -1;
		}

		this.validIndex[i] = true;

		return i;
	}

	/**
	 * モデルインデックスを破棄します。
	 * @param modelIndex モデルインデックス。
	 */
	public void releaseModelIndex(int modelIndex)
	{
		if (modelIndex < 0)
		{
			return;
		}

		this.pmx[modelIndex] = null;

		if (vmd[modelIndex] != null)
		{
			for (int i = 0; i < this.vmd[modelIndex].length; ++i)
			{
				this.vmd[modelIndex][i] = null;
			}
		}

		if (this.physics[modelIndex] != null) this.physics[modelIndex].destroy();
		this.physics[modelIndex] = null;

		if (textures[modelIndex] != null)
		{
			for (int i = 0; i < this.textures[modelIndex].length; ++i)
			{
				if (this.textures[modelIndex][i] != null) this.textures[modelIndex][i].release();
				this.textures[modelIndex][i] = null;
			}
		}

		initTime[modelIndex][0] = 0.0F;
		initTime[modelIndex][1] = 0.0F;
		preTime[modelIndex] = 0.0F;

		this.validIndex[modelIndex] = false;
	}

	/**
	 * モデルデータを初期化します。
	 * @param modelIndex モデルインデックス。
	 * @param pmxPath PMXファイルのパス。
	 * @param vmd1Path Idling状態のVMDファイル。
	 * @param vmd2Path Active状態のVMDファイル。
	 * @param modelHeight モデルの身長。
	 * @param speed モーションのスピード。
	 */
	public void initModelData(
			int modelIndex, String pmxPath, String vmd1Path, String vmd2Path,
			float modelHeight, float speed)
	{
		if (modelIndex < 0)
		{
			return;
		}

		// PMX
		if (pmxPath != null && !pmxPath.isEmpty())
		{
			this.pmx[modelIndex] = new PMXData();
			try
			{
				this.pmx[modelIndex].readFile(new File(pmxPath));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		// VMD1
		if (vmd1Path != null && !vmd1Path.isEmpty())
		{
			this.vmd[modelIndex][0] = new VMDData();
			try
			{
				this.vmd[modelIndex][0].readFile(new File(vmd1Path));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		// VMD2
		if (vmd2Path != null && !vmd2Path.isEmpty())
		{
			this.vmd[modelIndex][1] = new VMDData();
			try
			{
				this.vmd[modelIndex][1].readFile(new File(vmd2Path));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		// その他の読み込み
		if (pmx[modelIndex] != null)
		{
			this.scale[modelIndex] = modelHeight / (this.pmx[modelIndex].max.y - this.pmx[modelIndex].min.y);
			this.speed[modelIndex] = speed;
			this.lightX[modelIndex] = this.pmx[modelIndex].max.y * 1.5F;
			this.lightY[modelIndex] = this.pmx[modelIndex].max.y * 1.5F;
			this.lightZ[modelIndex] = this.pmx[modelIndex].max.y * 1.5F;

			this.physics[modelIndex] = new Physics(
					this.pmx[modelIndex].boneData,
					this.pmx[modelIndex].rigidBodyData,
					this.pmx[modelIndex].jointData, this.scale[modelIndex]);

			// テクスチャの読み込み
			this.textures[modelIndex] =
					new Texture[this.pmx[modelIndex].textureCount + this.pmx[modelIndex].sharedTextureCount];

			for (int i = 0; i < this.pmx[modelIndex].textureCount + this.pmx[modelIndex].sharedTextureCount; ++i)
			{
				if (this.pmx[modelIndex].textureUsageFlag[i])
				{
					try
					{
						File file = new File(
								this.pmx[modelIndex].resourceDirPath, this.pmx[modelIndex].textureData[i].path);
						String absPath = file.getAbsolutePath();
						int index = absPath.lastIndexOf(".") + 1;

						if (index >= 0)
						{
							String suffix = absPath.substring(index);

							if (suffix.equalsIgnoreCase("dds"))
							{
								new IOException("Cannot open dds file.").printStackTrace();
								this.textures[modelIndex][i] = null;
							}
							else
							{
								this.textures[modelIndex][i] = TextureLoader.getTexture(suffix,
										ResourceLoader.getResourceAsStream(absPath));
							}
						}
						else
						{
							new IOException("Cannot open file.").printStackTrace();
							this.textures[modelIndex][i] = null;
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						this.textures[modelIndex][i] = null;
					}
				}
				else
				{
					this.textures[modelIndex][i] = null;
				}
			}

			// 時間の初期化
			double time = (double)Sys.getTime() / (double)Sys.getTimerResolution();
			this.initTime[modelIndex][0] = time;
			this.initTime[modelIndex][1] = time;
			this.preTime[modelIndex] = time;
		}
	}

	/**
	 * モーションの時間をリセットします。
	 * @param modelIndex モデルインデックス。
	 * @param vmdIndex IdlingまたはActiveを示す添字。
	 */
	public void resetTime(int modelIndex, int vmdIndex)
	{
		if (modelIndex < 0)
		{
			return;
		}

		this.initTime[modelIndex][vmdIndex] = (double)Sys.getTime() / (double)Sys.getTimerResolution();
	}

	/**
	 * OpenGLの関数によりテクスチャをバインドします。
	 * @param modelIndex モデルインデックス。
	 * @param textureIndex テクスチャインデックス。
	 */
	private void bindTextureGL(int modelIndex, int textureIndex)
	{
		if (modelIndex < 0)
		{
			return;
		}

		if (this.textures[modelIndex][textureIndex] != null)
		{
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textures[modelIndex][textureIndex].getTextureID());
		}
	}

	/**
	 * Slickのメソッドを使用してテクスチャをバインドします。
	 * @param modelIndex モデルインデックス。
	 * @param textureIndex テクスチャインデックス。
	 */
	private void bindTextureSlick(int modelIndex, int textureIndex)
	{
		if (modelIndex < 0)
		{
			return;
		}

		if (this.textures[modelIndex][textureIndex] != null)
		{
			this.textures[modelIndex][textureIndex].bind();
		}
	}

	/**
	 * レンダリング処理です。
	 * @param modelIndex モデルインデックス。
	 * @param x ワールド座標。
	 * @param y ワールド座標。
	 * @param z ワールド座標。
	 * @param side 向き。
	 * @param vmdRatio IdlingおよびAvtiveモーションの比率。
	 * @param enablePhysics 物理演算を有効化するかどうか。
	 */
	public void update(
			int modelIndex,
			float x, float y, float z,
			int side, float vmdRatio, boolean enablePhysics, float lightRatio)
	{
		if (modelIndex < 0)
		{
			return;
		}

		if (this.pmx[modelIndex] == null)
		{
			return;
		}

		double now = (double)Sys.getTime() / (double)Sys.getTimerResolution();
		int frameOffset1 = (int)((now - this.initTime[modelIndex][0]) * fps * this.speed[modelIndex]);
		int frameOffset2 = (int)((now - this.initTime[modelIndex][1]) * fps * this.speed[modelIndex]);
		float deltaTime = (float)(now - this.preTime[modelIndex]) * this.speed[modelIndex];
		this.preTime[modelIndex] = now;

		this.pmx[modelIndex].transform(
				this.vmd[modelIndex][0],
				frameOffset1,
				this.vmd[modelIndex][1],
				frameOffset2,
				vmdRatio,
				this.physics[modelIndex],
				enablePhysics, deltaTime);


		GL11.glEnable(GL11.GL_NORMALIZE);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPushMatrix();
		GL11.glTranslatef(x + 0.5F, y + 1.0F, z + 0.5F);
		float angle = 0.0F;
		switch (side)
		{
		case 2:
			break;
		case 1:
			angle = 90.0F;
			break;
		case 0:
			angle = 180.0F;
			break;
		case 3:
			angle = 270.0F;
			break;
		}
		GL11.glRotatef(angle, 0.0F, 1.0F, 0.0F);
		GL11.glScalef(this.scale[modelIndex], this.scale[modelIndex], this.scale[modelIndex]);

		GL20.glUseProgram(this.program);
		doProgram(modelIndex, lightRatio);
		GL20.glUseProgram(0);

		GL11.glPopMatrix();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_NORMALIZE);
	}

	/**
	 * レンダリングのシェーダプログラムに関する処理です。
	 * @param modelIndex モデルインデックス。
	 */
	private void doProgram(int modelIndex, float lightRatio)
	{
		if (modelIndex < 0)
		{
			return;
		}

		int faceIndex = 0;
		for (int i = 0; i < this.pmx[modelIndex].materialCount; ++i)
		{
			// 光源
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light0Pos"),
					this.lightX[modelIndex], this.lightY[modelIndex], -this.lightZ[modelIndex]);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light0Diffuse"),
					1.0F * lightRatio, 1.0F * lightRatio, 1.0F * lightRatio);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light0Specular"),
					1.0F * lightRatio, 1.0F * lightRatio, 1.0F * lightRatio);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light0Ambient"),
					0.05F * lightRatio, 0.05F * lightRatio, 0.05F * lightRatio);

			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light1Pos"),
					-this.lightX[modelIndex], this.lightY[modelIndex], -this.lightZ[modelIndex]);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light1Diffuse"),
					0.4F * lightRatio, 0.3F * lightRatio, 0.4F * lightRatio);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light1Specular"),
					0.4F * lightRatio, 0.3F * lightRatio, 0.4F * lightRatio);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light1Ambient"),
					0.05F * lightRatio, 0.05F * lightRatio, 0.05F * lightRatio);

			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light2Pos"),
					this.lightX[modelIndex], -this.lightY[modelIndex], this.lightZ[modelIndex]);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light2Diffuse"),
					0.5F * lightRatio, 1.0F * lightRatio, 1.2F * lightRatio);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light2Specular"),
					0.5F * lightRatio, 1.0F * lightRatio, 1.2F * lightRatio);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "light2Ambient"),
					0.0F * lightRatio, 0.05F * lightRatio, 0.1F * lightRatio);

			// 材質
			GL20.glUniform4f(GL20.glGetUniformLocation(this.program, "materialDiffuse"),
					this.pmx[modelIndex].materialData[i].diffuse.x
					* this.pmx[modelIndex].materialData[i].multMorphDiffuse.x
					+ this.pmx[modelIndex].materialData[i].addMorphDiffuse.x,
					this.pmx[modelIndex].materialData[i].diffuse.y
					* this.pmx[modelIndex].materialData[i].multMorphDiffuse.y
					+ this.pmx[modelIndex].materialData[i].addMorphDiffuse.y,
					this.pmx[modelIndex].materialData[i].diffuse.z
					* this.pmx[modelIndex].materialData[i].multMorphDiffuse.z
					+ this.pmx[modelIndex].materialData[i].addMorphDiffuse.z,
					this.pmx[modelIndex].materialData[i].diffuse.w);
			GL20.glUniform4f(GL20.glGetUniformLocation(this.program, "materialSpecular"),
					this.pmx[modelIndex].materialData[i].specular.x
					* this.pmx[modelIndex].materialData[i].multMorphSpecular.x
					+ this.pmx[modelIndex].materialData[i].addMorphSpecular.x,
					this.pmx[modelIndex].materialData[i].specular.y
					* this.pmx[modelIndex].materialData[i].multMorphSpecular.y
					+ this.pmx[modelIndex].materialData[i].addMorphSpecular.y,
					this.pmx[modelIndex].materialData[i].specular.z
					* this.pmx[modelIndex].materialData[i].multMorphSpecular.z
					+ this.pmx[modelIndex].materialData[i].addMorphSpecular.z,
					this.pmx[modelIndex].materialData[i].specular.w
					* this.pmx[modelIndex].materialData[i].multMorphSpecular.w
					+ this.pmx[modelIndex].materialData[i].addMorphSpecular.w);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "materialAmbient"),
					this.pmx[modelIndex].materialData[i].ambient.x
					* this.pmx[modelIndex].materialData[i].multMorphAmbient.x
					+ this.pmx[modelIndex].materialData[i].addMorphAmbient.x,
					this.pmx[modelIndex].materialData[i].ambient.y
					* this.pmx[modelIndex].materialData[i].multMorphAmbient.y
					+ this.pmx[modelIndex].materialData[i].addMorphAmbient.y,
					this.pmx[modelIndex].materialData[i].ambient.z
					* this.pmx[modelIndex].materialData[i].multMorphAmbient.z
					+ this.pmx[modelIndex].materialData[i].addMorphAmbient.z);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "multMorphTexture"),
					this.pmx[modelIndex].materialData[i].multMorphTexture.x,
					this.pmx[modelIndex].materialData[i].multMorphTexture.y,
					this.pmx[modelIndex].materialData[i].multMorphTexture.z);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "addMorphTexture"),
					this.pmx[modelIndex].materialData[i].addMorphTexture.x,
					this.pmx[modelIndex].materialData[i].addMorphTexture.y,
					this.pmx[modelIndex].materialData[i].addMorphTexture.z);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "multMorphSphere"),
					this.pmx[modelIndex].materialData[i].multMorphSphere.x,
					this.pmx[modelIndex].materialData[i].multMorphSphere.y,
					this.pmx[modelIndex].materialData[i].multMorphSphere.z);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "addMorphSphere"),
					this.pmx[modelIndex].materialData[i].addMorphSphere.x,
					this.pmx[modelIndex].materialData[i].addMorphSphere.y,
					this.pmx[modelIndex].materialData[i].addMorphSphere.z);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "multMorphToon"),
					this.pmx[modelIndex].materialData[i].multMorphToon.x,
					this.pmx[modelIndex].materialData[i].multMorphToon.y,
					this.pmx[modelIndex].materialData[i].multMorphToon.z);
			GL20.glUniform3f(GL20.glGetUniformLocation(this.program, "addMorphToon"),
					this.pmx[modelIndex].materialData[i].addMorphToon.x,
					this.pmx[modelIndex].materialData[i].addMorphToon.y,
					this.pmx[modelIndex].materialData[i].addMorphToon.z);

			// 両面描画
			if (this.pmx[modelIndex].materialData[i].bothFaces)
			{
				GL11.glDisable(GL11.GL_CULL_FACE);
				GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "cullBack"), 0);
			}
			else
			{
				GL11.glEnable(GL11.GL_CULL_FACE);
				GL11.glCullFace(GL11.GL_BACK);
				GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "cullBack"), 1);
			}

			// テクスチャ
			boolean tex0Usage = false;
			if (this.pmx[modelIndex].materialData[i].textureIndex >= 0)
			{
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				this.bindTextureGL(modelIndex, this.pmx[modelIndex].materialData[i].textureIndex);
				this.bindTextureSlick(modelIndex, this.pmx[modelIndex].materialData[i].textureIndex);
				tex0Usage = true;
			}
			GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "texture0"), 0);
			GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "texture0Usage"), tex0Usage ? 1 : 0);

			boolean tex1Usage = false;
			if (this.pmx[modelIndex].materialData[i].sphereIndex >= 0)
			{
				GL13.glActiveTexture(GL13.GL_TEXTURE1);
				this.bindTextureGL(modelIndex, this.pmx[modelIndex].materialData[i].sphereIndex);
				this.bindTextureSlick(modelIndex, this.pmx[modelIndex].materialData[i].sphereIndex);
				tex1Usage = true;
			}
			GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "texture1"), 1);
			GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "texture1Usage"),
					tex1Usage ? this.pmx[modelIndex].materialData[i].sphereMode : 0);

			boolean tex2Usage = false;
			if (this.program != 0
					&& this.pmx[modelIndex].materialData[i].toonTextureIndex >= 0
					&& this.textures[this.pmx[modelIndex].materialData[i].toonTextureIndex] != null)
			{
				GL13.glActiveTexture(GL13.GL_TEXTURE2);
				this.bindTextureGL(modelIndex, this.pmx[modelIndex].materialData[i].toonTextureIndex);
				this.bindTextureSlick(modelIndex, this.pmx[modelIndex].materialData[i].toonTextureIndex);
				tex2Usage = true;
			}
			GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "texture2"), 2);
			GL20.glUniform1i(GL20.glGetUniformLocation(this.program, "texture2Usage"), tex2Usage ? 1 : 0);

			// モデルの描画
			this.renderModel(modelIndex, faceIndex, i);

			// 後処理
			if (tex2Usage)
			{
				GL13.glActiveTexture(GL13.GL_TEXTURE2);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
			}

			if (tex1Usage)
			{
				GL13.glActiveTexture(GL13.GL_TEXTURE1);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
			}

			if (tex0Usage)
			{
				GL13.glActiveTexture(GL13.GL_TEXTURE0);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
			}

			faceIndex += this.pmx[modelIndex].materialData[i].faceCount / 3;
		}
	}

	/**
	 * モデルを描画します。材質単位で描画されます。
	 * @param modelIndex モデルインデックス。
	 * @param faceIndex 描画対象の面の添字。
	 * @param materialIndex 描画対象の材質の添字。
	 */
	private void renderModel(int modelIndex, int faceIndex, int materialIndex)
	{
		if (modelIndex < 0)
		{
			return;
		}

		int faceLimit = faceIndex + this.pmx[modelIndex].materialData[materialIndex].faceCount / 3;
		GL11.glBegin(GL11.GL_TRIANGLES);
		for (int i = faceIndex; i < faceLimit; ++i)
		{
			GL11.glTexCoord2f(
					this.pmx[modelIndex].faceData[i].vertex3.renderUV.x,
					this.pmx[modelIndex].faceData[i].vertex3.renderUV.y);
			GL11.glNormal3f(
					-this.pmx[modelIndex].faceData[i].vertex3.renderN.x,
					this.pmx[modelIndex].faceData[i].vertex3.renderN.y,
					this.pmx[modelIndex].faceData[i].vertex3.renderN.z);
			GL11.glVertex3f(
					-this.pmx[modelIndex].faceData[i].vertex3.renderP.x,
					this.pmx[modelIndex].faceData[i].vertex3.renderP.y,
					this.pmx[modelIndex].faceData[i].vertex3.renderP.z);

			GL11.glTexCoord2f(
					this.pmx[modelIndex].faceData[i].vertex2.renderUV.x,
					this.pmx[modelIndex].faceData[i].vertex2.renderUV.y);
			GL11.glNormal3f(
					-this.pmx[modelIndex].faceData[i].vertex2.renderN.x,
					this.pmx[modelIndex].faceData[i].vertex2.renderN.y,
					this.pmx[modelIndex].faceData[i].vertex2.renderN.z);
			GL11.glVertex3f(
					-this.pmx[modelIndex].faceData[i].vertex2.renderP.x,
					this.pmx[modelIndex].faceData[i].vertex2.renderP.y,
					this.pmx[modelIndex].faceData[i].vertex2.renderP.z);

			GL11.glTexCoord2f(
					this.pmx[modelIndex].faceData[i].vertex1.renderUV.x,
					this.pmx[modelIndex].faceData[i].vertex1.renderUV.y);
			GL11.glNormal3f(
					-this.pmx[modelIndex].faceData[i].vertex1.renderN.x,
					this.pmx[modelIndex].faceData[i].vertex1.renderN.y,
					this.pmx[modelIndex].faceData[i].vertex1.renderN.z);
			GL11.glVertex3f(
					-this.pmx[modelIndex].faceData[i].vertex1.renderP.x,
					this.pmx[modelIndex].faceData[i].vertex1.renderP.y,
					this.pmx[modelIndex].faceData[i].vertex1.renderP.z);
		}
		GL11.glEnd();
	}
}
