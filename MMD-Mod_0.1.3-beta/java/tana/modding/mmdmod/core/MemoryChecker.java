package tana.modding.mmdmod.core;

import java.io.File;


/**
 * メモリ容量に関する機能を提供します。
 * @author tana
 */
public class MemoryChecker
{
	public static boolean checkMemorySizeWithGC(RenderedModelInfo info)
	{
		System.gc();

		if (info == null)
		{
			return true;
		}

		long freeSize = Runtime.getRuntime().freeMemory();

		long pmxSize = (long)(new File(info.pmxPath).length() * 3.0);
		long vmd1Size = (long)(new File(info.vmd1Path).length() * 14.0);
		long vmd2Size = (long)(new File(info.vmd2Path).length() * 14.0);
		long audioSize = (long)(new File(info.audioPath).length() * 1.0);

		return freeSize > pmxSize + vmd1Size + vmd2Size + audioSize;
	}
}
