package tana.modding.mmdmod.core;


/**
 * PMXのテクスチャデータです。
 * @author tana
 */
public class TextureData
{
	public String path = null;

	public int index = -1;
}
