package tana.modding.mmdmod.core;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;

/**
 * 音声ファイルを再生します。
 * @author tana
 */
public class AudioPlayer
{
	/**
	 * 音声ファイルのデータが格納されるバッファです。
	 */
	private byte[] rawData = null;
	/**
	 * ファイルがロードされているかどうかを示すフラグです。
	 */
	private boolean isLoaded = false;
	/**
	 * 再生中かどうかを示すフラグです。
	 */
	private volatile boolean isPlaying = false;
	/**
	 * 再生スレッドの停止を制御するフラグです。
	 */
	private volatile boolean stopPending = false;
	/**
	 * 音声データのフォーマットです。
	 */
	private AudioFormat format = null;

	/**
	 * コンストラクタです。
	 */
	public AudioPlayer()
	{
	}

	public void load(String fileName, float volume)
	{
		if (fileName != null && !fileName.isEmpty())
		{
			this.load(new File(fileName), volume);
		}
	}

	/**
	 * 音声ファイルをロードします。
	 * @param file 音声ファイル（WAVE形式）。
	 * @param volume 音量。
	 */
	public void load(File file, float volume)
	{
		String absPath = file.getAbsolutePath();
		int index = absPath.lastIndexOf(".") + 1;

		if (index < 0)
		{
			new IOException("Cannot open audio file.").printStackTrace();
			return;
		}

		String suffix = absPath.substring(index);

		if (!suffix.equalsIgnoreCase("wav"))
		{
			new IOException("Cannot open audio file.").printStackTrace();
			return;
		}

		AudioInputStream in = null;

		try
		{
			in = AudioSystem.getAudioInputStream(file);

			this.rawData = new byte[in.available()];

			int pos = 0;
			int bytes;
			while ((bytes = in.read(this.rawData, pos, rawData.length - pos)) > 0)
			{
				pos += bytes;
			}

			this.format = in.getFormat();
			this.isLoaded = true;

			// 音量調整
			for (int i = 0; i < rawData.length; ++i)
			{
				rawData[i] *= volume;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (in != null) in.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * 音声データを再生します。
	 */
	public void start()
	{
		if (!this.isLoaded)
		{
			return;
		}

		Thread thread = new Thread() {
			public void run()
			{
				SourceDataLine line = null;
				try
				{
					int frameSize = format.getFrameSize();
					DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
					line = (SourceDataLine)AudioSystem.getLine(info);
					line.open(format);
					line.start();
					int i;
					for (i = 0; i < rawData.length; i += frameSize * 10000)
					{
						if (stopPending) break;
						line.write(rawData, i, frameSize * 10000);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					if (line != null)
					{
						if (line.isRunning())
						{
							line.drain();
							line.stop();
						}
						line.close();
					}
					isPlaying = false;
					stopPending = false;
				}
			}
		};

		this.isPlaying = true;
		thread.start();
	}

	/**
	 * 音声データの再生を停止します。
	 */
	public void stop()
	{
		if (this.isLoaded && this.isPlaying)
		{
			this.stopPending = true;
		}
	}
}
