package tana.modding.mmdmod.core;

/**
 * MMDRendererに渡されるモデル情報です。
 * @author tana
 */
public class RenderedModelInfo
{
	public boolean enable = false;
	public int modelIndex = -1;
	public String pmxPath = null;
	public String vmd1Path = null;
	public String vmd2Path = null;
	public String audioPath = null;
	public int side = -1;
	public float modelHeight = 0.0F;
	public float speed = 0.0F;
	public float volume = 0.0F;
	public boolean enablePhysics = false;
	public float vmdRatio = 0.0F;
	public boolean enableLight = false;
	public float lightRatio = 0.0F;
	public boolean resetTime1 = false;
	public boolean resetTime2 = false;

	public RenderedModelInfo()
	{
	}

	public RenderedModelInfo(RenderedModelInfo info)
	{
		if (info != null)
		{
			this.enable = info.enable;
			this.modelIndex = info.modelIndex;
			this.pmxPath = info.pmxPath;
			this.vmd1Path = info.vmd1Path;
			this.vmd2Path = info.vmd2Path;
			this.audioPath = info.audioPath;
			this.side = info.side;
			this.modelHeight = info.modelHeight;
			this.speed = info.speed;
			this.volume = info.volume;
			this.enablePhysics = info.enablePhysics;
			this.vmdRatio = info.vmdRatio;
			this.enableLight = info.enableLight;
			this.lightRatio = info.lightRatio;
			this.resetTime1 = info.resetTime1;
			this.resetTime2 = info.resetTime2;
		}
	}
}
