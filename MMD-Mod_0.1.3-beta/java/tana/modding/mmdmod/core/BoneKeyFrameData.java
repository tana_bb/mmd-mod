package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

/**
 * VMDのボーンキーフレームデータです。
 * @author tana
 */
public class BoneKeyFrameData
{
	public String name = null;
	public int frame = -1;
	public Vector3f translate = new Vector3f();
	public Quaternion rotate = new Quaternion();
	public BezierInterpolation ipX = null;
	public BezierInterpolation ipY = null;
	public BezierInterpolation ipZ = null;
	public BezierInterpolation ipR = null;
}
