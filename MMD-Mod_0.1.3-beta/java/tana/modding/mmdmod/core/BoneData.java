package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

/**
 * PMXのボーンデータです。
 * @author tana
 */
public class BoneData
{
	public String name = null;
	public String nameEn = null;
	public Vector3f p = new Vector3f();
	public int parentIndex = -1;
	public int hierarchy = -1;
	public boolean accessPointFlag = false;
	public boolean canRotate = false;
	public boolean canTranslate = false;
	public boolean visible = false;
	public boolean canControl = false;
	public boolean isIK = false;
	public boolean localAddition = false;
	public boolean rotateAddition = false;
	public boolean translateAddition = false;
	public boolean fixAxis = false;
	public boolean localAxis = false;
	public boolean postTransform = false;
	public boolean externalTransform = false;
	public Vector3f accessPointP = new Vector3f();
	public int accessPointIndex = -1;
	public int additionParentIndex = -1;
	public float additionRatio = 0.0F;
	public Vector3f pivot = new Vector3f();
	public Vector3f localXAxis = new Vector3f();
	public Vector3f localZAxis = new Vector3f();
	public int externalKey = -1;
	public int ikTargetIndex = -1;
	public int ikLoopCount = -1;
	public float ikLoopConstRadian = 0.0F;
	public int ikLinkCount = -1;
	public IKLink[] ikLink;

	public int index = -1;
	public BoneData[] children = null;
	public boolean isPhysicsBone = false;
	public Vector3f translate = new Vector3f();
	public Quaternion rotate = new Quaternion();
	public Vector3f morphTranslate = new Vector3f();
	public Quaternion morphRotate = new Quaternion();
	public Vector3f addTranslate = new Vector3f();
	public Quaternion addRotate = new Quaternion();
	public Quaternion ikRotate = new Quaternion();
	public Matrix4f localMatrix = new Matrix4f();
	public Vector3f localP = new Vector3f();

	public class IKLink
	{
		public int linkIndex = -1;
		public boolean constRadian = false;
		public Vector3f min = new Vector3f();
		public Vector3f max = new Vector3f();
	}
}
