package tana.modding.mmdmod.core;


import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * PMXの頂点データです。
 * @author tana
 */
public class VertexData
{
	public Vector3f p = new Vector3f();
	public Vector3f n = new Vector3f();
	public Vector2f uv = new Vector2f();
	public Vector4f addUV1 = null;
	public Vector4f addUV2 = null;
	public Vector4f addUV3 = null;
	public Vector4f addUV4 = null;
	public int boneBdefMethod = -1;
	public int boneIndex1 = -1;
	public int boneIndex2 = -1;
	public int boneIndex3 = -1;
	public int boneIndex4 = -1;
	public float boneWeight1 = 0.0F;
	public float boneWeight2 = 0.0F;
	public float boneWeight3 = 0.0F;
	public float boneWeight4 = 0.0F;
	public Vector3f sdefC = null;
	public Vector3f sdefR0 = null;
	public Vector3f sdefR1 = null;
	public float edgeScale = 0.0F;

	public int index = -1;
	public List<FaceData> faces = null;
	public List<EdgeData> edges = null;
	public Vector3f morphP = new Vector3f();
	public Vector2f morphUV = new Vector2f();
	public Vector4f morphAddUV1 = null;
	public Vector4f morphAddUV2 = null;
	public Vector4f morphAddUV3 = null;
	public Vector4f morphAddUV4 = null;

	public Vector3f renderP = new Vector3f();
	public Vector3f renderN = new Vector3f();
	public Vector2f renderUV = new Vector2f();
	public Vector4f renderAddUV1 = null;
	public Vector4f renderAddUV2 = null;
	public Vector4f renderAddUV3 = null;
	public Vector4f renderAddUV4 = null;
}
