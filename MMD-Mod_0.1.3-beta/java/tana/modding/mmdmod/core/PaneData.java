package tana.modding.mmdmod.core;


/**
 * PMXの表示枠データです。
 * @author tana
 */
public class PaneData
{
	public String name = null;
	public String nameEn = null;
	public boolean isSpecialPane = false;
	public int elementCount = -1;
	public PaneElement[] elements = null;
	
	public int index = -1;

	public class PaneElement
	{
		public int targetFlag = -1;
		public int elementIndex = -1;
	}
}
