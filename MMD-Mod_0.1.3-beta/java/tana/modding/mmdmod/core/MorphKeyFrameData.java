package tana.modding.mmdmod.core;


/**
 * VMDのモーフキーフレームデータです。
 * @author tana
 */
public class MorphKeyFrameData
{
	public String name = null;
	public int frame = -1;
	public float value = 0.0F;
}
