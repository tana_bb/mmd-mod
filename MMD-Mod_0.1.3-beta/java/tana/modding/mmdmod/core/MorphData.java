package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;


/**
 * PMXのモーフデータです。
 * @author tana
 */
public class MorphData
{
	public String name = null;
	public String nameEn = null;
	public int panelPosition = -1;
	public int morphType = -1;
	public int offsetCount = -1;
	public MorphOffset[] offsets = null;

	public int index = -1;
	public float ratio = 0.0F;
	public float groupMorphRatio = 0.0F;

	public abstract class MorphOffset
	{
		public int targetIndex = -1;
	}

	public class VertexOffset extends MorphOffset
	{
		public Vector3f p = new Vector3f();
	}

	public class UVOffset extends MorphOffset
	{
		public Vector4f uv = new Vector4f();
	}

	public class BoneOffset extends MorphOffset
	{
		public Vector3f translate = new Vector3f();
		public Quaternion rotate = new Quaternion();
	}

	public class MaterialOffset extends MorphOffset
	{
		public int operator = -1;
		public Vector4f diffuse = new Vector4f();
		public Vector4f specular = new Vector4f();
		public Vector3f ambient = new Vector3f();
		public Vector4f edgeColor = new Vector4f();
		public float edgeSize = 0.0F;
		public Vector4f texture = new Vector4f();
		public Vector4f sphere = new Vector4f();
		public Vector4f toon = new Vector4f();
	}

	public class GroupMorph extends MorphOffset
	{
		public float ratio = 0.0F;
	}

	public class FlipMorph extends MorphOffset
	{
		public float ratio = 0.0F;
	}

	public class ImpulseMorph extends MorphOffset
	{
		public int localFlag = -1;
		public Vector3f velocity = new Vector3f();
		public Vector3f torque = new Vector3f();

	}
}
