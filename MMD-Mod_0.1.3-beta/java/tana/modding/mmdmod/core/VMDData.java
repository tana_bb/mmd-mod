package tana.modding.mmdmod.core;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.util.vector.Vector2f;

/**
 * VMDファイルのデータを格納します。
 * @author tana
 */
public class VMDData
{
	/**
	 * VMDヘッダです。
	 */
	public VMDHeader vmdHeader = null;

	/**
	 * ボーンのキーフレームデータです。
	 */
	public BoneKeyFrameData[] rawBoneKeys = null;
	/**
	 * ボーンの名称ごとに分類されたボーンキーフレームデータです。
	 * リストには、フレーム順にデータが格納されます。
	 */
	public Map<String, List<BoneKeyFrameData> > boneKeys = null;

	/**
	 * モーフのキーフレームデータです。
	 */
	public MorphKeyFrameData[] rawMorphKeys = null;
	/**
	 * モーフの名称ごとに分類されたモーフキーフレームデータです。
	 * リストには、フレーム順にデータが格納されます。
	 */
	public Map<String, List<MorphKeyFrameData> > morphKeys = null;

	/**
	 * 最大フレーム数です。
	 */
	public int maxFrame = -1;

	/**
	 * リソースが格納されるディレクトリのパスです。
	 */
	public String resourceDirPath = null;

	/**
	 * ファイルの文字列データに使用されるエンコーディングです。
	 */
	private final Charset encoding = Charset.forName("Shift_JIS");
	/**
	 * 不正なファイル形式を警告するメッセージです。
	 */
	private final String invalidPMXDataMessage = "Invalid VMD data.";

	/**
	 * コンストラクタです。
	 */
	public VMDData()
	{
	}

	/**
	 * VMDファイルを読み込みます。
	 * @param resourceDirPath リソースが格納されるディレクトリのパス。
	 * @param fileName VMDファイルのファイル名。
	 * @throws IOException
	 */
	public void readFile(String resourceDirPath, String fileName) throws IOException
	{
		this.readFile(new File(resourceDirPath, fileName));
	}

	/**
	 * PMXファイルを読み込みます。
	 * @param file VMDファイルへのパス。
	 * @throws IOException
	 */
	public void readFile(File file) throws IOException
	{
		this.resourceDirPath = file.getParent();

		LittleEndianInputStream in = null;
		try
		{
			in = new LittleEndianInputStream(new FileInputStream(file));

			// ファイル読み取り
			readVMDHeader(in);
			readBoneKeyFrameData(in);
			readMorphKeyFrameData(in);
		}
		finally
		{
			if (in != null) in.close();
		}
	}

	/**
	 * VMDヘッダを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readVMDHeader(LittleEndianInputStream in) throws IOException
	{
		String[] strBuf = new String[1];

		this.vmdHeader = new VMDHeader();

		readString(in, strBuf, 30, this.encoding);
		this.vmdHeader.vmdName = strBuf[0];

		readString(in, strBuf, 20, this.encoding);
		this.vmdHeader.modelName = strBuf[0];
	}

	/**
	 * ボーンのキーフレームデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readBoneKeyFrameData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[64];
		String[] strBuf = new String[1];
		float[] fBuf = new float[4];

		int dataCount;

		readInt(in, iBuf, 0, 1);
		dataCount = iBuf[0];

		this.rawBoneKeys = new BoneKeyFrameData[dataCount];
		this.boneKeys = new HashMap<String, List<BoneKeyFrameData> >();

		for (int i = 0; i < dataCount; ++i)
		{
			this.rawBoneKeys[i] = new BoneKeyFrameData();

			readString(in, strBuf, 15, this.encoding);
			this.rawBoneKeys[i].name = strBuf[0];

			readInt(in, iBuf, 0, 1);
			this.rawBoneKeys[i].frame = iBuf[0];

			readFloat(in, fBuf, 0, 3);
			this.rawBoneKeys[i].translate.x = fBuf[0];
			this.rawBoneKeys[i].translate.y = fBuf[1];
			this.rawBoneKeys[i].translate.z = fBuf[2];

			readFloat(in, fBuf, 0, 4);
			this.rawBoneKeys[i].rotate.x = fBuf[0];
			this.rawBoneKeys[i].rotate.y = fBuf[1];
			this.rawBoneKeys[i].rotate.z = fBuf[2];
			this.rawBoneKeys[i].rotate.w = fBuf[3];

			readUByte(in, iBuf, 0, 64);
			this.rawBoneKeys[i].ipX =
					new BezierInterpolation(new Vector2f(iBuf[0], iBuf[4]), new Vector2f(iBuf[8], iBuf[12]));
			this.rawBoneKeys[i].ipY =
					new BezierInterpolation(new Vector2f(iBuf[1], iBuf[5]), new Vector2f(iBuf[9], iBuf[13]));
			this.rawBoneKeys[i].ipZ =
					new BezierInterpolation(new Vector2f(iBuf[2], iBuf[6]), new Vector2f(iBuf[10], iBuf[14]));
			this.rawBoneKeys[i].ipR =
					new BezierInterpolation(new Vector2f(iBuf[3], iBuf[7]), new Vector2f(iBuf[11], iBuf[15]));

			// 最大フレーム数を記録
			if (this.maxFrame < this.rawBoneKeys[i].frame)
			{
				this.maxFrame = this.rawBoneKeys[i].frame;
			}

			// ボーン名称ごとに分類
			if (!this.boneKeys.containsKey(this.rawBoneKeys[i].name))
			{
				this.boneKeys.put(this.rawBoneKeys[i].name, new ArrayList<BoneKeyFrameData>());
			}
			this.boneKeys.get(this.rawBoneKeys[i].name).add(this.rawBoneKeys[i]);
		}

		// 分類データをソート
		for (List<BoneKeyFrameData> list : this.boneKeys.values())
		{
			Collections.sort(list, new Comparator<BoneKeyFrameData>() {
				public int compare(BoneKeyFrameData key1, BoneKeyFrameData key2)
				{
					if (key1.frame < key2.frame)
					{
						return -1;
					}
					else if (key1.frame > key2.frame)
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}
			});
		}
	}

	/**
	 * モーフのキーフレームデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @throws IOException
	 */
	protected void readMorphKeyFrameData(LittleEndianInputStream in) throws IOException
	{
		int[] iBuf = new int[1];
		String[] strBuf = new String[1];
		float[] fBuf = new float[1];

		int dataCount;

		readInt(in, iBuf, 0, 1);
		dataCount = iBuf[0];

		this.rawMorphKeys = new MorphKeyFrameData[dataCount];
		this.morphKeys = new HashMap<String, List<MorphKeyFrameData> >();

		for (int i = 0; i < dataCount; ++i)
		{
			this.rawMorphKeys[i] = new MorphKeyFrameData();

			readString(in, strBuf, 15, this.encoding);
			this.rawMorphKeys[i].name = strBuf[0];

			readInt(in, iBuf, 0, 1);
			this.rawMorphKeys[i].frame = iBuf[0];

			readFloat(in, fBuf, 0, 1);
			this.rawMorphKeys[i].value = fBuf[0];

			// 最大フレーム数を記録
			if (this.maxFrame < this.rawMorphKeys[i].frame)
			{
				this.maxFrame = this.rawMorphKeys[i].frame;
			}

			// モーフ名称ごとに分類
			if (!this.morphKeys.containsKey(this.rawMorphKeys[i].name))
			{
				this.morphKeys.put(this.rawMorphKeys[i].name, new ArrayList<MorphKeyFrameData>());
			}
			this.morphKeys.get(this.rawMorphKeys[i].name).add(this.rawMorphKeys[i]);
		}

		// 分類データをソート
		for (List<MorphKeyFrameData> list : this.morphKeys.values())
		{
			Collections.sort(list, new Comparator<MorphKeyFrameData>() {
				public int compare(MorphKeyFrameData key1, MorphKeyFrameData key2)
				{
					if (key1.frame < key2.frame)
					{
						return -1;
					}
					else if (key1.frame > key2.frame)
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}
			});
		}
	}

	/**
	 * 指定したフレームにおけるボーンの値を取得します。
	 * @param boneName 対象ボーンの名前。
	 * @param frame フレーム番号。
	 * @param out 計算結果が返される。
	 */
	public void getBoneFrame(String boneName, int frame, BoneFrameData out)
	{
		List<BoneKeyFrameData> keys = this.boneKeys.get(boneName);

		if (keys == null)
		{
			out.translate.x = 0.0F;
			out.translate.y = 0.0F;
			out.translate.z = 0.0F;
			out.rotate.w = 1.0F;
			out.rotate.x = 0.0F;
			out.rotate.y = 0.0F;
			out.rotate.z = 0.0F;
			return;
		}

		BoneKeyFrameData startKey = null;  // 始点のキー
		BoneKeyFrameData endKey = null;  // 終点のキー

		// 始点と終点を探す
		for (BoneKeyFrameData key : keys)
		{
			if (key.frame >= frame)
			{
				endKey = key;
				break;
			}
			startKey = key;
		}

		// フレームが最初のキーより左
		if (startKey == null)
		{
			out.translate.x = endKey.translate.x;
			out.translate.y = endKey.translate.y;
			out.translate.z = endKey.translate.z;
			out.rotate.w = endKey.rotate.w;
			out.rotate.x = -endKey.rotate.x;
			out.rotate.y = -endKey.rotate.y;
			out.rotate.z = -endKey.rotate.z;
			return;
		}

		// フレームが最後のキーより右
		if (endKey == null)
		{
			out.translate.x = startKey.translate.x;
			out.translate.y = startKey.translate.y;
			out.translate.z = startKey.translate.z;
			out.rotate.w = startKey.rotate.w;
			out.rotate.x = -startKey.rotate.x;
			out.rotate.y = -startKey.rotate.y;
			out.rotate.z = -startKey.rotate.z;
			return;
		}

		// 終点の補間オブジェクトで値を計算
		out.translate.x =
				endKey.ipX.getValue(frame, startKey.frame, endKey.frame, startKey.translate.x, endKey.translate.x);
		out.translate.y =
				endKey.ipY.getValue(frame, startKey.frame, endKey.frame, startKey.translate.y, endKey.translate.y);
		out.translate.z =
				endKey.ipZ.getValue(frame, startKey.frame, endKey.frame, startKey.translate.z, endKey.translate.z);

		endKey.ipR.getValue(frame, startKey.frame, endKey.frame, startKey.rotate, endKey.rotate, out.rotate);

		out.rotate.x = -out.rotate.x;
		out.rotate.y = -out.rotate.y;
		out.rotate.z = -out.rotate.z;
	}

	/**
	 * 指定したフレームにおけるモーフの値を取得します。
	 * @param morphName 対象モーフの名前。
	 * @param frame フレーム番号。
	 * @param out 計算結果が返される。
	 */
	public void getMorphFrame(String morphName, int frame, MorphFrameData out)
	{
		List<MorphKeyFrameData> keys = this.morphKeys.get(morphName);

		if (keys == null)
		{
			out.value = 0.0F;
			return;
		}

		MorphKeyFrameData startKey = null;  // 始点のキー
		MorphKeyFrameData endKey = null;  // 終点のキー

		// 始点と終点を探す
		for (MorphKeyFrameData key : keys)
		{
			if (key.frame >= frame)
			{
				endKey = key;
				break;
			}
			startKey = key;
		}

		// フレームが最初のキーより左
		if (startKey == null)
		{
			out.value = endKey.value;
			return;
		}

		// フレームが最後のキーより右
		if (endKey == null)
		{
			out.value = startKey.value;
			return;
		}

		// 値を計算
		out.value =
				(endKey.value - startKey.value) * (frame - startKey.frame)
				/ (endKey.frame - startKey.frame) + startKey.value;
	}

	/**
	 * byteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void read(LittleEndianInputStream in, byte[] b, int off, int len) throws IOException
	{
		if (in.read(b, off, len) != len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号なしbyteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readUByte(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readUByte(b, off, len) != Byte.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号ありbyteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readByte(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readByte(b, off, len) != Byte.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 符号なしshortデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readUShort(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readUShort(b, off, len) != Short.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * byteデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readShort(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readShort(b, off, len) != Short.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * intデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readInt(LittleEndianInputStream in, int[] b, int off, int len) throws IOException
	{
		if (in.readInt(b, off, len) != Integer.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * floatデータを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param b データが格納される配列。
	 * @param off 配列のオフセット。
	 * @param len 読み取るデータの個数。
	 * @throws IOException
	 */
	protected void readFloat(LittleEndianInputStream in, float[] b, int off, int len) throws IOException
	{
		if (in.readFloat(b, off, len) != Float.SIZE / 8 * len)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}
	}

	/**
	 * 文字列データを読み込みます。
	 * @param in 読み取り対象のストリーム。
	 * @param oneString データが格納される配列（oneString[0]のみ使用）。
	 * @param byteSize 読み取るデータのバイトサイズ。
	 * @param encoding テキストのエンコード方式。
	 * @throws IOException
	 */
	protected void readString(LittleEndianInputStream in, String[] oneString, int byteSize, Charset encoding)
			throws IOException
	{
		byte[] buf = new byte[byteSize];

		if (in.read(buf, 0, byteSize) != byteSize)
		{
			throw new IOException(this.invalidPMXDataMessage);
		}

		 String str = new String(buf, 0, byteSize, encoding);
		 int index = str.indexOf("\0");
		 oneString[0] = index >= 0 ? str.substring(0, index) : str;
	}
}
