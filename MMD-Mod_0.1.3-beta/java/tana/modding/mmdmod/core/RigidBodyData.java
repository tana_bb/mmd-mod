package tana.modding.mmdmod.core;


import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 * PMXの剛体データです。
 * @author tana
 */
public class RigidBodyData
{
	public String name = null;
	public String nameEn = null;
	public int boneIndex = -1;
	public int group = -1;
	public int uncollisionGroupFlag = 0;
	public int shape = -1;
	public Vector3f size = new Vector3f();
	public Vector3f p = new Vector3f();
	public Vector3f r = new Vector3f();
	public float mass = 0.0F;
	public float linearDamping = 0.0F;
	public float angularDamping = 0.0F;
	public float restitution = 0.0F;
	public float friction = 0.0F;
	public int operationFlag = -1;

	public int index = -1;
	public Matrix4f initMatrix = new Matrix4f();
	public Matrix4f localMatrix = new Matrix4f();
}
