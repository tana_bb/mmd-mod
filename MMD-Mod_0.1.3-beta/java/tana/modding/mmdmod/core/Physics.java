package tana.modding.mmdmod.core;


import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.PointerByReference;

/**
 * 物理演算を提供します。
 * @author tana
 */
public class Physics
{
	/**
	 * 処理対象のボーンデータです。
	 */
	public BoneData[] bones;
	/**
	 * 処理対象の剛体データです。
	 */
	public RigidBodyData[] rigidBodies;
	/**
	 * 処理対象のJointデータです。
	 */
	public JointData[] joints;
	/**
	 * モデルのスケールです。
	 */
	public float scale;

	/**
	 * DLLが格納される一時ディレクトリのパスです。
	 */
	private static String tmpDLLPath;

	/**
	 * 物理演算オブジェクトのインデックスです。
	 */
	private int physicsIndex;
	/**
	 * DLLと共有する剛体情報です。
	 */
	private Pointer rigidBodyInfo;
	/**
	 * DLLと共有する座標変換情報です。
	 */
	private Pointer transformInfo;
	/**
	 * DLLと共有するJoint情報です。
	 */
	private Pointer jointInfo;

	/**
	 * 計算用のベクトルバッファです。
	 */
	private Vector3f vBuf0 = new Vector3f();
	/**
	 * 計算用の行列バッファです。
	 */
	private Matrix4f mBuf0 = new Matrix4f(), mBuf1 = new Matrix4f();

	/**
	 * スタティックコンストラクタです。
	 */
	static
	{
		tmpDLLPath =
				Platform.isWindows() ? (
					Platform.is64Bit() ? TmpFileManager.copyTmpFile("lib/win64/Physics.dll")
					: TmpFileManager.copyTmpFile("lib/win32/Physics.dll")
				) : null;

		if (tmpDLLPath == null)
		{
			throw new IllegalStateException("Windows only.");
		}

		PhysicsLibrary.instance.init();
	}

	/**
	 * コンストラクタです。
	 * @param bones 処理対象のボーンデータ。
	 * @param rigidBodies 処理対象の剛体データ。
	 * @param joints 処理対象のJointデータ。
	 * @param scale モデルのスケール。
	 */
	public Physics(BoneData[] bones, RigidBodyData[] rigidBodies, JointData[] joints, float scale)
	{
		this.bones = bones;
		this.rigidBodies = rigidBodies;
		this.joints = joints;
		this.scale = scale;

		init();
	}

	/**
	 * 初期化処理です。
	 */
	public void init()
	{
		// オブジェクト作成
		this.physicsIndex = PhysicsLibrary.instance.getObject();

		// メモリの確保
		PointerByReference rInfo = new PointerByReference();
		PointerByReference tInfo = new PointerByReference();
		PointerByReference jInfo = new PointerByReference();

		PhysicsLibrary.instance.initObject(
				this.physicsIndex, rInfo, tInfo, this.rigidBodies.length, jInfo, this.joints.length);

		this.rigidBodyInfo = rInfo.getValue();
		this.transformInfo = tInfo.getValue();
		this.jointInfo = jInfo.getValue();

		if (this.rigidBodies.length > 0)
		{
			// 剛体のパラメータを初期化
			RigidBodyInfo rInfoP = new RigidBodyInfo(this.rigidBodyInfo);
			Structure[] rInfoArray = rInfoP.toArray(this.rigidBodies.length);

			for (int i = 0; i < this.rigidBodies.length; ++i)
			{
				RigidBodyInfo info = new RigidBodyInfo(rInfoArray[i].getPointer());

				info.group = this.rigidBodies[i].group;
				info.uncollisionGroupFlag = this.rigidBodies[i].uncollisionGroupFlag;
				info.shape = this.rigidBodies[i].shape;
				info.sizeX = this.rigidBodies[i].size.x;
				info.sizeY = this.rigidBodies[i].size.y;
				info.sizeZ = this.rigidBodies[i].size.z;
				info.pX = this.rigidBodies[i].p.x;
				info.pY = this.rigidBodies[i].p.y;
				info.pZ = this.rigidBodies[i].p.z;
				Util.getZXYMatrix(this.rigidBodies[i].r, mBuf0);
				info.r00 = mBuf0.m00;
				info.r01 = mBuf0.m10;
				info.r02 = mBuf0.m20;
				info.r10 = mBuf0.m01;
				info.r11 = mBuf0.m11;
				info.r12 = mBuf0.m21;
				info.r20 = mBuf0.m02;
				info.r21 = mBuf0.m12;
				info.r22 = mBuf0.m22;
				info.mass = this.rigidBodies[i].mass;
				info.linearDamping = this.rigidBodies[i].linearDamping;
				info.angularDamping = this.rigidBodies[i].angularDamping;
				info.restitution = this.rigidBodies[i].restitution;
				info.friction = this.rigidBodies[i].friction;
				info.operationFlag = this.rigidBodies[i].operationFlag;

				info.write();
			}
		}

		if (this.joints.length > 0)
		{
			// Jointのパラメータを初期化
			JointInfo jInfoP = new JointInfo(this.jointInfo);
			Structure[] jInfoArray = jInfoP.toArray(this.joints.length);

			for (int i = 0; i < this.joints.length; ++i)
			{
				JointInfo info = new JointInfo(jInfoArray[i].getPointer());

				info.jointType = this.joints[i].jointType;
				info.rigidBodyIndex1 = this.joints[i].rigidBodyIndex1;
				info.rigidBodyIndex2 = this.joints[i].rigidBodyIndex2;
				info.pX = this.joints[i].p.x;
				info.pY = this.joints[i].p.y;
				info.pZ = this.joints[i].p.z;
				Util.getZXYMatrix(this.joints[i].r, mBuf0);
				info.r00 = mBuf0.m00;
				info.r01 = mBuf0.m10;
				info.r02 = mBuf0.m20;
				info.r10 = mBuf0.m01;
				info.r11 = mBuf0.m11;
				info.r12 = mBuf0.m21;
				info.r20 = mBuf0.m02;
				info.r21 = mBuf0.m12;
				info.r22 = mBuf0.m22;
				info.minLinearX = this.joints[i].minLinear.x;
				info.minLinearY = this.joints[i].minLinear.y;
				info.minLinearZ = this.joints[i].minLinear.z;
				info.maxLinearX = this.joints[i].maxLinear.x;
				info.maxLinearY = this.joints[i].maxLinear.y;
				info.maxLinearZ = this.joints[i].maxLinear.z;
				info.minAngularX = this.joints[i].minAngular.x;
				info.minAngularY = this.joints[i].minAngular.y;
				info.minAngularZ = this.joints[i].minAngular.z;
				info.maxAngularX = this.joints[i].maxAngular.x;
				info.maxAngularY = this.joints[i].maxAngular.y;
				info.maxAngularZ = this.joints[i].maxAngular.z;
				info.springLinearX = this.joints[i].springLinear.x;
				info.springLinearY = this.joints[i].springLinear.y;
				info.springLinearZ = this.joints[i].springLinear.z;
				info.springAngularX = this.joints[i].springAngular.x;
				info.springAngularY = this.joints[i].springAngular.y;
				info.springAngularZ = this.joints[i].springAngular.z;

				info.write();
			}
		}

		// DLL側の初期化処理。
		PhysicsLibrary.instance.initPhysics(this.physicsIndex, this.scale);
	}

	/**
	 * 剛体の座標変換を行ないます。
	 * @param deltaTime 前フレームからの経過時間。
	 */
	public void transformRigidBody(float deltaTime)
	{
		// Kinematic剛体の処理
		if (this.rigidBodies.length > 0)
		{
			TransformInfo tInfoP = new TransformInfo(this.transformInfo);
			Structure[] tInfoArray = tInfoP.toArray(this.rigidBodies.length);

			for (int i = 0; i < this.rigidBodies.length; ++i)
			{
				RigidBodyData body = this.rigidBodies[i];

				if (body.operationFlag == 0 && body.boneIndex >= 0)
				{
					TransformInfo transform = new TransformInfo(tInfoArray[i].getPointer());

					BoneData bone = this.bones[body.boneIndex];
					Matrix4f m = mBuf0;
					Vector3f offset = vBuf0;

					offset.x = body.p.x - bone.p.x;
					offset.y = body.p.y - bone.p.y;
					offset.z = body.p.z - bone.p.z;

					m.load(body.initMatrix);
					m.translate(offset);
					Matrix4f.mul(bone.localMatrix, m, m);
					offset.negate();
					m.translate(offset);

					transform.pX = m.m30;
					transform.pY = m.m31;
					transform.pZ = m.m32;

					transform.r00 = m.m00;
					transform.r01 = m.m10;
					transform.r02 = m.m20;
					transform.r10 = m.m01;
					transform.r11 = m.m11;
					transform.r12 = m.m21;
					transform.r20 = m.m02;
					transform.r21 = m.m12;
					transform.r22 = m.m22;

					transform.write();
				}
			}
		}
		PhysicsLibrary.instance.transformKinematics(this.physicsIndex);

		// ステップを計算
		PhysicsLibrary.instance.stepSimulation(this.physicsIndex, deltaTime);

		// ボーン位置合わせの処理
		PhysicsLibrary.instance.loadTransformOfRigidBodies(this.physicsIndex);
		if (this.rigidBodies.length > 0)
		{
			TransformInfo tInfoP = new TransformInfo(this.transformInfo);
			Structure[] tInfoArray = tInfoP.toArray(this.rigidBodies.length);

			for (int i = 0; i < this.rigidBodies.length; ++i)
			{
				RigidBodyData body = this.rigidBodies[i];

				if ((body.operationFlag == 1 || body.operationFlag == 2) && body.boneIndex >= 0)
				{
					TransformInfo transform = new TransformInfo(tInfoArray[i].getPointer());

					BoneData bone = this.bones[body.boneIndex];

					Matrix4f m = mBuf0;
					Matrix4f mi = mBuf1;
					Vector3f offset = vBuf0;

					offset.x = body.p.x - bone.p.x;
					offset.y = body.p.y - bone.p.y;
					offset.z = body.p.z - bone.p.z;

					m.setIdentity();
					mi.load(body.initMatrix);

					m.m30 = transform.pX;
					m.m31 = transform.pY;
					m.m32 = transform.pZ;

					m.m00 = transform.r00;
					m.m01 = transform.r10;
					m.m02 = transform.r20;
					m.m10 = transform.r01;
					m.m11 = transform.r11;
					m.m12 = transform.r21;
					m.m20 = transform.r02;
					m.m21 = transform.r12;
					m.m22 = transform.r22;

					m.translate(offset);

					mi.translate(offset);
					mi.invert();

					Matrix4f.mul(m, mi, m);

					bone.localMatrix.load(m);
				}
			}
		}
	}

	/**
	 * デストラクタです。
	 */
	public void destroy()
	{
		PhysicsLibrary.instance.destroy(this.physicsIndex);
	}

	/**
	 * DLLの関数定義を中継します。
	 * @author tana
	 */
	public interface PhysicsLibrary extends Library
	{
		public static PhysicsLibrary instance = Platform.isWindows()
				? (PhysicsLibrary)Native.loadLibrary(tmpDLLPath, PhysicsLibrary.class) : null;

		public void init();
		public int getObject();
		public void initObject(
				int index,
				PointerByReference rInfo, PointerByReference tInfo, int rCount,
				PointerByReference jInfo, int jCount);
		public void initPhysics(int index, float scale);
		public void transformKinematics(int index);
		public void stepSimulation(int index, float deltaTime);
		public void loadTransformOfRigidBodies(int index);
		public void destroy(int index);
	}

	/**
	 * DLLと共有する剛体情報です。
	 * @author tana
	 */
	public static class RigidBodyInfo extends Structure
	{
		public int group;
		public int uncollisionGroupFlag;
		public int shape;
		public float sizeX;
		public float sizeY;
		public float sizeZ;
		public float pX;
		public float pY;
		public float pZ;
		public float r00;
		public float r01;
		public float r02;
		public float r10;
		public float r11;
		public float r12;
		public float r20;
		public float r21;
		public float r22;
		public float mass;
		public float linearDamping;
		public float angularDamping;
		public float restitution;
		public float friction;
		public int operationFlag;

		public RigidBodyInfo()
		{
			super();
		}

		public RigidBodyInfo(Pointer p)
		{
			super();
			this.useMemory(p);
			this.read();
		}

		@Override
		protected List getFieldOrder()
		{
			List l = new ArrayList();

			l.add("group");
			l.add("uncollisionGroupFlag");
			l.add("shape");
			l.add("sizeX");
			l.add("sizeY");
			l.add("sizeZ");
			l.add("pX");
			l.add("pY");
			l.add("pZ");
			l.add("r00");
			l.add("r01");
			l.add("r02");
			l.add("r10");
			l.add("r11");
			l.add("r12");
			l.add("r20");
			l.add("r21");
			l.add("r22");
			l.add("mass");
			l.add("linearDamping");
			l.add("angularDamping");
			l.add("restitution");
			l.add("friction");
			l.add("operationFlag");

			return l;
		}
	}

	/**
	 * DLLと共有するJoint情報です。
	 * @author tana
	 */
	public static class JointInfo extends Structure
	{
		public int jointType;
		public int rigidBodyIndex1;
		public int rigidBodyIndex2;
		public float pX;
		public float pY;
		public float pZ;
		public float r00;
		public float r01;
		public float r02;
		public float r10;
		public float r11;
		public float r12;
		public float r20;
		public float r21;
		public float r22;
		public float minLinearX;
		public float minLinearY;
		public float minLinearZ;
		public float maxLinearX;
		public float maxLinearY;
		public float maxLinearZ;
		public float minAngularX;
		public float minAngularY;
		public float minAngularZ;
		public float maxAngularX;
		public float maxAngularY;
		public float maxAngularZ;
		public float springLinearX;
		public float springLinearY;
		public float springLinearZ;
		public float springAngularX;
		public float springAngularY;
		public float springAngularZ;

		public JointInfo()
		{
			super();
		}

		public JointInfo(Pointer p)
		{
			super();
			this.useMemory(p);
			this.read();
		}

		@Override
		protected List getFieldOrder()
		{
			List l = new ArrayList();

			l.add("jointType");
			l.add("rigidBodyIndex1");
			l.add("rigidBodyIndex2");
			l.add("pX");
			l.add("pY");
			l.add("pZ");
			l.add("r00");
			l.add("r01");
			l.add("r02");
			l.add("r10");
			l.add("r11");
			l.add("r12");
			l.add("r20");
			l.add("r21");
			l.add("r22");
			l.add("minLinearX");
			l.add("minLinearY");
			l.add("minLinearZ");
			l.add("maxLinearX");
			l.add("maxLinearY");
			l.add("maxLinearZ");
			l.add("minAngularX");
			l.add("minAngularY");
			l.add("minAngularZ");
			l.add("maxAngularX");
			l.add("maxAngularY");
			l.add("maxAngularZ");
			l.add("springLinearX");
			l.add("springLinearY");
			l.add("springLinearZ");
			l.add("springAngularX");
			l.add("springAngularY");
			l.add("springAngularZ");

			return l;
		}
	}

	/**
	 * DLLと共有する座標変換情報です。
	 * @author tana
	 */
	public static class TransformInfo extends Structure
	{
		public float pX;
		public float pY;
		public float pZ;
		public float r00;
		public float r01;
		public float r02;
		public float r10;
		public float r11;
		public float r12;
		public float r20;
		public float r21;
		public float r22;

		public TransformInfo()
		{
			super();
		}

		public TransformInfo(Pointer p)
		{
			super();
			this.useMemory(p);
			this.read();
		}

		@Override
		protected List getFieldOrder()
		{
			List l = new ArrayList();

			l.add("pX");
			l.add("pY");
			l.add("pZ");
			l.add("r00");
			l.add("r01");
			l.add("r02");
			l.add("r10");
			l.add("r11");
			l.add("r12");
			l.add("r20");
			l.add("r21");
			l.add("r22");

			return l;
		}
	}
}
