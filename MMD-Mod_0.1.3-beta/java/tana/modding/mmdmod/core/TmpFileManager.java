package tana.modding.mmdmod.core;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 一時ファイルを管理します。
 * @author tana
 */
public class TmpFileManager
{
	/**
	 * 一時ディレクトリのパスです。
	 */
	private static String tmpPath = "tana/MMD-Mod";
	/**
	 * ファイル読み込み時のバッファサイズです。
	 */
	private static int bufferSize = 1024;

	/**
	 * 指定されたファイルを一時ディレクトリにコピーします。
	 * そのファイルが一時ディレクトリに既に存在する場合、コピーは行ないません。
	 * @param fileName コピーするファイルのファイル名。
	 * @return コピーされた一時ファイルのファイル名。
	 */
	public static String copyTmpFile(String fileName)
	{
		InputStream in = null;
		OutputStream out = null;
		try
		{
			File tmpFile = new File(new File(System.getProperty("java.io.tmpdir"), tmpPath), fileName);

			if (tmpFile.exists())  // ファイルが存在する場合、リターン
			{
				return tmpFile.getAbsolutePath();
			}

			File parent = new File(tmpFile.getParent());

			if (!parent.exists())  // ディレクトリ作成の必要がある場合
			{
				parent.mkdirs();
			}

			in = TmpFileManager.class.getClassLoader().getResourceAsStream(fileName);
			out = new BufferedOutputStream(new FileOutputStream(tmpFile));

			copyStream(in, out);

			return tmpFile.getAbsolutePath();
		}
		catch (Exception e)
		{
			new IOException("Failed to copy required file.", e).printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (in != null) in.close();
				if (out != null) out.close();
			}
			catch (Exception e)
			{
				new IOException("Failed to copy required file.", e).printStackTrace();
				return null;
			}
		}
	}

	/**
	 * ストリームのデータをinからoutへコピーします。
	 * @param in 入力ストリーム。
	 * @param out 出力ストリーム。
	 * @throws IOException
	 */
	private static void copyStream(InputStream in, OutputStream out) throws IOException
	{
		int len = -1;
		byte[] buf = new byte[bufferSize];

		while ((len = in.read(buf, 0, buf.length)) != -1)
		{
			out.write(buf, 0, len);
		}
		out.flush();
	}
}
