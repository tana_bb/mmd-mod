package tana.modding.mmdmod;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import tana.modding.mmdmod.core.AudioPlayer;
import tana.modding.mmdmod.core.MMDRenderer;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * MMD-Modを実装します。
 * @author tana
 */
@Mod(modid = MMDMod.MODID, version = MMDMod.VERSION)
public class MMDMod implements ForgeChunkManager.LoadingCallback
{
	/**
	 * Mod-ID
	 */
	public static final String MODID = "mmdmod";
	/**
	 * Version
	 */
	public static final String VERSION = "0.1.3-beta";

	/**
	 * リソースが格納されるディレクトリです。
	 */
	public static final String dirPath = "./mmd-resources/";

	/**
	 * レンダリングに使用するオブジェクトです。
	 */
	public MMDRenderer renderer = null;
	/**
	 * オーディオ再生オブジェクト群です。
	 */
	public List<AudioPlayer> audioList = new ArrayList<AudioPlayer>();

	/**
	 * MMDブロックのオブジェクトです。
	 */
	public Block mmdBlock = null;
	/**
	 * MMDブロックのGUIのIDです。
	 */
	public int mmdGuiID = 1;

	/**
	 * このクラスのオブジェクトです。
	 */
	@Instance("mmdmod")
	public static MMDMod instance = null;

	/**
	 * プロキシです。
	 */
	@SidedProxy(clientSide = "tana.modding.mmdmod.ClientProxy", serverSide = "tana.modding.mmdmod.CommonProxy")
	public static CommonProxy proxy;

	/**
	 * PreInitializationEventのハンドラです。
	 * @param e
	 */
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		// リソースディレクトリを作成
		try
		{
			File dir = new File(dirPath);
			if (!dir.exists())
			{
				dir.mkdirs();
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		// ブロックを登録
		mmdBlock = new BlockMMDBlock();
		GameRegistry.registerBlock(mmdBlock
				.setHardness(1.5F).setResistance(10.0F)
				.setStepSound(new Block.SoundType("stone", 1.0F, 1.0F))
				.setBlockName("mmdmod:mmd_block")
				.setBlockTextureName("mmdmod:mmd_block"),
				"mmdmod:mmd_block");

		// レシピを登録
		GameRegistry.addRecipe(new ItemStack(mmdBlock, 1),
			new Object[] {"XYX", "XZX", "XXX",
			'X', Items.iron_ingot, 'Y', Items.redstone, 'Z', Blocks.jukebox});
	}

	/**
	 * InitializationEventのハンドラです。
	 * @param e
	 */
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();

		MinecraftForge.EVENT_BUS.register(this);
		NetworkRegistry.INSTANCE.registerGuiHandler(this, proxy);
		ForgeChunkManager.setForcedChunkLoadingCallback(this, this);
	}

	/**
	 * Loadイベントのハンドラです。
	 * @param e
	 */
	@SubscribeEvent
	public void load(WorldEvent.Load e)
	{
	}

	/**
	 * Unloadイベントのハンドラです。
	 * @param e
	 */
	@SubscribeEvent
	public void unload(WorldEvent.Unload e)
	{
		// オーディオを停止
		int aSize = this.audioList.size();
		for (int i = 0; i < aSize; ++i)
		{
			AudioPlayer audio = this.audioList.get(0);
			if (audio != null)
			{
				audio.stop();
			}
			this.audioList.remove(0);
		}
	}

	@Override
	public void ticketsLoaded(List<Ticket> tickets, World world)
	{
	}
}
