#include "stdafx.h"
#include "Physics.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"


#define ARRAY_SIZE 1000

static PhysicsObject *obj[ARRAY_SIZE];


PHYSICS_API void init()
{
	for (int i = 0; i < ARRAY_SIZE; ++i)
	{
		obj[i] = NULL;
	}
}

PHYSICS_API int getObject()
{
	int i;
	for (i = 0; i < ARRAY_SIZE; ++i)
	{
		if (obj[i] == NULL)
		{
			break;
		}
	}

	if (i >= ARRAY_SIZE)
	{
		return -1;
	}

	obj[i] = new PhysicsObject();
	return i;
}

PHYSICS_API void initObject(int index, RigidBodyInfo **rInfo, TransformInfo **tInfo, int rCount, JointInfo **jInfo, int jCount)
{
	obj[index]->rigidBodyInfo = new RigidBodyInfo[rCount];
	obj[index]->transformInfo = new TransformInfo[rCount];
	obj[index]->bodies = new btRigidBody*[rCount];
	obj[index]->shapes = new btCollisionShape*[rCount];
	obj[index]->rigidBodyCount = rCount;
	
	obj[index]->jointInfo = new JointInfo[jCount];
	obj[index]->joints = new btTypedConstraint*[jCount];
	obj[index]->jointCount = jCount;

	*rInfo = obj[index]->rigidBodyInfo;
	*tInfo = obj[index]->transformInfo;
	*jInfo = obj[index]->jointInfo;
}

PHYSICS_API void initPhysics(int index, float scale)
{
	obj[index]->scale = scale;

	obj[index]->collisionConfig = new btDefaultCollisionConfiguration();
	obj[index]->dispatcher = new btCollisionDispatcher(obj[index]->collisionConfig);
	obj[index]->overlappingPairCache = new btDbvtBroadphase();

	obj[index]->constraintSolver = new btSequentialImpulseConstraintSolver();

	obj[index]->dynamicsWorld = new btDiscreteDynamicsWorld(
		obj[index]->dispatcher, obj[index]->overlappingPairCache, obj[index]->constraintSolver, obj[index]->collisionConfig);
	obj[index]->dynamicsWorld->setGravity(btVector3(0.0F, -10.0F, 0.0F));

	for (int i = 0; i < obj[index]->rigidBodyCount; ++i)
	{
		switch (obj[index]->rigidBodyInfo[i].shape)
		{
		case 0:
			obj[index]->shapes[i] = new btSphereShape(obj[index]->rigidBodyInfo[i].sizeX * scale);
			break;
		case 1:
			obj[index]->shapes[i] = new btBoxShape(btVector3(
				obj[index]->rigidBodyInfo[i].sizeX * scale,
				obj[index]->rigidBodyInfo[i].sizeY * scale,
				obj[index]->rigidBodyInfo[i].sizeZ * scale));
			break;
		case 2:
			obj[index]->shapes[i] = new btCapsuleShape(obj[index]->rigidBodyInfo[i].sizeX * scale, obj[index]->rigidBodyInfo[i].sizeY * scale);
			break;
		}

		btTransform transform = btTransform();
		transform.setIdentity();

		transform.setOrigin(btVector3(
			obj[index]->rigidBodyInfo[i].pX * scale,
			obj[index]->rigidBodyInfo[i].pY * scale,
			obj[index]->rigidBodyInfo[i].pZ * scale));
		transform.setBasis(btMatrix3x3(
			obj[index]->rigidBodyInfo[i].r00, obj[index]->rigidBodyInfo[i].r01, obj[index]->rigidBodyInfo[i].r02,
			obj[index]->rigidBodyInfo[i].r10, obj[index]->rigidBodyInfo[i].r11, obj[index]->rigidBodyInfo[i].r12,
			obj[index]->rigidBodyInfo[i].r20, obj[index]->rigidBodyInfo[i].r21, obj[index]->rigidBodyInfo[i].r22));

		btMotionState *motionState = new btDefaultMotionState(transform);

		btVector3 localInertia = btVector3(0.0F, 0.0F, 0.0F);
		if (obj[index]->rigidBodyInfo[i].mass != 0.0F)
		{
			obj[index]->shapes[i]->calculateLocalInertia(obj[index]->rigidBodyInfo[i].mass, localInertia);
		}

		btRigidBody::btRigidBodyConstructionInfo cInfo =
			btRigidBody::btRigidBodyConstructionInfo(obj[index]->rigidBodyInfo[i].mass, motionState, obj[index]->shapes[i], localInertia);
		obj[index]->bodies[i] = new btRigidBody(cInfo);

		obj[index]->bodies[i]->setDamping(obj[index]->rigidBodyInfo[i].linearDamping, obj[index]->rigidBodyInfo[i].angularDamping);
		obj[index]->bodies[i]->setRestitution(obj[index]->rigidBodyInfo[i].restitution);
		obj[index]->bodies[i]->setFriction(obj[index]->rigidBodyInfo[i].friction);

		if (obj[index]->rigidBodyInfo[i].operationFlag == 0)
		{
			obj[index]->bodies[i]->setCollisionFlags(obj[index]->bodies[i]->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
			obj[index]->bodies[i]->setActivationState(DISABLE_DEACTIVATION);
		}

		obj[index]->dynamicsWorld->addRigidBody(
			obj[index]->bodies[i]);//,
			//(short)obj[index]->rigidBodyInfo[i].group,
			//(short)~obj[index]->rigidBodyInfo[i].uncollisionGroupFlag);
	}

	for (int i = 0; i < obj[index]->jointCount; ++i)
	{
		if (obj[index]->jointInfo[i].rigidBodyIndex1 < 0 || obj[index]->jointInfo[i].rigidBodyIndex2 < 0)
		{
			continue;
		}

		RigidBodyInfo *bodyInfo1 = &obj[index]->rigidBodyInfo[obj[index]->jointInfo[i].rigidBodyIndex1];
		RigidBodyInfo *bodyInfo2 = &obj[index]->rigidBodyInfo[obj[index]->jointInfo[i].rigidBodyIndex2];

		btRigidBody *body1 = obj[index]->bodies[obj[index]->jointInfo[i].rigidBodyIndex1];
		btRigidBody *body2 = obj[index]->bodies[obj[index]->jointInfo[i].rigidBodyIndex2];

		btTransform t1 = btTransform();
		btTransform t2 = btTransform();

		t1.setIdentity();
		t1.setOrigin(btVector3(
			(obj[index]->jointInfo[i].pX - bodyInfo1->pX) * scale,
			(obj[index]->jointInfo[i].pY - bodyInfo1->pY) * scale,
			(obj[index]->jointInfo[i].pZ - bodyInfo1->pZ) * scale));

		t1.setBasis(btMatrix3x3(
			obj[index]->jointInfo[i].r00, obj[index]->jointInfo[i].r01, obj[index]->jointInfo[i].r02,
			obj[index]->jointInfo[i].r10, obj[index]->jointInfo[i].r11, obj[index]->jointInfo[i].r12,
			obj[index]->jointInfo[i].r20, obj[index]->jointInfo[i].r21, obj[index]->jointInfo[i].r22));

		t2.setIdentity();
		t2.setOrigin(btVector3(
			(obj[index]->jointInfo[i].pX - bodyInfo2->pX) * scale,
			(obj[index]->jointInfo[i].pY - bodyInfo2->pY) * scale,
			(obj[index]->jointInfo[i].pZ - bodyInfo2->pZ) * scale));

		t2.setBasis(btMatrix3x3(
			obj[index]->jointInfo[i].r00, obj[index]->jointInfo[i].r01, obj[index]->jointInfo[i].r02,
			obj[index]->jointInfo[i].r10, obj[index]->jointInfo[i].r11, obj[index]->jointInfo[i].r12,
			obj[index]->jointInfo[i].r20, obj[index]->jointInfo[i].r21, obj[index]->jointInfo[i].r22));

		btTypedConstraint *joint = NULL;
		btGeneric6DofSpringConstraint *joint6DofS = NULL;
		btPoint2PointConstraint *jointP2P = NULL;
		float damping = 0.8;

		switch (obj[index]->jointInfo[i].jointType)
		{
		case 0:
			joint6DofS = new btGeneric6DofSpringConstraint(*body1, *body2, t1, t2, true);
			joint = joint6DofS;

			joint6DofS->setLinearLowerLimit(btVector3(
				obj[index]->jointInfo[i].minLinearX * scale,
				obj[index]->jointInfo[i].minLinearY * scale,
				obj[index]->jointInfo[i].minLinearZ * scale));
			joint6DofS->setLinearUpperLimit(btVector3(
				obj[index]->jointInfo[i].maxLinearX * scale,
				obj[index]->jointInfo[i].maxLinearY * scale,
				obj[index]->jointInfo[i].maxLinearZ * scale));
			joint6DofS->setAngularLowerLimit(btVector3(
				obj[index]->jointInfo[i].minAngularX, obj[index]->jointInfo[i].minAngularY, obj[index]->jointInfo[i].minAngularZ));
			joint6DofS->setAngularUpperLimit(btVector3(
				obj[index]->jointInfo[i].maxAngularX, obj[index]->jointInfo[i].maxAngularY, obj[index]->jointInfo[i].maxAngularZ));

			joint6DofS->enableSpring(0, true);
			joint6DofS->enableSpring(1, true);
			joint6DofS->enableSpring(2, true);
			joint6DofS->enableSpring(3, true);
			joint6DofS->enableSpring(4, true);
			joint6DofS->enableSpring(5, true);

			joint6DofS->setStiffness(0, obj[index]->jointInfo[i].springLinearX);
			joint6DofS->setStiffness(1, obj[index]->jointInfo[i].springLinearY);
			joint6DofS->setStiffness(2, obj[index]->jointInfo[i].springLinearZ);
			joint6DofS->setStiffness(3, obj[index]->jointInfo[i].springAngularX);
			joint6DofS->setStiffness(4, obj[index]->jointInfo[i].springAngularY);
			joint6DofS->setStiffness(5, obj[index]->jointInfo[i].springAngularZ);

			joint6DofS->setDamping(0, damping);
			joint6DofS->setDamping(1, damping);
			joint6DofS->setDamping(2, damping);
			joint6DofS->setDamping(3, damping);
			joint6DofS->setDamping(4, damping);
			joint6DofS->setDamping(5, damping);
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			jointP2P = new btPoint2PointConstraint(*body1, *body2, t1.getOrigin(), t2.getOrigin());
			joint = jointP2P;
			break;
		}

		obj[index]->joints[i] = joint;
		obj[index]->dynamicsWorld->addConstraint(joint, true);
	}
}

PHYSICS_API void transformKinematics(int index)
{
	for (int i = 0; i < obj[index]->rigidBodyCount; ++i)
	{
		if (obj[index]->rigidBodyInfo[i].operationFlag == 0)
		{
			btTransform transform = btTransform();
			transform.setIdentity();

			transform.setOrigin(btVector3(
				(obj[index]->transformInfo[i].pX + obj[index]->rigidBodyInfo[i].pX) * obj[index]->scale,
				(obj[index]->transformInfo[i].pY + obj[index]->rigidBodyInfo[i].pY) * obj[index]->scale,
				(obj[index]->transformInfo[i].pZ + obj[index]->rigidBodyInfo[i].pZ) * obj[index]->scale));

			transform.setBasis(btMatrix3x3(
				obj[index]->transformInfo[i].r00, obj[index]->transformInfo[i].r01, obj[index]->transformInfo[i].r02,
				obj[index]->transformInfo[i].r10, obj[index]->transformInfo[i].r11, obj[index]->transformInfo[i].r12,
				obj[index]->transformInfo[i].r20, obj[index]->transformInfo[i].r21, obj[index]->transformInfo[i].r22));

			obj[index]->bodies[i]->getMotionState()->setWorldTransform(transform);
		}
	}
}

PHYSICS_API void stepSimulation(int index, float deltaTime)
{
	obj[index]->dynamicsWorld->stepSimulation(deltaTime);
}

PHYSICS_API void loadTransformOfRigidBodies(int index)
{
	for (int i = 0; i < obj[index]->rigidBodyCount; ++i)
	{
		btTransform transform = btTransform();
		obj[index]->bodies[i]->getMotionState()->getWorldTransform(transform);

		btVector3 p = transform.getOrigin();
		obj[index]->transformInfo[i].pX = p.getX() / obj[index]->scale - obj[index]->rigidBodyInfo[i].pX;
		obj[index]->transformInfo[i].pY = p.getY() / obj[index]->scale - obj[index]->rigidBodyInfo[i].pY;
		obj[index]->transformInfo[i].pZ = p.getZ() / obj[index]->scale - obj[index]->rigidBodyInfo[i].pZ;

		btMatrix3x3 r = transform.getBasis();
		obj[index]->transformInfo[i].r00 = r.getRow(0).getX();
		obj[index]->transformInfo[i].r01 = r.getRow(0).getY();
		obj[index]->transformInfo[i].r02 = r.getRow(0).getZ();
		obj[index]->transformInfo[i].r10 = r.getRow(1).getX();
		obj[index]->transformInfo[i].r11 = r.getRow(1).getY();
		obj[index]->transformInfo[i].r12 = r.getRow(1).getZ();
		obj[index]->transformInfo[i].r20 = r.getRow(2).getX();
		obj[index]->transformInfo[i].r21 = r.getRow(2).getY();
		obj[index]->transformInfo[i].r22 = r.getRow(2).getZ();
	}
}

PHYSICS_API void destroy(int index)
{
	for (int i = 0; i < obj[index]->jointCount; ++i)
	{
		obj[index]->dynamicsWorld->removeConstraint(obj[index]->joints[i]);
		delete obj[index]->joints[i];
	}
	delete[] obj[index]->joints;
	delete[] obj[index]->jointInfo;

	for (int i = 0; i < obj[index]->rigidBodyCount; ++i)
	{
		obj[index]->dynamicsWorld->removeCollisionObject(obj[index]->bodies[i]);
		delete obj[index]->bodies[i]->getMotionState();
		delete obj[index]->bodies[i];
		delete obj[index]->shapes[i];
	}
	delete[] obj[index]->bodies;
	delete[] obj[index]->shapes;
	delete[] obj[index]->transformInfo;
	delete[] obj[index]->rigidBodyInfo;

	delete obj[index]->dynamicsWorld;
	delete obj[index]->constraintSolver;
	delete obj[index]->overlappingPairCache;
	delete obj[index]->dispatcher;
	delete obj[index]->collisionConfig;

	delete obj[index];
	obj[index] = NULL;
}
