#pragma once

struct RigidBodyInfo
{
	int group;
	int uncollisionGroupFlag;
	int shape;
	float sizeX;
	float sizeY;
	float sizeZ;
	float pX;
	float pY;
	float pZ;
	float r00;
	float r01;
	float r02;
	float r10;
	float r11;
	float r12;
	float r20;
	float r21;
	float r22;
	float mass;
	float linearDamping;
	float angularDamping;
	float restitution;
	float friction;
	int operationFlag;
};
