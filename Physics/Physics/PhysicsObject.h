#pragma once

#include "RigidBodyInfo.h"
#include "JointInfo.h"
#include "TransformInfo.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"


struct PhysicsObject
{
	btDiscreteDynamicsWorld *dynamicsWorld;
	btDefaultCollisionConfiguration *collisionConfig;
	btCollisionDispatcher *dispatcher;
	btBroadphaseInterface *overlappingPairCache;
	btConstraintSolver *constraintSolver;

	RigidBodyInfo *rigidBodyInfo;
	TransformInfo *transformInfo;
	btRigidBody **bodies;
	btCollisionShape **shapes;
	int rigidBodyCount;

	JointInfo *jointInfo;
	btTypedConstraint **joints;
	int jointCount;

	float scale;
};