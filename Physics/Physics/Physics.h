#pragma once

#ifdef PHYSICS_EXPORTS
#define PHYSICS_API extern "C" __declspec(dllexport)
#else
#define PHYSICS_API extern "C" __declspec(dllimport)
#endif

#include "RigidBodyInfo.h"
#include "JointInfo.h"
#include "TransformInfo.h"
#include "PhysicsObject.h"


PHYSICS_API void init();
PHYSICS_API int getObject();
PHYSICS_API void initObject(int index, RigidBodyInfo **rInfo, TransformInfo **tInfo, int rCount, JointInfo **jInfo, int jCount);
PHYSICS_API void initPhysics(int index, float scale);
PHYSICS_API void transformKinematics(int index);
PHYSICS_API void stepSimulation(int index, float deltaTime);
PHYSICS_API void loadTransformOfRigidBodies(int index);
PHYSICS_API void destroy(int index);
