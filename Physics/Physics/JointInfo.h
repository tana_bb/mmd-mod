#pragma once

struct JointInfo
{
	int jointType;
	int rigidBodyIndex1;
	int rigidBodyIndex2;
	float pX;
	float pY;
	float pZ;
	float r00;
	float r01;
	float r02;
	float r10;
	float r11;
	float r12;
	float r20;
	float r21;
	float r22;
	float minLinearX;
	float minLinearY;
	float minLinearZ;
	float maxLinearX;
	float maxLinearY;
	float maxLinearZ;
	float minAngularX;
	float minAngularY;
	float minAngularZ;
	float maxAngularX;
	float maxAngularY;
	float maxAngularZ;
	float springLinearX;
	float springLinearY;
	float springLinearZ;
	float springAngularX;
	float springAngularY;
	float springAngularZ;
};
