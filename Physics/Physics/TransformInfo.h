#pragma once

struct TransformInfo
{
	float pX;
	float pY;
	float pZ;
	float r00;
	float r01;
	float r02;
	float r10;
	float r11;
	float r12;
	float r20;
	float r21;
	float r22;
};
