◇◇◇◇    MMD-Mod version 0.1.3-beta    ◇◇◇◇
                            作成者 tana@ヒキニートP


このプログラムはMojangのMincraft用Modです。
本プログラムはWindows専用です。注意してください。
Minecraftの対応バージョンは、1.7.2です。

本プログラムの所持、使用に際して起こるいかなる事象に関して、
作成者は一切の責任を負いません。
あらかじめご了承ください。

本プログラムはベータ版です。
バグを含む可能性がありますので、
使用時にはワールドデータのバックアップを行なっていただくよう
お願いいたします。

◆インストール方法

１．同梱の、「Visual Studio 2013 の Visual C++ 再頒布可能パッケージ」を
    インストールしてください。
    このパッケージには「vcredist_x86.exe」と「vcredist_x64.exe」がありますが、
    ご自身のJavaVMが32bitであれば「vcredist_x86.exe」を、
    64bitであれば「vcredist_x64.exe」をインストールしてください。
    （OSが32bitならJavaVMも32bitです。）
    （OSが64bitの場合、両方インストールしても差し支えありません。）

２．Minecraft Forge 1.7.2-10.12.1.1060以降をダウンロードし、
    インストールしてください。
    http://files.minecraftforge.net/

３．ユーザフォルダ\AppData\Roaming\.minecraft\mods に
    「[1.7.2]MMD-Mod-0.1.3-beta.jar」を置いてください。

◆使用方法

・Modを入れた状態でMinecraftを起動すると、.minecraftフォルダ配下に
  「mmd-resources」というフォルダが作成されます。
  この中に、モデルデータ、モーションデータ、音声データを配置します。

・MMDブロックのレシピは次の通り。
  鉄 赤 鉄
  鉄 Ｊ 鉄
  鉄 鉄 鉄
  （鉄＝鉄インゴット、赤＝レッドストーン、Ｊ＝ジュークボックス）

・MMDブロックを右クリックすると、メニュー画面が開きます。
  ここで「mmd-resources」フォルダに配置したモデルデータなどを読み込めます。

・モデルデータは、PMX形式のみ対応しています。
  また、dds形式のテクスチャには非対応となっていますので、注意してください。

・モーションデータはVMD形式を読み込めます。
  モーションにはIdling状態とActive状態の2つがありますが、
  それぞれ、通常時、レッドストーン信号入力時、の動きです。

・音声ファイルはWAV形式のみ対応しています。
  レッドストーン信号入力時に再生されます。

・Heightはモデルの身長です。

・Speedはモーションの動きの速さです。

・Volumeは音声ファイルのボリュームです。

・Physics:Enable/Disableは、物理演算のon/offです。
  （物理演算の精度は非常に悪いため、モデルによっては酷いことになります。
  その場合、offにしてください。）

・Lighting:ON/OFFは、ライティングの切り替えです。
  ONの場合、モデルは暗くても光が当たった状態になります。
  OFFの場合、周囲が暗いとモデルも暗く表示されます。

・Applyをクリックすると適用されます。メモリ残量に注意してください。
  （Javaのメモリ最大値を1024MBにして実行することをオススメします。）

◆ご注意

・本Modは、樋口優氏製作の「MikuMikuDance」とは関わりがありません。
  また、極北P氏製作の「PMXエディタ」とも関わりがありません。

・冒頭にも述べたとおり、このModはWindows専用です。
  MacやLinuxで実行するとMinecraftが異常終了します。

・IKボーンの計算方法が貧弱なため、膝をうまく曲げることができていません。
  しゃがむ動作などが不自然になります。

・セルフシャドウには対応していません。

・物理演算の再現性が非常に悪いです。試してみてダメだと感じたら、
  offにしてください。

・モデルの表示には、CPUおよびGPUの処理能力、およびメモリ容量を
  多く使用します。
  特にメモリは、使用しすぎるとMinecraftが異常終了する原因となりますので、
  特に複数のモデルを表示する際には、F3キー押下によるデバッグ表示を参考に
  メモリ使用量を管理してください。

◆更新履歴

＜0.1.3-beta＞
MMDブロックを含むチャンクがアンロードされたとき、
音楽が止まらなくなるバグを修正。

＜0.1.2-beta＞
ライティングボタンを追加。
背景画像のアゴを微修正。

＜0.1.1-beta＞
IKボーンの計算を改善。
剛体数、Joint数が0のときに異常終了するバグを修正。

＜0.1-beta＞
リリース。


Copyright (c) 2014 tana@ヒキニートP
Released under the MIT license
http://opensource.org/licenses/mit-license.php
